###############################################################################
#
# Copyright 2010 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################

include make.variables

ifdef DEBUG_GEN_MACROS
GEN_MACROS_FLAGS := -d
endif

ifndef OA_SRC
OA_SRC = $(OA_ROOT)
endif

default : all

PERL_INSTALL   := perl5
PYTHON_INSTALL := python2.5
RUBY_INSTALL   := ruby1.8
TCL_INSTALL    := tcl8.4

TARGETS := $(PERL_INSTALL) $(PYTHON_INSTALL) $(RUBY_INSTALL) $(TCL_INSTALL)

.PHONY : default all $(TARGETS) wrappers perl python ruby tcl prereq labs test

perl : $(PERL_INSTALL)

python : $(PYTHON_INSTALL)

ruby : $(RUBY_INSTALL)

tcl : $(TCL_INSTALL)

all : $(TARGETS)

wrappers : $(TARGETS:%=%-wrappers)

%-wrappers : prereq
	$(MAKE) -C $* wrappers

$(MODS) :

$(TARGETS) : prereq
	$(MAKE) -C $@ $(MAKECMDGOALS)

prereq : macros/.macros munged_headers/.munged_headers

clean : cleanlabs
	rm -rf tu.cc.t00.tu tu.cc.001t.tu macros/*.i macros/.macros munged_headers/oaCommonSPtr.h munged_headers/.munged_headers
	for target in $(TARGETS) ; do \
	  $(MAKE) -C $$target clean; \
	done

macros munged_headers :
	@mkdir -p $@

munged_headers/.munged_headers : scripts/munge_headers.pl | munged_headers
	$(PERL) scripts/munge_headers.pl -oaroot $(OA_ROOT) -o munged_headers && touch $@

macros/.macros : tu.cc.t00.tu scripts/gen_macros.pl | macros
	$(PERL) scripts/gen_macros.pl $(GEN_MACROS_FLAGS) -f tu.cc.t00.tu -arrays macros/oa_arrays.i -enums macros -collections macros -types macros/oa_typenums.i -objects macros/oa_objects.i -lookuptables macros/oa_lookuptables.i -namespaces macros/oa_namespaces.i -oasrc $(OA_SRC) && touch $@

tu.cc.t00.tu : tu.cc
	$(CXX) -I$(OA_ROOT)/include/oa $(FDUMP) -o /dev/null $< \
		-ldl $(LDLIBS) -loaBase -loaCommon -loaDesign -loaDM -loaPlugIn -loaTech -loaUtil -loaWafer
	if [ ! -f $@ -o $< -nt $@ ]; then \
		cp tu.cc.001t.tu $@; \
	fi

#------- special stuff for running the labs

LABS := $(PERL_INSTALL) $(PYTHON_INSTALL) $(RUBY_INSTALL) $(TCL_INSTALL)

export PERL
export PYTHON
export RUBY
export TCL

export OA_ROOT
export OA_LIB

export OAS_ROOT = $(CURDIR)

include testtarget.mk

#------- end of lab stuff

