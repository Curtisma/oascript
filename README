oaScript

This package provides a scripting language interface to the OpenAccess
C++ API.  Wrappers for the following languages are currently provided:

perl
python
ruby
tcl

In addition this this file, there are language-specific README files
contained in the directories for each target language.

DISCLAIMER: The tcl wrappers do NOT provide the same interface as the
existing oaTcl package.  The tcl wrappers are not meant to supplant
oaTcl, nor are they intended to address any deficiency in oaTcl.

DISCLAIMER: The python wrappers do NOT provide the same interface as
the existing Pyoa wrappers.  These wrappers also imply a performance
penalty compared to Pyoa.

DISCLAIMER: This code is experimental, and the individual language
interfaces are subject to change at any time without warning.  Support
for this package by the developers is available on a strictly
voluntary basis.

DISCLAIMER: These wrappers were created with a "low-effort"
methodology.  Very little has been done thus far to tune them for
performance.


--------------------------------------------------------------------------------
Goals
--------------------------------------------------------------------------------

- To provide an interface to OpenAccess which hews closely to the C++ API, while
using idiomatic syntax for each target language where it makes sense to do so.

- To add convenience and safety features beyond what is provided by the C++
API.  For example:

  - oaException's are caught and translated into the appropriate action in the
target language.

  - oaIsValid() is used to detect invalid or corrupt objects before a segfault
occurs (see "Environment Variables" below).

  - oa[Base/DM/Design/Tech/Wafer]Init() is called automatically when the
respective module is loaded (see "Environment Variables" below).


--------------------------------------------------------------------------------
Instructions for Downloading
--------------------------------------------------------------------------------

The latest packages can be downloaded from the oaScript project page at Si2
found here:

        https://www.si2.org/openeda.si2.org/project/showfiles.php?group_id=78

There, four packages can be downloaded:
 1. The "bin" package:
    This package contains pre-built binaries for oaScript.  It is a best-effort
    to be compatible with common versions of Perl, Python, Ruby, and Tcl.  It can
    be used with the pre-built OA binaries available from Si2.

    Currently, this package is built for:
        Perl 5.8.x
        Python 2.5.x
        Ruby 1.8.x (1.8.5+ on RHEL 5.6 and SLES10)
        Tcl 8.4.x

    The package is built against OpenAccess 22.04.54, but is compatible with other
    versions of OpenAccess of the same major release.

    If you are unable to use the binary package due to compatibility problems,
    you may wish to try a custom build using the "src" and "wrappers" packages
    described below.

    To use the binary package, simply extract it and skip ahead to the
    "Using oaScript" section.

 2. The "src" package:
    This package includes all of the files necessary to build the bindings from
    scratch.  It also contains the unit tests and labs.  If you are interested
    in building the bindings, or running or using the labs, you will need this
    package.

    Please follow the "Build Instructions" section below.  Also, you may wish
    to use the "wrappers" package below to simplify the build procedure.

 3. The "wrappers" package:
    This package contains ready-made SWIG output files to allow a simplified
    custom builds of the oaScript bindings without requiring SWIG or a
    particular version of gcc.

    This package should be unpacked in the same location as "src".

 4. The "tu" package:
    This package contains the gcc translation unit dump files that were used
    to create the wrappers package.  This pacakge is intended for building
    on non-Unix platforms.

    Please follow the "Build Instructions" section below.


--------------------------------------------------------------------------------
System Requirements
--------------------------------------------------------------------------------

Currently, the build system only supports Linux.

It is recommended that you have at least 2GB of RAM.

The following software must be installed on your system:

  - perl version 5.6.0 or newer.  Module ExtUtils::Embed is required.
  - A gcc compiler in the version range 4.1.1 - 4.2.3.
    The Working Group uses mostly gcc 4.2.0.
  - gnu make version 3.81 or newer.
  - swig version 2.0.4 or newer.
  - A full set of OpenAccess source code and libraries, with the original directory structure intact.


--------------------------------------------------------------------------------
Build Instructions
--------------------------------------------------------------------------------

1) Unpack the source code:

$ tar xvfz oaScript.tar.gz
$ cd oaScript

2) Create the file make.variables from the provided template:

$ cp make.variables.template make.variables

3) Open make.variables in a text editor, and provide definitions for the variables at the top of the file.
   (if you're using the "wrappers" package, you can ignore the SWIG and SWIGINC variables).

4) Compile.  To make individual target languages, you may do any of the following:

$ make perl
$ make python
$ make ruby
$ make tcl

To make all target languages, simply run `make`.

If you have multiple processor cores available on your machine, it is recommended
that you also use the '-j <num>' argument to `make` to enable parallel builds.

WARNING: There will be a LOT of benign compiler warnings.

5) Test.  The following directories contain test scripts for each target language:

perl5/test/
python2.5/test/
ruby/test/
tcl/test/

'base_test' may be run standalone with no arguments.
'dm_test' requires a '-libdefs <lib.defs file>' argument.


--------------------------------------------------------------------------------
Using oaScript
--------------------------------------------------------------------------------

To configure your environment to use oaScript, set any of all following
environment variables depending on which scripting languages you will be using:

    export PYTHONPATH=${base}/python2.5/
    export PERL5LIB=${base}/perl5
    export TCLLIBPATH=${base}/tcl8.4
    export RUBYLIB=${base}/ruby1.8

- where ${base} is the location of oaScript, ex:
    /home/OpenAccess/oaScript-v0.5-2011.01.07

And LD_LIBRARY_PATH to include the OpenAccess libraries:

    export LD_LIBRARY_PATH=/home/OpenAccess/oa22.04p054/lib/linux_rhel40_64/opt

Please see the language-specific README file for your scripting language for
details on how to load the bindings into your interpreter.


--------------------------------------------------------------------------------
Environment Variables
--------------------------------------------------------------------------------

Additional features may be enabled/disabled in oaScript through the use of
environment variables.  The following environment variables are recognized:

$OASCRIPT_USE_ISVALID 
By default the oaObject::isValid() method is called on every data object before
it is operated upon to check for safety.  An exception is raised if the object
is not valid.  Set to "0" to disable.

$OASCRIPT_USE_INIT
By default OA module initialization is performed (e.g. loading "design" will
cause oaDesignInit() to be called).  This helps avoid the nasty SEGV that is
received by those who forget to call oaDesignInit() before using OA design
method calls.  Set to "0" to disable.


--------------------------------------------------------------------------------
Exceptions in Process of LibDefList
--------------------------------------------------------------------------------

Runtime errors are thrown when processing the oaLibDefList through the
oaLibDefList::open() and oaLibDefList::openLibs() methods.  The default OA
behavior is to silently fail which is undesirable.  This mechanism will be
transparent to the normal usage of open() and openLibs(); however, the author
of a script should be aware of this behavior and may want to trap it in the
target language.  The language-specific README files may have more specific
information on this feature specific to that language.


--------------------------------------------------------------------------------
Ignored classes
--------------------------------------------------------------------------------

File and directory utility classes (oaFile, oaDir, oaDirIter, and
oaFSComponent) are not available in the scripting interfaces.  These utilities
are natively provided in each language and the native functions should be used
instead.


--------------------------------------------------------------------------------
Known Issues and Errata
--------------------------------------------------------------------------------

1) There is currently no support for the oaOccTraverser class.

2) There is currently no support for the oaObserver class.

3) oaAppDefs are currently supported only in the python, ruby, and tcl 
   interfaces.


--------------------------------------------------------------------------------
TODO
--------------------------------------------------------------------------------

Dates below are working group goals and not guaranteed commitments.

2011
----
08/2011: Perl oaAppDef support
10/2011: Simplify use of oaNames by using a default, user-modifiable global 
         namespace.
12/2011: Production release of oaScript (bug fixes, tie down loose ends)

Future
------
* Performance improvements
* Support oaObservers
* Support region query
* Plugins in scripting languages


--------------------------------------------------------------------------------
FAQ
--------------------------------------------------------------------------------

Q: Are there any safety features on iterators beyond what is provided by the C++
API.

A: No.

Q: Do oaIters pre-fetch their sequence of return values?

A: No.  Just as in C++, the elements returned by an oaIter are generated
incrementally on demand.
