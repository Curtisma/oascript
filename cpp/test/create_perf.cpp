/*
###############################################################################
#
# Copyright 2011 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   05/05/2011: Initial copy created
#
###############################################################################
*/

#include <iostream>
#include <oa/oaDesignDB.h>
#include <unistd.h>
#include <time.h>


using namespace oa;
using namespace std;

static oaNativeNS nns;



int main( int argc, char *argv[] )
{
  // Initialize OA with data model 3, since incremental technology
  // databases are supported by this application.
  oaDesignInit(oacAPIMajorRevNumber, oacAPIMinorRevNumber, 3);
  char pid[21]; // enough to hold all ints to 64bits
  sprintf(pid,"%d",getpid());

  char libname[500];
  sprintf(libname,"testlib_perfcheck");
  char libpath[1000];
  char cmd[2000];
  
  int max_shapes = 1000000;
  int unit_size = 500;

  try {
    sprintf(libpath, "./tmp/%s-%s", libname,pid);
    
    // delete libpath if it exists
    struct stat st;
    if(stat(libpath,&st) == 0){
      printf("%s is present\n",libpath);
      sprintf(cmd,"/bin/rm -rf %s",libpath);
      system(cmd);
    }
  
  
    oaScalarName slib(nns, libname),
      scell(nns, "testlib_perfcheck"),
      sview(nns, "layout");
  
    oaString oaLibPath(libpath);
  
    // create lib
    oaLib *lib = oaLib::find(slib);
    if (lib){
      cerr << "Library already exists - remove first: " << libname << endl;
      return 1;
    }
    lib = oaLib::create(slib, oaLibPath); 
                                                         
    //   layers = [['poly',    1, 'poly'
    //             ['ndiff',   2, 'nDiff'],
    //             ['pdiff',   3, 'pDiff'],
    //             ['contact', 4, 'cut'],
    //             ['metal1',  5, 'metal'],
    //             ['via1',    6, 'cut']]
    //  layers.each do |layer|
    //    name, num, mat_str = layer
    //    OaPhysicalLayer.create(tech, name, num, OaMaterial.new(mat_str))
    //  end

    oaTech          *tech = oaTech::create(slib);
    oaPhysicalLayer::create(tech,"poly",1,oaMaterial("poly"));
    oaPhysicalLayer::create(tech,"ndiff",2,oaMaterial("nDiff"));
    oaPhysicalLayer::create(tech,"pdiff",3,oaMaterial("pDiff"));
    oaPhysicalLayer::create(tech,"contact",4,oaMaterial("cut"));
    oaPhysicalLayer::create(tech,"metal1",5,oaMaterial("metal"));
    oaPhysicalLayer::create(tech,"via1",6,oaMaterial("cut"));

    // Use the first layer number in creating shapes on drawing purpose
    int use_lnum = 1;
    int use_pnum = oavPurposeNumberDrawing;
  
    // create a design and get top block
    oaViewType              *vt = oaViewType::get(oacMaskLayout);
    oaDesign                *design = oaDesign::open(slib, scell, sview, vt, 'w');
    oaBlock *block = oaBlock::create(design);
  
    // create a bunch of rectangles
	oaTimer timer;
    int i = 0;
    oaBox box;
  
    oaLayer         *layer = oaPhysicalLayer::find(tech, "poly");
    oaPurpose       *purpose = oaPurpose::get(tech, oacDrawingPurposeType);

    int lnum = layer->getNumber();
    int pnum = purpose->getNumber();


    for (i=0; i<max_shapes; i++) {
      box.set(oaPoint(i*unit_size, 0), oaPoint((i+1)*unit_size,unit_size));
      oaRect::create(block, lnum, pnum, box);
    }
    printf( "Create rectangles   %.4f\n", timer.getElapsed());


    // Create a bunch of paths
	timer.reset();
    for (i=1; i<max_shapes; i++) {
      int st = i*2*unit_size;
      oaPointArray    pts(3);
      
      pts.append(oaPoint(st,0));
      pts.append(oaPoint(st+i*unit_size, 0));
      pts.append(oaPoint(st+i*unit_size, i*unit_size));

      oaPath::create(block, lnum, pnum, unit_size/4, pts);
    }
	
    printf( "Create paths        %.4f\n", timer.getElapsed());

    // Iterate through all shapes in the block
	timer.reset();
    oaIter<oaShape> shapeIter(block->getShapes());
    while (oaShape * shape = shapeIter.getNext()) {
      shape->isValid();
    }
    printf( "Iterate Shapes      %.4f\n", timer.getElapsed());

    return 0;
  } catch (oaException &excp) {
    cout << "ERROR: " << excp.getMsg() << endl;
    exit(1);
  }
  
  return 0;  
}
