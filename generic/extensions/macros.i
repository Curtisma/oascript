/* -*- mode: c++ -*- */
/*
   Copyright 2011 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%define GETSTRING(class1, func)
%extend OpenAccess_4::class1 {
        oaString func() const
        {
                oaString result;
                self->func(result);
                return result;
        }
};
%enddef

%define GETSTRINGWITHNS(class1, func)
%extend OpenAccess_4::class1 {
        oaString func(const oaNameSpace& ns) const
        {
                oaString result;
                self->func(ns, result);
                return result;
        }

        oaString func() const
        {
                oaString result;
                const oaNameSpace* ns = getDefaultNameSpace();
                self->func(*ns, result);
                return result;
        }
};
%enddef

%define STATICGETSTRING(class1, func)
%extend OpenAccess_4::class1 {
        static oaString func() const
        {
                oaString result;
                class1::func(result);
                return result;
        }
};
%enddef

%define CONSTRUCTFROMSTRING(class1)
%extend OpenAccess_4::class1 {
        // Construct using the default oaNameSpace
        class1(const oaChar* name)
        {
                return new class1(*getDefaultNameSpace(), name);
        }
};
%enddef

%define INITNAME(class1)
%extend OpenAccess_4::class1 {
        void init(const oaString& in)
        {
                self->init(*getDefaultNameSpace(), in);
        }
};
%enddef

%define INITNAMEWITHBASEONLYOPTION(class1)
%extend OpenAccess_4::class1 {
        void init(const oaString& in, oaBoolean baseOnly=false)
        {
                self->init(*getDefaultNameSpace(), in, baseOnly);
        }
};
%enddef

%define GETBOX(class1, func)
%extend OpenAccess_4::class1 {
        oaBox func() const
        {
                oaBox result;
                self->func(result);
                return result;
        }
};
%enddef

%define GETPOINT(class1, func)
%extend OpenAccess_4::class1 {
        oaPoint func() const
        {
                oaPoint result;
                self->func(result);
                return result;
        }
};
%enddef

%define GETPOINTARRAY(class1, func)
%extend OpenAccess_4::class1 {
        oaPointArray func() const
        {
                oaPointArray result;
                self->func(result);
                return result;
        }
};
%enddef

%define GETTRANSFORM(class1, func)
%extend OpenAccess_4::class1 {
        oaTransform func() const
        {
                oaTransform result;
                self->func(result);
                return result;
        }
};
%enddef

%define GETSCALARNAME(class1, func)
%extend OpenAccess_4::class1 {
        oaString func(const OpenAccess_4::oaNameSpace& ns) const
        {
                oaScalarName scalarName;
                self->func(scalarName);
                oaString result;
                scalarName.get(ns, result);
                return result;
        }
};
%extend OpenAccess_4::class1 {
        oaString func() const
        {
                const oaNameSpace* ns = getDefaultNameSpace();
                oaScalarName scalarName;
                self->func(scalarName);
                oaString result;
                scalarName.get(*ns, result);
                return result;
        }
};
%enddef

%define GETUUBOX(class1, origFunc, func)
%extend OpenAccess_4::class1 {
        oaUUBox func() const
        {
                OpenAccess_4::oaDesign* design = self->getDesign();
                OpenAccess_4::oaTech* tech = design->getTech();
                OpenAccess_4::oaViewType* viewType = design->getViewType();
                OpenAccess_4::oaBox box;
                self->origFunc(box);
                oaUUBox uuBox(box, tech, viewType);
                return uuBox;
        }
}
%enddef

%define GETUUPOINT(class1, origFunc, func)
%extend OpenAccess_4::class1 {
        oaUUPoint func() const
        {
                OpenAccess_4::oaDesign* design = self->getDesign();
                OpenAccess_4::oaTech* tech = design->getTech();
                OpenAccess_4::oaViewType* viewType = design->getViewType();
                OpenAccess_4::oaPoint point;
                self->origFunc(point);
                oaUUPoint uuPoint(point, tech, viewType);
                return uuPoint;
        }
}
%enddef

