/* -*- mode: c++ -*- */
/*
   Copyright 2011 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%inline %{

class oaUUSegment
{
public:
    oaUUSegment(const OpenAccess_4::oaSegment& segment,
                OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mSegment(segment)
     , mTech(tech)
     , mViewType(viewType)
    {
    }

    oaUUSegment(OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mSegment()
     , mTech(tech)
     , mViewType(viewType)
    {
    }

    oaUUSegment(const oaUUPoint& headIn, const oaUUPoint& tailIn,
                OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mSegment(headIn.getPoint(), tailIn.getPoint())
     , mTech(tech)
     , mViewType(viewType)
    {
    }

    oaUUSegment(const oaUUVector& vector, const oaUUPoint& ref,
                OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mSegment(vector.getVector(), ref.getPoint())
     , mTech(tech)
     , mViewType(viewType)
    {
    }

    ~oaUUSegment()
    {
    }

    oaUUPoint head() const
    {
        return oaUUPoint(mSegment.head(), mTech, mViewType);
    }

    oaUUPoint tail() const
    {
        return oaUUPoint(mSegment.tail(), mTech, mViewType);
    }

    OpenAccess_4::oaDouble x1() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mSegment.x1());
    }

    OpenAccess_4::oaDouble x2() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mSegment.x2());
    }

    OpenAccess_4::oaDouble y1() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mSegment.y1());
    }

    OpenAccess_4::oaDouble y2() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mSegment.y2());
    }

    void set(const oaUUPoint& head, const oaUUPoint& tail)
    {
        mSegment.set(head.getPoint(), tail.getPoint());
    }

    void set(const oaUUVector& vector, const oaUUPoint& ref)
    {
        mSegment.set(vector.getVector(), ref.getPoint());
    }

    OpenAccess_4::oaBoolean isHorizontal() const
    {
        return mSegment.isHorizontal();
    }

    OpenAccess_4::oaBoolean isVertical() const
    {
        return mSegment.isVertical();
    }

    OpenAccess_4::oaBoolean isOrthogonal() const
    {
        return mSegment.isOrthogonal();
    }

    OpenAccess_4::oaDouble getDeltaX() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mSegment.getDeltaX());
    }

    OpenAccess_4::oaDouble getDeltaY() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mSegment.getDeltaY());
    }

    OpenAccess_4::oaDouble distanceFrom2(const oaUUPoint& point) const
    {
        checkTech();
        oaDouble d = mSegment.distanceFrom2(point.getPoint());
        return d / mTech->getDBUPerUU(mViewType);
    }

    OpenAccess_4::oaDouble distanceFrom2(const oaUUPoint& point, oaUUPoint& loc) const
    {
        checkTech();
        oaDouble d = mSegment.distanceFrom2(point.getPoint(), loc.getPoint());
        return d / mTech->getDBUPerUU(mViewType);
    }

    OpenAccess_4::oaBoolean contains(const oaUUPoint& point,
                                     OpenAccess_4::oaBoolean includeEnds=true) const
    {
        return mSegment.contains(point.getPoint(), includeEnds);
    }

    OpenAccess_4::oaBoolean collinearContains(const oaUUPoint& point,
                                              OpenAccess_4::oaBoolean includeEnds=true) const
    {
        return mSegment.collinearContains(point.getPoint(), includeEnds);
    }

    OpenAccess_4::oaBoolean intersects(const oaUUSegment& seg,
                                       OpenAccess_4::oaBoolean includeEnds=true) const
    {
        return mSegment.intersects(seg.mSegment, includeEnds);
    }

    OpenAccess_4::oaBoolean intersects(const oaUUSegment& seg,
                                       OpenAccess_4::oaBoolean includeEnds,
                                       OpenAccess_4::oaBoolean includeOverlap) const
    {
        return mSegment.intersects(seg.mSegment, includeEnds, includeOverlap);
    }

    OpenAccess_4::oaBoolean intersects(const oaUUSegment& seg, oaUUPoint& pt,
                                       OpenAccess_4::oaBoolean mustIntersect=true) const
    {
        return mSegment.intersects(seg.mSegment, pt.getPoint(), mustIntersect);
    }

    const OpenAccess_4::oaSegment& getSegment() const
    {
        return mSegment;
    }

    OpenAccess_4::oaTech* getTech() const
    {
        return mTech;
    }

    OpenAccess_4::oaViewType* getViewType() const
    {
        return mViewType;
    }

private:
    void checkTech() const
    {
        if (!mTech->isValid()) {
            // make this into an exception!
            printf("invalid tech!\n");
        }
    }

private:
    OpenAccess_4::oaSegment mSegment;
    OpenAccess_4::oaTech* mTech;
    OpenAccess_4::oaViewType* mViewType;
};

%}

