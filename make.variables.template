# -*- mode: makefile-gmake -*-
#-------------------------------------------------------------------------------
# Configuration; change as necessary
#-------------------------------------------------------------------------------

# Set this to the root directory of your OA installation
OA_ROOT	 	:= /disks/delbaere/oaRoot

# This should be 'opt' or 'dbg'.
OA_OPTMODE	:= opt

# Only change this if you're doing a cross-compile (e.g., 32-bit build on 64-bit system).
OA_SYSNAME      := $(shell $(OA_ROOT)/bin/sysname)_$(if $(findstring 64,$(shell uname -m)),64,32)

# Must be g++ in the version range 4.1.1-4.2.x
CXX		:= /usr/bin/g++

# Uncomment if you want to build a debuggable version of the wrappers.  This
# has no relation to OA_OPTMODE.
#OPT		:= -ggdb

# Must be version 1.3.34 or newer.
SWIG		= /disks/delbaere/local/bin/swig

# Location of swig typemap library (.i files).
SWIGINC         = /disks/delbaere/local/share/swig/1.3.35/

# Must be version 5.6.0 or newer.
PERL		= /usr/bin/perl

# Must be version 2.5 or newer
PYTHON		= /usr/bin/python

# Location of python header files
PYTHON_INC	= /usr/include/python2.4

# Tested using Ruby 1.8.4
RUBY            = /usr/bin/ruby

# Shouldn't need to modify this
RUBY_PLATFORM   := $(shell $(RUBY) -e 'puts PLATFORM')

# Location of ruby header files
RUBY_INC        = /usr/lib64/ruby/1.8/$(RUBY_PLATFORM)

# Location of where to install OAS Ruby
OAS_RUBY_INST   = /lib/ruby

# Tested using Tcl 8.4.5
TCL             = /usr/bin/tclsh

# Location of Tcl header files
TCL_INC        = /usr/include

#-------------------------------------------------------------------------------
# Shouldn't need to modify anything below here.
#-------------------------------------------------------------------------------

OA_SRC		:= $(OA_ROOT)

OA_LIB		= lib/$(OA_SYSNAME)/$(OA_OPTMODE)

CPPFLAGS	= -I$(OA_ROOT)/include -DSWIG_TYPE_TABLE=oa -DHAVE_EACCESS
OPT		?= -O2
CXXFLAGS	= $(OPT) -fpic
FDUMP		:= -fdump-translation-unit

SWIGIGNORE	=
SWIGFLAGS	= -c++ $(SWIGIGNORE) -I$(OA_ROOT)/include -includeall -ignoremissing

LDFLAGS	= -shared -Wl,--enable-new-dtags -Wl,-rpath,$(OA_ROOT)/$(OA_LIB)
LDLIBS	= -L$(OA_ROOT)/$(OA_LIB)

MODS		= common base plugin dm tech design util wafer
MOD_RE		= common|base|plugin|dm|tech|design|util|wafer

common_LDLIBS	:= -loaCommon -loaPlugIn -loaBase
plugin_LDLIBS	:= $(common_LDLIBS)
base_LDLIBS	:= $(common_LDLIBS)
dm_LDLIBS	:= $(base_LDLIBS) -loaDM -loaDMFileSysBase -loaDMFileSys -loaDMTurboBase -loaDMTurbo
tech_LDLIBS	:= $(dm_LDLIBS) -loaTech
design_LDLIBS	:= $(tech_LDLIBS) -loaDesign -loaNativeText -loaPcellCPP -loaPcellScript -loaRQXYTree
wafer_LDLIBS	:= $(design_LDLIBS) -loaWafer
util_LDLIBS	:= $(common_LDLIBS) -loaDM -loaUtil -loaTech -loaDesign
