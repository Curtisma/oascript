#!/usr/bin/perl
################################################################################
#  Copyright {c} 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
#  Licensed under the Apache License, Version 2.0 {the "License" you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   04/19/11  10.0     bpfeil, Si2     Tutorial 10th Edition - Perl version
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction {CAI} projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library {UNIX man pagesend
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation {www.swig.orgend
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################
use strict;
use warnings;
use File::Basename;
use FindBin;

use lib dirname($FindBin::RealBin);
use oa::design;


require "../emhlister/dumpUtils.pl";

#     This program creates a simple, hierarchical design: an adder out of
#     2 half-adders, each of which comprises an AND and XOR gate.
#                           _____           ____            _____
#     Leaf cells:       A--|     |      A--|    |       A--|     |
#     from netlist         | AND |--Y      | OR |--Y       | XOR |--Y
#     lab library       B--|_____|      B--|____|       B--|_____|
#
#
#     Embeded Modules created in this code:
#
#     Half-adder block:            Adder block:                           Co|
#         ____________________            __________________________________|__
#        |                    |          |                                  |  |
#        |          ______    |          |   _____       H1c         _____  |  |
#     A--|-----o---|      |   |       A--|--|     |-----------------|     | |  |
#        |     |   | AND1 |---|--C       |  | HA1 | H1s  _____  H2c | OR1 |-o  |
#        |  o------|______|   |       B--|--|_____|-----|     |-----|_____|    |
#        |  |  |              |          |              | HA2 |                |
#        |  |  |              |          |            o-|_____|----------------|--S
#        |  |  |    _____     |          |            |                        |
#        |  |  o---|      |   |          |            |_____________________   |
#        |  |      | XOR1 |---|--S       |__________________________________|__|
#     B--|--o------|______|   |                                             |
#        |                    |                                             |Ci
#        |____________________|
#
#
#     3-bit Top Module:
#
#               B0  A0           B1  A1           B2  A2
#                |  |             |  |             |  |
#        ________|__|_____________|__|_____________|__|______
#       |    ____|__|__       ____|__|__       ____|__|__    |
#       |   |    B  A  |     |    B  A  |     |    B  A  |   |
#       |   |          |     |          |     |          |   |
#       |   |   Add0   |     |   Add1   |     |   Add2   |   |
#       |   |          | C01 |          | C12 |          |   |
#  Ci---|---|Ci      Co|-----|Ci      Co|-----|Ci      Co|---|---Co
#       |   |___S______|     |___S______|     |___S______|   |
#       |_______|________________|________________|__________|
#               |                |                |
#               |                |                |
#               S0               S1               S2



our $mode_trunc ;
our $mode_read  ;
our $mode_edit  ;
our $visibleToBlock;

our $ns;
our $dump_occ_domain;

# ================================  M A I N  ================================

eval {
    $ns = new oa::oaNativeNS();

    $mode_trunc = "w";
    $mode_read  = "r";
    $mode_edit  = "a";
    $visibleToBlock = 1;



    my $lib_net_name  = "Lib";
    my $lib_mod_name  = "Lib3bitAdder";
    my $cellName      = "Lib";
    my $view_name     = "netlist";
    my $libModDir     = "Lib3bit";
    my $libDefsFile   = "lib.defs";


    # Perform logical lib name to physical path name mappings for the source
    # library created in the netlist/ lab, then the symbol library to be
    # created in this lab.
    my $strLibDefDef;
    oa::oaLibDefList::getDefaultPath( $strLibDefDef );

    if ( $strLibDefDef ne $libDefsFile ) {
        printf( "***Missing local %s file.\n", $libDefsFile );
        printf( "   Please create one with the one line shown below:\n ");
        printf( "       INCLUDE ../netlist/lib.defs\n");
        exit 4;
    }
    &oa::oaLibDefList::openLibs();

    my $lib_net_name_sc = new oa::oaScalarName( $ns, $lib_net_name);
    my $libNet = oa::oaLib::find($lib_net_name_sc);

    if ( ! $libNet ) {
	printf("***Either lib.defs file missing or netlist lab was not completed.");
        exit 1;
    }
    

    # Create (or open if this lab is being rerun) the output 3bit Module
    # library.
    #
    my $lib_mod_name_sc = new  oa::oaScalarName( $ns, $lib_mod_name );
    my $libMod = oa::oaLib::find($lib_mod_name_sc);

    if ( $libMod ) {
        printf( "LibMod opened via openLibs of %s\n", $strLibDefDef);
    } else {
        if ( oa::oaLib::exists($libModDir) ) {
            #
            # Directory there but DEFINE for it missing in lib.defs.
            #
            $libMod = oa::oaLib::open( $lib_mod_name_sc, $libModDir );
            printf( "Opened existing %s in directory %s\n", $lib_mod_name, $libModDir);
        } else {
            # No directory for LibMod.
            #
            $libMod = oa::oaLib::create( $lib_mod_name_sc, 
					 $libModDir,
					 $oa::oacSharedLibMode, 
					 "oaDMFileSys");
            printf( "Created new %s in directory  %s\n", $lib_mod_name, $libModDir );
        }
        # Add a LibDef for this new Module Lib to the lib.defs file
        #
        printf( "\nAppending new Lib Def List for %s->%s to LibDefs file = %s\n", 
	         $libMod->getName(), $libModDir, $strLibDefDef);

#>>>        my $ldl = oa::oaLibDefList::get( $strLibDefDef, "a");
#>>>	&oa::oaLibDef::create( $ldl, $libMod->getName(), $libModDir );
#>>>        $ldl->save();
#>>> oaLibDefList.get and oaLibDef::create - bug here somewhere - no append of new LibMod info.
#>>> See http://www.si2.org/openeda.si2.org/tracker/index.php?func=detail&aid=1336&group_id=78&atid=314
#>>> Finishing lab with plain perl append to lib.defs which is needed for oadump lab verification step.
        open(LIBDEFS, '>>./lib.defs');
 	my $str = "\nDEFINE Lib3bitAdder Lib3bit\nASSIGN Lib3bitAdder writePath Lib3bit\nASSIGN Lib3bitAdder libMode shared\n";
 	print LIBDEFS $str;
 	close(LIBDEFS);

    }

    # Pretend to "import" Verilog descriptions of design modules
    #
    #    module Adder3bit(Co, S0,S1,S2,S3, A0,A1,A2,A3, B0,B1,B2,B3, Ci);
    #      input A0,A1,A2,A3, B0,B1,B2,B3, Ci;
    #      output Co, S0,S1,S2,S3;
    #      FA Add0 (Co,S,A,B,Ci);
    #      FA Add1 (Co,S,A,B,Ci);
    #      FA Add2 (Co,S,A,B,Ci);
    #      FA Add3 (Co,S,A,B,Ci);
    #    endmodule
    #    module FullAdder(Co,S,A,B,Ci);
    #      input A,B,Ci;
    #      output Co,S;
    #      HA Ha1 (C,S,A,B);
    #      HA Ha2 (C,S,A,B);
    #      Or Or1 (Y,A,B);
    #    endmodule
    #    module HalfAdder(C,S,A,B);
    #      input A,B;
    #      output C,S;
    #      And And1 (Y,A,B);
    #      Xor Xor1 (Y,A,B);
    #    endmodule

    # Use the CreateAdder3bit function defined in this lab to
    # create design_3bit using the mapped names of the:
    #    - cell library (from the netlist lab) containing the leaf OR, AND, XOR gates
    #    - design library to hold the 3-bit Adder begin created in this lab
    #    - View name to be used for all the design containers created.
    my $design_3bit = &create_adder_3bit( $lib_net_name, $lib_mod_name, $view_name );

    # Save the design at this initial stage BEFORE any partitioning or
    # clustering is done to it.
    $design_3bit->save();

    $dump_occ_domain = 0;

    &dump_open_designs();

    &new_section("Detach FullAdderMod");

    # Locate the full-adder module by name in the Design
    my $mod_fa = oa::oaModule::find( $design_3bit, "FullAdderMod" );
    &assert(eval{$mod_fa->isValid()}, "mod_fa->isValid()");

    # Detach the full-adder Module hierarchy from Design 3bitAdder creating a new
    # Design in the same Lib, with the same View name, but a Cell name of "FAdetach".
    my $des_fa_detatch = $mod_fa->detach( $lib_mod_name, "FAdetach", $view_name );

    &dump_open_designs();

    &new_section("Embed Or");

    # Embed the top Module of the Or Design into the new FADetach Design.
    oa::oaModule::embed( $des_fa_detatch,
			 oa::oaDesign::open( $lib_net_name, "Or", $view_name, $mode_read));

    &dump_open_designs();

    $design_3bit->close();
    $des_fa_detatch->close();

    # NOTE!!!! Si2 C++ module lab (and this version of it) DOES NOT SAVE the des_fa_detatch design.
    # Note that the Lib3bit/FAdetach/netlist: will have a 0 length layout.oa
    # Attempts to oadump or emhlister dump this lib-cell-view will report
    # ***ERROR: Can't read Design Lib3bitAdder/FAdetach/netlist: \
    # File '..path../Lib3bit/FAdetach/netlist/layout.oa' does not exist, or is not a valid OpenAccess database.


    printf( "\n...........Normal Termination........\n" );

    return 0;
};

if ($@) {
    print STDERR "Caught exception: $@\n";
}

exit(0);

# ================================= SUBROUTINES  =============================

sub new_section ($) {
    my $id = shift;
    printf( "\n\n||||||||||||||||||||||||||||||||||||||||||| " );
    printf("%s", $id );
    printf( " |||||||||||||||||||||||||||||||||||||||||||\n\n" );
}


sub assert($$) {
    my $condition = shift;
    my $assert_msg = shift;
    printf("  ASSERT [");
    if ( $condition ) {
	printf("PASS");
    } else {
	printf("FAIL");
    }
    printf("] %s\n", $assert_msg);
    
}


##----------------------------------------------------------------------

sub get_mapped_name ($) {
    my $obj = shift;
    # Many of the getName functions allow you to enter
    #     $myObj getName
    # to get a simple string representing the Object's name.
    #
    # For those that don't, this function does the ScalarName setup and
    # retrieval, and returns the simple string.

    my $ns = new oa::oaNativeNS();
    my $scalar_name = new oa::oaScalarName($ns, " ");

    $obj->getName( $scalar_name );
    my $simple_string;
    $scalar_name->get($ns, $simple_string);
    return $simple_string;
}


sub get_lcv_name ($) {
    my $obj = shift;
    # Templatize a function that exploits the commonality of method names to
    # get the Lib/Cell/View name attributes from different types of containers.

    # Return a String that is the concatenation of the Lib/Cell/View names,
    # separated by the / character as a delimiter, using method[s] that are
    # are valid for all possible Inst types the template parameter might designate.
    my $string = sprintf("%s/%s/%s", $obj->getLibName(), $obj->getCellName(), $obj->getViewName() );
    return $string;
}


sub get_num_terms ($) {
    my $mod = shift;
    # Return the number of ModTerms in a Module.
    return $mod->getTerms()->getCount();
}


sub get_num_instTerms ($) {
    my $mod = shift;
    # Return the number of ModInstTerms in a Module.
    return $mod->getInstTerms()->getCount();
}


sub get_num_nets ($) {
    my $mod = shift;
    # Return the number of ModNets in a Module
    return $mod->getNets()->getCount();
}


sub create_modInstTerm_by_name ($$$) {
    my $inst = shift;
    my $name = shift;
    my $net  = shift;
    # Templatize a function that exploits the commonality of method names to
    # get the master Module of an inst is the same for different ModInst
    # classes
    # Tcl note: a template is not needed here.

    # Set the mod variable to the master Module of the Inst argument.
    my $mod = $inst->getMasterModule();

    # A Block-specific Design will not have a master Module, so verify
    # mod exists before trying to use the handle to find its ModTerm ons.
    #
    my $instTerm;

    if ( $mod ) {
        # Find the term with the name specified in the function argument.
        my $term = oa::oaModTerm::find( $mod, $name );

        # Verify the handle is not null {ie, there was a Term by that name}.
        #
        if ( $term->isValid() ) {

            # Set the instTerm variable to a ModInstTerm created from the term
            # handle.
            $instTerm = oa::oaModInstTerm::create( $net, $inst, $term );

	    my $msg = sprintf("Created InstTerm of \"%s\" on Inst \"%s\", attached to net \"%s\"\n",
			      $name, $inst->getName(), $net->getName() );
            log_it( $msg  );

            assert(eval{ $instTerm->isValid()} , "instTerm->isValid()" );

        } else {
            printf( "***Couldn't find term named '%s' on module. Quitting.", $name );
            exit 2;
        }
    }
    return $instTerm;
}


sub expect_modTerm_on_net ($$) {
    my $term = shift;
    my $net  = shift;
    # Create an oaCollection named coll_terms_on_net consisting of the
    # ModTerms on the ModNet passed as a function argument.
    my $coll_terms_on_net = $net->getTerms();

    assert(eval{ $coll_terms_on_net->getCount() == 1},
	   "coll_terms_on_net->getCount() == 1");

    # The only Term in the collection should be the one passed in as an arg.
    my $term_name = $term->getName();

    my $itIter = new oa::oaIter::oaModTerm($coll_terms_on_net);
    while (my $got_term = $itIter->getNext()) {
        assert(eval{$got_term->getName() eq $term_name},
	       "got_term->getName() eq term->getName()"); 
    }
}



sub create_modNet($$) {
    my $mod = shift;
    my $net_name = shift;
      # Create a ModScalarNet such that the following assertions pass and
      # a corresponding Net is automatically created in the Block Domain.
    my $net_scname= new oa::oaScalarName($ns,  $net_name );
    my $net = oa::oaModScalarNet::create( $mod, $net_scname);

    my ($type, $name);
    assert(eval{$net->isValid()}, "net->isValid()");
    assert(eval{$net->getModule()->getName() eq $mod->getName()},
	   "net->getModule()->getName() eq mod->getName()");
    assert(eval{$net->getName() eq $net_name}, "net->getName() eq net_name");

    $type = $net->getType();
    $name = (new oa::oaType($type))->getName();
    assert(eval{$name eq "ModScalarNet"},
	   'net->getType()->getName() eq "ModScalarNet"');

    $type = $net->getSigType();
    $name = (new oa::oaSigType($type))->getName();
    assert(eval{$name eq "signal"},
	   'net->getSigType()->getName() eq "signal"');

    assert(eval{!$net->isGlobal()}, "!net->isGlobal()");
    assert(eval{!$net->isImplicit()}, "!net->IsImplicit()");
    assert(eval{$net->isEmpty()}, "net->isEmpty()");
    assert(eval{$net->getEquivalentNets()->isEmpty()}, "net->getEquivalentNets()->isEmpty()");
    assert(eval{$net->getInstTerms()->isEmpty()}, "net->getInstTerms()->isEmpty()"); 
    assert(eval{$net->getTerms()->isEmpty()}, "net->getTerms()->isEmpty()"); 

    my $msg = sprintf("Created ModScalarNet '%s'\n", $net_name);
    log_it($msg);
    return $net;
}

sub add_modNet_term($$$) {
    my $mod  = shift;
    my $name = shift;
    my $io_dir = shift;
    # Use the create_modNet helper function defined above to create a ModScalarNet
    # using the passed arguments. Then create a ModScalarTerm (with the proper
    # OA API function) with attributes specified by the passed arguments, so that
    # appropriate Objects are propagated to the Block Domain and the assertions pass.
    my $net  = &create_modNet( $mod, $name );
    my $term = oa::oaModScalarTerm::create( $net, $name, $io_dir );

    my ($type, $tname);
    assert(eval{$net->getModule()->getName() eq $mod->getName()},
	   "net->getModule()->getName() eq mod->getName()");

    $type = $net->getType();
    $tname = (new oa::oaType($type))->getName();
    assert(eval{$tname eq "ModScalarNet"},
	   'net->getType()->getName() eq "ModScalarNet"');
    
    $type = $net->getSigType();
    $tname = (new oa::oaSigType($type))->getName();
    assert(eval{$tname eq "signal"},
	   'net->getSigType()->getName() eq "signal"');

    assert(eval{!$net->isGlobal()}, "!net->isGlobal()");
    assert(eval{$term->getModule()->getName() eq $mod->getName()},
	   "term->getModule()->getName() eq mod->getName()");

    $type = $term->getType();
    $tname = (new oa::oaType($type))->getName();
    assert(eval{$tname eq "ModScalarTerm"},
	   'term->getType()->getName() eq "ModScalarTerm"');

    assert(eval{$term->getTermType() eq $io_dir}, 
	   "term->getTermType() eq io_dir");
    assert(eval{$term->getNet()->getName() eq $net->getName()},
	   "term->getNet()->getName() eq net->getName()");

    my $msg = sprintf("Term '%s' created attached to Net '%s'\n", $term->getName(), $term->getNet()->getName() );
    log_it( $msg );

    &expect_modTerm_on_net( $term, $net );
    return $net;

}

sub create_modInst($$$$) {
    my $type = shift;
    my $mod  = shift;
    my $master = shift;
    my $inst_name = shift;
 
    # CPP implmenents a Templatized function that exploits the commonality of method names to
    # create Insts of different types. Changed to oaModModuleScalarInst and oaModModuleScalarInst   

    my $inst_scname = new oa::oaScalarName( $ns, $inst_name );

    # Create an Inst using the function arguments to specify its master
    # and its owner Module. Assign to the inst variable.
    my $inst = 0;


    if ($type eq "oaModModuleScalarInst") {
	$inst =  oa::oaModModuleScalarInst::create($mod, $master, $inst_scname);

    } elsif ($type eq "oaModScalarInst") {
	$inst =  oa::oaModScalarInst::create($mod, $master, $inst_scname);
	
    }

    my ($instType, $name, $msg);
    if ( $inst ) {
	assert(eval{ $inst->isValid()}, "inst->isValid()");
	$instType = $inst->getType();
	$name = (new oa::oaType($instType))->getName();
	$msg   = sprintf("Created %s '%s'\n", $name, $inst->getName() ); 
	log_it( $msg );
    }
    return $inst;
}



sub create_half_adder ($$$$) {
    my $design = shift; 
    my $cell_lib_name = shift;
    my $design_lib_name= shift;
    my $view = shift;

    # Create the "HalfAdder" Module in the specified design Lib and View
    # (passed as args), assigning it to the mod_ha variable.
    my $ha_scname = new oa::oaScalarName( $ns, "HalfAdderMod");
    my $mod_ha = oa::oaModule::create( $design, $ha_scname);

    # Open the "Xor" and "And" Designs from the cell Lib and View
    # (passed as args) in READ mode.
    my $cell_lib_scname= new  oa::oaScalarName( $ns, $cell_lib_name);
    my $xor_scname = new  oa::oaScalarName( $ns, "Xor");
    my $and_scname = new  oa::oaScalarName( $ns, "And");
    my $view_scname = new  oa::oaScalarName( $ns, $view);
    my $design_xor = oa::oaDesign::open($cell_lib_scname, 
					$xor_scname, 
					$view_scname, 
					$mode_read);
    my $design_and = oa::oaDesign::open($cell_lib_scname,
					$and_scname, 
					$view_scname, 
					$mode_read);

    eval {
        # Use the create_modInst helper function defined in this lab to
        # attempt to create a ModModuleInst in mod_ha of the top Module
        # of the Xor Design.
	&create_modInst("oaModModuleScalarInst", 
			$mod_ha,
			design_xor->getTopModule(), "Xor1q");
    };
    if ($@) {
        printf ("{As expected, cannot instantiate Modules from other Designs}\n");
    }

    # Use the create_modInst helper function to create ModInsts named "Xor1"
    # and "And1" of  master Designs Xor and And to be owned/contained by mod_ha.
    my $inst_xor = create_modInst("oaModScalarInst", $mod_ha, $design_xor, "Xor1");
    my $inst_and = create_modInst("oaModScalarInst", $mod_ha, $design_and, "And1");

    log_it( "Creating Design 'HA'.\n");
    add_indent();
    assert(eval{$mod_ha->isValid()}, "mod_ha->isValid()");

    my $netA = &add_modNet_term( $mod_ha, "A", $oa::oacInputTermType );
    my $netB = &add_modNet_term( $mod_ha, "B", $oa::oacInputTermType );
    my $netC = &add_modNet_term( $mod_ha, "C", $oa::oacOutputTermType );
    my $netS = &add_modNet_term( $mod_ha, "S", $oa::oacOutputTermType );

    assert(eval{get_num_nets($mod_ha) == 4},  "get_num_nets(mod_ha) == 4");
    assert(eval{get_num_terms($mod_ha) == 4}, "get_num_terms(mod_ha) == 4");

    # Call create_modInstTerm_by_name, defined above to create ModInstTerms on
    # inst_xor named "A", "B", "Y", connected to netA, netB, NetS, respectively.
    my $instTermXorA = create_modInstTerm_by_name( $inst_xor, "A", $netA);
    my $instTermXorB = create_modInstTerm_by_name( $inst_xor, "B", $netB);
    my $instTermXorY = create_modInstTerm_by_name( $inst_xor, "Y", $netS);

    # Call create_modInstTerm_by_name, defined above, to create ModInstTerms on
    # inst_and named "A", "B", "Y", connected to netA, netB, NetC, respectively.
    my $instTermAndA = create_modInstTerm_by_name( $inst_and, "A", $netA );
    my $instTermAndB = create_modInstTerm_by_name( $inst_and, "B", $netB );
    my $instTermAndY = create_modInstTerm_by_name( $inst_and, "Y", $netC );

    assert(eval{get_num_instTerms( $mod_ha ) == 6},
	   "get_num_instTerms(mod_ha) == 6");
    sub_indent();
    return $mod_ha;
}
# ---------- Create Design with Modules and Blocks

sub create_design ($$$) {
    my $design_lib_name = shift;
    my $cell = shift;
    my $view = shift;

    # Create a Design with the names passed as arguments, maskLayout ViewType,
    # overwriting any Design of those names/type that might already exist.
    my $design_lib_scname = new oa::oaScalarName($ns, $design_lib_name);
    my $cell_scname= new oa::oaScalarName($ns, $cell);
    my $view_scname= new oa::oaScalarName($ns, $view);

    my $design = oa::oaDesign::open( $design_lib_scname, 
				     $cell_scname, 
				     $view_scname,
				     oa::oaViewType::get($oa::oacMaskLayout),
				     "w");

    assert(eval{ $design->isValid()}, "design->isValid");
    assert(eval{ $design->getRefCount() == 1}, "design->getRefCount == 1");


    my $type = $design->getType();
    my $name = (new oa::oaType($type))->getName();
    assert(eval{ $name eq "Design"},
	   'design->getType()->getName() eq "Design"');

    assert(eval{ $design->getViewType()->getName() eq "maskLayout"},
	   'design->getViewType()->getName() eq "maskLayout"');

    # Assign design a "block" CellType attribute value.
    $design->setCellType( $oa::oacBlockCellType );

    assert(eval{$design->getCellType() == $oa::oacBlockCellType},
	   "design->getCellType() == oa::oacBlockCellType");

    return $design;
}


sub create_full_adder ($$$$) {
    my $design = shift;
    my $cell_lib_name = shift;
    my $design_lib_name= shift;
    my $view = shift;
    # Open the existing design named "Or" in the CellLib in read mode.

    my $cell_lib_scname = new oa::oaScalarName($ns, $cell_lib_name);
    my $design_lib_scname= new oa::oaScalarName($ns, $design_lib_name);
    my $view_scname= new oa::oaScalarName($ns, $view);
    my $design_name=new oa::oaScalarName($ns, "Or");

    my $design_or = oa::oaDesign::open($cell_lib_scname, $design_name, $view_scname, $mode_read);

    # Create an embedded mod_fa FullAdder module owned by the Design passed as an argument.
    my $fa_scname = new oa::oaScalarName($ns, "FullAdderMod");
    my $mod_fa = oa::oaModule::create($design, $fa_scname );

    # Call the CreateHalfAdder utility function to create the mod_ha module, also owned by
    # the design, passing along the mapped names of the cell and design libraries, and View.
    my $mod_ha = create_half_adder( $design, $cell_lib_name, $design_lib_name, $view);

    
    # Call the CreateModInst utility function defined above to create in
    # the mod_fa parent an Inst named "Ha1" and one named "Ha2 of mod_ha.
    my $inst_ha1 = create_modInst("oaModModuleScalarInst", $mod_fa,$mod_ha , "Ha1");
    my $inst_ha2 = create_modInst("oaModModuleScalarInst", $mod_fa, $mod_ha, "Ha2");

    # Call the CreateModInst{} utility function defined above to create in
    # the mod_fa parent an Inst named "Or1" of design_or.
    my $inst_or1 = create_modInst("oaModScalarInst", $mod_fa, $design_or, "Or1");

    #  Create nets and terminals for each I/O on this block.
    #
    my $netA  = add_modNet_term( $mod_fa, "A",  $oa::oacInputTermType );
    my $netB  = add_modNet_term( $mod_fa, "B",  $oa::oacInputTermType );
    my $netCi = add_modNet_term( $mod_fa, "Ci", $oa::oacInputTermType );
    my $netCo = add_modNet_term( $mod_fa, "Co", $oa::oacOutputTermType );
    my $netS  = add_modNet_term( $mod_fa, "S",  $oa::oacOutputTermType );
      #
      # Create internal nets
      #
    my $netH1c = create_modNet( $mod_fa, "H1c");
    my $netH1s = create_modNet( $mod_fa, "H1s");
    my $netH2c = create_modNet( $mod_fa, "H2c");
    assert(eval{get_num_terms( $mod_fa ) == 5},
	   "get_num_terms( mod_fa ) == 5");

    add_indent();

    my ($modInst, $itype, $itype_name,  $htype, $htype_name);
    # Create an iterator that collects all the ModuleInsts in mod_fa
    my $itIter = new oa::oaIter::oaModInst($mod_fa->getInsts());
    while ($modInst = $itIter->getNext()) {
	$itype = $modInst->getType();
	$itype_name = (new oa::oaType($itype))->getName();
	$htype = $modInst->getHeader()->getType();
        $htype_name= (new oa::oaType($htype))->getName();
        printf( "  modInst '%s' type=%s InstHeader type=%s\n",
		$modInst->getName(), $itype_name, $htype_name); 
    }

    # Create an iterator that collects all the ModuleInstHeaders in mod_fa
    $itIter = new oa::oaIter::oaModModuleInstHeader($mod_fa->getModuleInstHeaders());
    while (my $mmiHeader = $itIter->getNext()) {
        printf( "     modModuleInstHeader %s\n", get_mapped_name( $mmiHeader) );
	
        # Create an iterator that collects all the Insts in the Header
	my $itIterN = new oa::oaIter::oaModInst($mmiHeader->getInsts());
	while ($modInst = $itIterN->getNext()) {
            printf( "       modInst '%s'\n", $modInst->getName() );
	}
    }
    
  # Create ModInstTerms on the HalfAdder Insts.

    create_modInstTerm_by_name( $inst_ha1, "A", $netA);
    create_modInstTerm_by_name( $inst_ha1, "B", $netB);
    create_modInstTerm_by_name( $inst_ha1, "C", $netH1c);
    create_modInstTerm_by_name( $inst_ha1, "S", $netH1s);

    create_modInstTerm_by_name( $inst_ha2, "A", $netH1s);
    create_modInstTerm_by_name( $inst_ha2, "B", $netCi);
    create_modInstTerm_by_name( $inst_ha2, "C", $netH2c);
    create_modInstTerm_by_name( $inst_ha2, "S", $netS);

    # Create ModInstTerms on the Or Insts.

    create_modInstTerm_by_name( $inst_or1, "A", $netH1c);
    create_modInstTerm_by_name( $inst_or1, "B", $netH2c);
    create_modInstTerm_by_name( $inst_or1, "Y", $netCo);

    sub_indent();
    return $mod_fa;
}


sub create_adder_3bit ($$$) {
    my $cell_lib_name = shift;
    my $design_lib_name = shift; 
    my $view = shift;
    # Create the top level 3-bit adder Design using the arguments passed in.
    #
    my $design_3bit = &create_design( $design_lib_name, "Adder3bit", $view);

    # Create a Module named "Adder3bitMod" in the newly created Design.
    my $mod_3bit = oa::oaModule::create( $design_3bit, "Adder3bitMod");

    # Set the module created as the topModule BEFORE creating more hierarchy
    # (or it won't propagate to Block domain).
    $design_3bit->setTopModule( $mod_3bit, $visibleToBlock);

    # Create mod_fa, a FullAdder Module in the 3bit design.
     #
    my $mod_fa = create_full_adder( $design_3bit, $cell_lib_name, $design_lib_name, $view);

    # Use the create_ModInst helper function to create inside design_3bit's top Module,
    # three ModInsts of the mod_fa FullAdder Module just created above. Name them "Fa0",
    # "Fa1", and "Fa2". {Use the schematic drawing at the top of this file as a guide.end
    my $mmi_adder0 = create_modInst("oaModModuleScalarInst", $mod_3bit, $mod_fa, "Fa0");
    my $mmi_adder1 = create_modInst("oaModModuleScalarInst", $mod_3bit, $mod_fa, "Fa1");
    my $mmi_adder2 = create_modInst("oaModModuleScalarInst", $mod_3bit, $mod_fa, "Fa2");

    # Create in the top module of design_3bit each of the ModScalarNets declared above
    # by calling the add_modNet_term helper function defined above, passing in as
    # the name the last two characters of the corresponding Net name.
    # For example netA0 {and the Term created for it by the helperend
    # should be named "A0", and so on.  All "S*" and "Co" Terms should
    # be Output types, with the "A*", "B*", and "Ci" Terns as Input types.
    # {Use the schematic drawing at the top of this file as a guide.end
    my $netA0 = add_modNet_term( $mod_3bit, "A0", $oa::oacInputTermType);
    my $netB0 = add_modNet_term( $mod_3bit, "B0", $oa::oacInputTermType);
    my $netS0 = add_modNet_term( $mod_3bit, "S0", $oa::oacOutputTermType);
    my $netA1 = add_modNet_term( $mod_3bit, "A1", $oa::oacInputTermType);
    my $netB1 = add_modNet_term( $mod_3bit, "B1", $oa::oacInputTermType);
    my $netS1 = add_modNet_term( $mod_3bit, "S1", $oa::oacOutputTermType);
    my $netA2 = add_modNet_term( $mod_3bit, "A2", $oa::oacInputTermType);
    my $netB2 = add_modNet_term( $mod_3bit, "B2", $oa::oacInputTermType);
    my $netS2 = add_modNet_term( $mod_3bit, "S2", $oa::oacOutputTermType);
    my $netA3 = add_modNet_term( $mod_3bit, "A3", $oa::oacInputTermType);
    my $netB3 = add_modNet_term( $mod_3bit, "B3", $oa::oacInputTermType);
    my $netS3 = add_modNet_term( $mod_3bit, "S3", $oa::oacOutputTermType);
    my $netCi = add_modNet_term( $mod_3bit, "Ci", $oa::oacInputTermType);
    my $netCo = add_modNet_term( $mod_3bit, "Co", $oa::oacOutputTermType);

    # Create in the top module of design_3bit two ModScalarNets declared above
    # by calling the CreateModNet{} helper function defined above, passing in
    # as the names "C01" and "C02", respectively.
    my $netC01 = create_modNet( $mod_3bit, "C01");
    my $netC12 = create_modNet( $mod_3bit, "C12");

    # Call the CreateModInstTermByName{} helper function defined above
    # to create ModInstTerms on each of the three FullAdder Insts for all
    # master Terms, connecting each to the corresponding Net created above.
    # {Use the schematicdrawing at the top of this file as a guide.end
    #
    my $itAdd0A  = create_modInstTerm_by_name( $mmi_adder0, "A",  $netA0);
    my $itAdd0B  = create_modInstTerm_by_name( $mmi_adder0, "B",  $netB0);
    my $itAdd0S  = create_modInstTerm_by_name( $mmi_adder0, "S",  $netS0);
    my $itAdd0Ci = create_modInstTerm_by_name( $mmi_adder0, "Ci", $netCi);
    my $itAdd0Co = create_modInstTerm_by_name( $mmi_adder0, "Co", $netC01);
    my $itAdd1A  = create_modInstTerm_by_name( $mmi_adder1, "A",  $netA1);
    my $itAdd1B  = create_modInstTerm_by_name( $mmi_adder1, "B",  $netB1);
    my $itAdd1S  = create_modInstTerm_by_name( $mmi_adder1, "S",  $netS1);
    my $itAdd1Ci = create_modInstTerm_by_name( $mmi_adder1, "Ci", $netC01);
    my $itAdd1Co = create_modInstTerm_by_name( $mmi_adder1, "Co", $netC12);
    my $itAdd2A  = create_modInstTerm_by_name( $mmi_adder2, "A",  $netA2);
    my $itAdd2B  = create_modInstTerm_by_name( $mmi_adder2, "B",  $netB2);
    my $itAdd2S  = create_modInstTerm_by_name( $mmi_adder2, "S",  $netS2);
    my $itAdd2Ci = create_modInstTerm_by_name( $mmi_adder2, "Ci", $netC12);
    my $itAdd2Co = create_modInstTerm_by_name( $mmi_adder2, "Co", $netCo);

    return $design_3bit;
}


1;
