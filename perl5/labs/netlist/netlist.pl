#! /usr/bin/env perl
#-------------------------------------------------------------------------------
#
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use
# this file except in compliance with the License. You may obtain a copy of the
# License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed
# under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
# CONDITIONS OF ANY KIND, either express or implied. See the License for the
# specific language governing permissions and limitations under the License.
#
#-------------------------------------------------------------------------------
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   08/16/10  1.0      bpfeil, Si2      Si2 Tutorial 10thEd labs - Perl version
#
#-------------------------------------------------------------------------------
#
# Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working qGroup, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
#-------------------------------------------------------------------------------


use strict;
use warnings;
use File::Basename;
use FindBin;

#
## Path to bindings. Add to PERL5LIB env variable OR here
#use lib "path-to-bindings/amd-init/perl5";

use lib dirname($FindBin::RealBin);
use oa::design;

our $ns;
our $_d;

our %globs =   ('MODE_WRITE',    'w',
              'chpNameLib',    'Lib',
              'chpPathLib',    './Lib',
              'chpNameView',   'netlist' );

eval {

    oa::oaDesignInit( $oa::oacAPIMajorRevNumber,
		      $oa::oacAPIMinorRevNumber,
		      $oa::oacDataModelRevNumber );
    
    $ns = new oa::oaNativeNS;
    
    # No point in trying oaLib::find() since in this simple program there
    # are no other threads or functions that might have opened the Lib alrready.
    # However, the program may have executed already, in which case a
    # Lib directory will exist and attempting create again on it will hurl.
    #    
    my $libnamestr = $globs{ 'chpNameLib' };
    my $pathLib = $globs{ 'chpPathLib' };
    
    
    my $libName  = new oa::oaScalarName($ns, $libnamestr);
    my $cellName = new oa::oaScalarName($ns, 'somecell' );
    my $viewName = new oa::oaScalarName($ns, 'netlist' );
    
    if (  oa::oaLib::exists( $pathLib ) ) {
	printf( "Opening existing Lib name=%s mapped to filesystem path=%s\n", $libnamestr, $pathLib );
	
	# Open an existing Lib using the scNameLib and chpPathLib from globs.
	# begin_skel
	my $lib = oa::oaLib::open( $libName, $pathLib );
	# end_skel
    } else {
	printf( "Creating new Library for Lib name=%s mapped to filesystem path=%s\n", $libnamestr, $pathLib );   
	
	# Create a new Lib in shared mode using the DMFileSys PlugIn.
	# begin_skel
	my $lib = oa::oaLib::create( $libName, $pathLib , new oa::oaLibMode($oa::oacSharedLibMode), 'oaDMFileSys');
	# end_skel
    }
    printf( "Overwriting lib.defs file.\n" );
    
    # Create/overwrite a lib.defs file in the current directory,
    # that has a library definition mapping scNameLib to chpPathLib.
    # begin_skel
    my $ldl = oa::oaLibDefList::get( "lib.defs", "w" );
    &oa::oaLibDef::create( $ldl, $libName, $pathLib );
    $ldl->save();
    # end_skel
    
    # Create the "cell library".
    #
    
    &makeLeafCell( "And" );
    &makeLeafCell( "Or" );
    &makeLeafCell( "Xor" );
		
    # Create the design hierarchy.
    #
    &makeHalfAdder();
    &makeFullAdder();

};

if ($@) {
    print STDERR "Caught exception: $@\n";
}

exit(0);

#-------------------------------------------------------------------------------

sub makeInstTermConnectToNet ($$$) {
    my $strNameTerm = shift;
    my $inst = shift;
    my $net  = shift;
    # Create an InstTerm of the Term named strNameTerm
    # (using the NameSpace in the globs singleton) on the Inst and connected to
    # the Net specified by the input arguments.
    # begin_skel
    my $nameTerm = new oa::oaName( $ns , $strNameTerm );
    oa::oaInstTerm::create( $net, $inst, $nameTerm );
    # end_skel
}


# Declare a proc that takes a path argument and returns true if a Lib exists
# at that path; false otherwise
# begin_skel
sub libAlreadyOnDisk ($) {
    my $path = shift;
    if (-e $path) {
	return 1;
    } else {
	return 0;
    }
}
# end_skel



sub getTermName ($) {
    my $term = shift;
    
    my $name = "";
    if ( !$term) { return $name; }
    
    
    # Return a String representation for the term's name.
    # Use the NameSpace from the global_data singleton.
    # begin_skel
    $term->getName( $ns, $name );
    
    return $name;
    # end_skel
}

sub getInstTermName ($) {
    my $instTerm = shift;

    my $name = "";
    if ( !$instTerm ) { return $name; }
    
    # Return a name of the form, Instname.Termname, that is, the name
    # of InstTerm's Inst, followed by a ".", followed by the Term's name.
    # Use the NameSpace from the global_data singleton.
    # begin_ske
    my $instname;
    $instTerm->getInst()->getName( $ns, $instname );
    my $termname;
    $instTerm->getTerm()->getName( $ns, $termname );
    $name = $instname .".". $termname;
    return $name;
    # end_skel
}



sub printNets ($) {
    my $block = shift;

    # Print the names of all Nets in the specified Block. For each Net,
    # print the Term names of all Terms and InstTerms attached to it.
    
    if (! $block ) { return; }
    
    # Using the NameSpace from the globals singleton, get the Cell name of
    # the Block's owner Design in the cellName variable.
    # begin_skel
    my $cellName;
    $block->getDesign()->getCellName( $ns , $cellName );
    # end_skel
    
    printf("\n");
    printf("The following Nets exist in Cell: %s\n",$cellName);
    
    
    # Create an iterator for the Nets in the Block
    # begin_skel
    my $nets = $block->getNets();
    my $netIter   = new oa::oaIter::oaNet($block->getNets());
    # end_skel
    
    # Create a while statement that gets the next Net in the iterator. Be sure
    # to include the opening curly bracket for the subsequent block of code.
    # begin_skel
    while ( my $nextNet = $netIter->getNext() ) {
	# end_skel
	
	# Get the name of the Net into the netName variable.
	# (HINT: Map via the namespace from the globals singleton.)
	# begin_skel
	my $netName;
	$nextNet->getName( $ns, $netName );
	
	# end_skel
	
	my $termNames =  "";
	
	# Create an if condition to test that there is at least one Term on the Net.
	# Be sure to include the opening curly bracket for the subsequent block of code.
	# begin_skel
	my $terms = $nextNet->getTerms();
	if ( $terms ) {
	    # end_skel
	    
	    # Create an iterator for the Terms connected to nextNet.
	    # begin_skel
	    my $iterTerms = new oa::oaIter::oaTerm($nextNet->getTerms());
	    #set terms [ $nextNet getTerms ]
	    #set iterTerms [ oa::oaIter_oaTerm $terms ]
	    # end_skel
	    
	    # Create a while condition that gets the next Term in the iterator. Be sure
	    # to include the opening curly bracket for the subsequent block of code.
	    # begin_skel
	    while ( my $nextTerm = $iterTerms->getNext() ) {
		# end_skel
		$termNames .= getTermName($nextTerm )."\t";
	    }
	}
	#printf (" %s ", $termNames);
	
	# Create an if condition to test that there is at least one InstTerm connected to the Net.
	# Be sure to include the opening curly bracket for the subsequent block of code.
	# begin_skel
	my $iTerms = $nextNet->getInstTerms();
	if ( $iTerms ) {
	    # end_skel
	    
	    # Create an iterator for the InstTerms attached to nextNet.
	    # begin_skel
	    my $iterInstTerms = new oa::oaIter::oaInstTerm($nextNet->getInstTerms);
	    # end_skel
	    
	    # Create a while condition that gets the next InstTerm in the iterator. Be sure
	    # to include the opening curly bracket for the subsequent block of code.
	    # begin_skel
	    while ( my $nextInstTerm =  $iterInstTerms->getNext()  ) {
		# end_skel
		$termNames .= getInstTermName( $nextInstTerm ) ."\t";
	    }
	}
	printf("Net: %s is connected to Term: %s\n", $netName, $termNames );
    }
    printf("\n");
}




sub makeNetAndTerm ($$$) {
    #my ($block, $chNameTerm, $termType) = @_;
    my $block = shift;
    my $chNameTerm = shift;
    my $termType = shift;
    
    # Create a Net allowing the database to assign a name to it.
    # Create a Term connected to that Net with the name and type passed in
    # as an args. Use the NameSpace from the global_data singleton.
    # begin_skel
    my $net = oa::oaScalarNet::create( $block  );
    my $termName = new oa::oaScalarName( $ns, $chNameTerm );
    oa::oaScalarTerm::create( $net, $termName, $termType );
    # end_skel

    return $net;
}


sub makeLeafCell ($) {
    my $strNameCell = shift;
    
    # Create a netlist type Design with the mode that will overwrite any
    # existing Design of that type and names.
    # (HINT: Use global_data namespace variables to help build the scalar names 
    # and other needed arguments.)
    # begin_skel
    
    my $scNameCell = new oa::oaScalarName( $ns, $strNameCell );
    my $scNameLib = new oa::oaScalarName( $ns, $globs{'chpNameLib'} );
    my $scNameView = new oa::oaScalarName( $ns, $globs{'chpNameView'} );
    my $view_type = oa::oaViewType::get(new oa::oaReservedViewType('netlist'));
    
    my $design = oa::oaDesign::open( $scNameLib, $scNameCell, $scNameView, $view_type, 'w' );
    # end_skel
    
    # Create the top Block in the Design to own the design objects.
    # begin_skel
    my $block = oa::oaBlock::create( $design );
    # end_skel
    
    # Create the Nets and Terms for the leaf gate.
    #
    my $termTypeInput = new oa::oaTermType( $oa::oacInputTermType  );
    &makeNetAndTerm( $block, "A", $termTypeInput);
    &makeNetAndTerm( $block, "B", $termTypeInput);
    
    my $termTypeOutput = new oa::oaTermType( $oa::oacOutputTermType  );
    &makeNetAndTerm( $block, "Y", $termTypeOutput ) ;
    
    &printNets( $block );
    
    
    $design->save();
    $design->close();
    
}

sub makeHalfAdder () {

    # Open a netlist type Design and name "HalfAdder" with the mode that will
    # overwrite any existing Design of that type and name.
    # begin_skel
    my $scNameCell = new oa::oaScalarName( $ns, "HalfAdder" );
    my $scNameLib = new oa::oaScalarName( $ns, $globs{'chpNameLib'} );
    my $scNameView = new oa::oaScalarName( $ns, $globs{'chpNameView'} );
    my $view_type = oa::oaViewType::get(new oa::oaReservedViewType('netlist'));
    my $mode = $globs{'MODE_WRITE'};
    my $designHA = oa::oaDesign::open( $scNameLib, $scNameCell, $scNameView, $view_type, $mode );
    # end_skel
    
    # Create a top Block for HalfAdder Design.
    # begin_skel
    my $blockHA = oa::oaBlock::create( $designHA );
    # end_skel
    
    printf("Creating HalfAdder with 1 And Inst and 1 Xor Inst\n");
    
    # Create the 4 Terms on this halfadder: A and B are inputs, C and S are outputs.
    my $termTypeInput = new oa::oaTermType( $oa::oacInputTermType  );
    my $termTypeOutput = new oa::oaTermType( $oa::oacOutputTermType  );
    
    my $A_Net=&makeNetAndTerm( $blockHA, "A", $termTypeInput);
    my $B_Net=&makeNetAndTerm( $blockHA, "B", $termTypeInput);
    
    my $C_Net=&makeNetAndTerm( $blockHA, "C", $termTypeOutput ) ;
    my $S_Net=&makeNetAndTerm( $blockHA, "S", $termTypeOutput ) ;
    
    # oaTransform trans is necessary for and Inst even though we don't
    # care about placement at this point in the netlist.
    #
    my $origin_x = 0;
    my $origin_y = 0;

    my @transform =  ( $origin_x, $origin_y,  $oa::oacR0  );

    # Create a scalar Inst named "And1" with the master "And" using the
    # method that takes lib/cell/view names of the master.
    # begin_skel
    my $scNameAnd =  new oa::oaScalarName( $ns,  "And" );
    my $scNameAnd1 =  new oa::oaScalarName( $ns,  "And1" );
    my $and1 = oa::oaScalarInst::create( $blockHA, $scNameLib, $scNameAnd, $scNameView, $scNameAnd1, \@transform);
    # end_skel
    
    # Create by names a scalar Inst named "Xor1" with the master "Xor".
    # of the master.
    # begin_skel
    my $scNameXor = new oa::oaScalarName( $ns, 'Xor' );
    my $scNameXor1= new oa::oaScalarName( $ns, "Xor1" );
    my $xor1 =  oa::oaScalarInst::create( $blockHA, $scNameLib, $scNameXor, $scNameView, $scNameXor1, \@transform);
    # end_skel
    
    # Create and hook the InstTerms of And1 and Xor1 to the right Nets.
    &makeInstTermConnectToNet( "A", $and1, $A_Net);
    &makeInstTermConnectToNet( "B", $and1, $B_Net);
    &makeInstTermConnectToNet( "Y", $and1, $C_Net);
    &makeInstTermConnectToNet( "A", $xor1, $A_Net);
    &makeInstTermConnectToNet( "B", $xor1, $B_Net);
    &makeInstTermConnectToNet( "Y", $xor1, $S_Net);
    
    &printNets( $blockHA );
    $designHA->save();
    $designHA->close();
    
}

sub makeFullAdder () {
    # Create a 'netlist' type Design and name "FullAdder" with the mode that
    # will overwrite any existing Design of that type and name.
    # begin_skel
    my $scNameCell = new oa::oaScalarName( $ns, "FullAdder" );
    my $scNameLib  = new oa::oaScalarName( $ns, $globs{'chpNameLib'} );
    my $scNameView = new oa::oaScalarName( $ns, $globs{'chpNameView'} );
    my $view_type  = oa::oaViewType::get(new oa::oaReservedViewType('netlist'));
    my $mode = $globs{'MODE_WRITE'};
    my $designFA = oa::oaDesign::open( $scNameLib, $scNameCell, $scNameView, $view_type, $mode );
    
    # end_skel
    
    # Create a top Block for FullAdder Design.
    # begin_skel
    my $blockFA = oa::oaBlock::create( $designFA );
    # end_skel
    
    printf("Creating FullAdder with two HalfAdder Insts, one Or Inst\n");
    
    my $termTypeInput = new oa::oaTermType( $oa::oacInputTermType  );
    my $termTypeOutput = new oa::oaTermType( $oa::oacOutputTermType  );
    
    my $A_Net  = &makeNetAndTerm ( $blockFA, "A",  $termTypeInput  );
    my $B_Net  = &makeNetAndTerm ( $blockFA, "B",  $termTypeInput  );
    my $Ci_Net = &makeNetAndTerm ( $blockFA, "Ci", $termTypeInput  );
    my $Co_Net = &makeNetAndTerm ( $blockFA, "Co", $termTypeOutput );
    my $S_Net  = &makeNetAndTerm ( $blockFA, "S",  $termTypeOutput );
    
    # Create internal Nets SA_Net, CA_Net, CB_Net, using the Net create
    # method that lets the implementation assign the Net name.
    # begin_skel
    my $SA_Net = oa::oaScalarNet::create( $blockFA ); 
    my $CA_Net = oa::oaScalarNet::create( $blockFA );
    my $CB_Net = oa::oaScalarNet::create( $blockFA );
    # end_skel
    
    # oaTransform trans is necessary to create the Inst even though
    # placement at this netlist stage is irrelevant. Create a null
    # Transform that neither translates nor rotates.
    #
    my $origin_x = 0;
    my $origin_y = 0;

    my @transform =  ( $origin_x, $origin_y,  $oa::oacR0  );
    
    # Create two ScalarInsts "Ha1" and "Ha2" of master "HalfAdder"
    # and one ScalarInst "Or1" of master "Or" using the create method that 
    # takes the lib/cell/view names of the master.
    # begin_skel
    my $scNameInstCellHA = new oa::oaScalarName( $ns,  "HalfAdder" );
    
    my $scNameInstNameHa1 = new oa::oaScalarName( $ns, "Ha1" );
    my $instHa1 = oa::oaScalarInst::create( $blockFA, $scNameLib, $scNameInstCellHA, $scNameView, 
					    $scNameInstNameHa1, \@transform);
    
    my $scNameInstNameHa2 = new oa::oaScalarName( $ns, "Ha2" );
    my $instHa2 = oa::oaScalarInst::create( $blockFA, $scNameLib, $scNameInstCellHA, $scNameView, 
					    $scNameInstNameHa2, \@transform );
    
    my $scNameInstCellOr  = new oa::oaScalarName( $ns, "Or"  );
    my $scNameInstNameOr1 = new oa::oaScalarName( $ns, "Or1" );
    my $instOr1 = oa::oaScalarInst::create( $blockFA, $scNameLib, $scNameInstCellOr, $scNameView, 
					    $scNameInstNameOr1, \@transform );
    
    # end_skel
    
    # Create and hook the InstTerms to the right Nets.
    #
    &makeInstTermConnectToNet( "A", $instHa1, $A_Net );
    
    
    &makeInstTermConnectToNet( "B", $instHa1, $B_Net );
    &makeInstTermConnectToNet( "C", $instHa1, $CA_Net );
    &makeInstTermConnectToNet( "S", $instHa1, $SA_Net );
    
    &makeInstTermConnectToNet( "A", $instHa2, $SA_Net );
    &makeInstTermConnectToNet( "B", $instHa2, $Ci_Net );
    &makeInstTermConnectToNet( "C", $instHa2, $CB_Net );
    &makeInstTermConnectToNet( "S", $instHa2, $S_Net );
    
    &makeInstTermConnectToNet( "A", $instOr1, $CA_Net );
    &makeInstTermConnectToNet( "B", $instOr1, $CB_Net );
    &makeInstTermConnectToNet( "Y", $instOr1, $Co_Net );
    
    &printNets( $blockFA );
    $designFA->save();
    $designFA->close();
    
}

1;


