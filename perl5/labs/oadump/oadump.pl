#!/usr/bin/perl

#-------------------------------------------------------------------------------
#
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use
# this file except in compliance with the License. You may obtain a copy of the
# License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed
# under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
# CONDITIONS OF ANY KIND, either express or implied. See the License for the
# specific language governing permissions and limitations under the License.
#
#-------------------------------------------------------------------------------
#
# Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
#-------------------------------------------------------------------------------


use strict;
use warnings;
use File::Basename;
use FindBin;

use lib dirname($FindBin::RealBin);
use oa::design;

our $ns;
our $_d;

eval {
    oa::oaDesignInit( $oa::oacAPIMajorRevNumber,
		      $oa::oacAPIMinorRevNumber,
		      $oa::oacDataModelRevNumber );

    if (scalar(@ARGV) < 3) {
	printf(" Use : \n\toadump.pl  libraryName cellName viewName\n");
	exit 1;
    }

    $ns = new oa::oaNativeNS;

    my ($libstr, $cellstr, $viewstr, $cvType) = @ARGV;

    my $pathLibDefs = "./lib.defs";
    if (! -e $pathLibDefs) {
	printf( "***Must have %s file.\n", $pathLibDefs );
	exit 1;
    }

    &oa::oaLibDefList::openLibs();
    my $lib;
    if (!($lib = oa::oaLib::find($libstr))) {
	printf( "***Either Lib name=%s or its path mapping in file=%s is invalid.\n",
		$libstr, $pathLibDefs );
	exit 2;
    }

    my $design;
    eval {
	if ($cvType) {
	    $design = oa::oaDesign::open($libstr, $cellstr, $viewstr, oa::oaViewType::find($cvType), "r");
	} else {
	    $design = oa::oaDesign::open($libstr, $cellstr, $viewstr, "r");
	}
    };

    if ($@) {
	printf("Can't read Design %s/%s/%s (check lib.defs)\n",
	       $libstr, $cellstr, $viewstr);
	printf("\t%s\n", $@);
	exit 1;
    }

    printf( "Reading Design %s/%s/%s\n", $libstr, $cellstr, $viewstr);

    $_d = new dumper($cellstr, $viewstr);

    my $block = $design->getTopBlock();
    $design->openHier(200);
    &dumpCV($block, $design, $lib);
    &dumpInst($block);
    &dumpNet($block);
    &dumpShape($block, $design);
    &dumpRoute($block);
};

if ($@) {
    printf("Error: %s\n", $@);
    exit 1;
}

$_d->closeLog();

&sortDumpFile();

exit(0);

#-------------------------------------------------------------------------------

sub dumpCV ($$$) {
    my ($block, $design, $lib) = @_;
    my ($vtStr, $lstr, $cstr, $vstr);
  
    $design->getLibName ($ns, $lstr);
    $design->getCellName($ns, $cstr);
    $design->getViewName($ns, $vstr);
    &dumper::log( "Design %s/%s/%s ", $lstr, $cstr, $vstr);
  
    my $ctype = new oa::oaCellType($design->getCellType());
    my $vtype = $design->getViewType();
    $vtype->getName($vtStr);
    &dumper::log("%s %s ", $vtStr, $ctype->getName());

    my $bbox;
    $block->getBBox($bbox);
    &dumper::log( "BBOX ((%d, %d) (%d, %d))\n", @$bbox);
}

sub dumpInst ($) {
    my $block = shift;

    my $iIter = new oa::oaIter::oaInst($block->getInsts());
    while (my $inst = $iIter->getNext()) {
	my ($iStr, $origin, $orient, $bbox, $libStr, $cellStr, $viewStr);
	$inst->getName($ns, $iStr);
	$inst->getOrigin($origin);
	$orient = new oa::oaOrient($inst->getOrient());
	$inst->getBBox($bbox);
	$inst->getLibName ($ns, $libStr );
	$inst->getCellName($ns, $cellStr);
	$inst->getViewName($ns, $viewStr);
	
	&dumper::log( "Inst %s OF %s/%s/%s ", $iStr, $libStr, $cellStr, $viewStr);
	&dumper::log( "%s LOC (%d, %d) BBOX ((%d, %d), (%d, %d))\n", $orient->getName(), @$origin, @$bbox);

	my $itIter = new oa::oaIter::oaInstTerm($inst->getInstTerms());
	while (my $iterm = $itIter->getNext()) {
	    my ($tStr, $nStr);
	    $iterm->getTermName($ns, $tStr);
	    my $net = $iterm->getNet();
	    if ($net) { 
		$net->getName($ns, $nStr);
	    } else {
		$nStr = "<none>";
	    }

	    &dumper::log( "InstTerm %s of %s is on %s\n", $iStr, $tStr, $nStr);
	}
    }
}

sub dumpNet ($) {
    my $block = shift;

    my $nIter = new oa::oaIter::oaNet($block->getNets());
    while (my $net = $nIter->getNext()) {
	my $stype = new oa::oaSigType($net->getSigType());
	my $nStr;
	$net->getName($ns, $nStr);
	&dumper::log( "Net %s %s \n", $nStr, $stype->getName());

	my $tIter = new oa::oaIter::oaTerm($net->getTerms());
	while (my $term = $tIter->getNext()) {
	    my $ttype = new oa::oaTermType($term->getTermType());
	    my $tStr;
	    $term->getName($ns, $tStr);
	    &dumper::log( "Term %s on %s type %s", $tStr, $nStr, $ttype->getName());
	    my $iterPins = new oa::oaIter::oaPin($term->getPins());
	    while (my $pin = $iterPins->getNext()) {
		$pin->getName($tStr);
		&dumper::log( " Pin=%s", $tStr);
            }
	    &dumper::log( "\n" );
	    
	}
    }
}

sub dumpShape ($$) {
    my $block = shift;
    my $design = shift;
    my $tech = $design->getTech();

    my $sIter = new oa::oaIter::oaShape($block->getShapes());
    while (my $shape = $sIter->getNext()) {
	my $bbox;
	$shape->getBBox($bbox);
	my $type = $shape->getType();
	&dumper::log( "Shape %s", (new oa::oaType($type))->getName() );
	if ($type == $oa::oacTextType) {
	    my $str;
	    $shape->getText($str);
	    &dumper::log( "\"%s\"", $str);
	}
	&dumper::log( " at ((%d, %d) (%d,%d))", @$bbox);

	my $lnum = $shape->getLayerNum();
	my $pnum = $shape->getPurposeNum();

	if ($tech) {
	    my $layer = oa::oaLayer::find($tech, $lnum);
	    my $purp = oa::oaPurpose::find($tech, $pnum);
	    my ($lStr, $pStr) = ($lnum, $pnum);
	    $layer->getName($lStr) if $layer;
	    $purp->getName($pStr) if $purp;
	    &dumper::log( " LPP %s/%s", $lStr, $pStr);
	} else {
	    &dumper::log( " LPP %d/%d", $lnum, $pnum );
	}

	if ($shape->hasPin()) {
	    my $pin = $shape->getPin();
	    my $pStr;
	    $pin->getName($pStr);
	    &dumper::log( " on pin %s", $pStr);
	}

	if ($shape->hasNet()) {
	    my $net = $shape->getNet();
	    my $nStr;
	    $net->getName($ns, $nStr);
	    &dumper::log( " on net %s", $nStr);
	}

	if ( $type == $oa::oacPathSegType && $shape->hasRoute()) {
	    &dumper::log( " ROUTE" );
	}
  
	&dumper::log( "\n" );
    }
}

sub dumpRtPoint ($$) {
    my $pt = shift;
    my $key = shift;

    if (!$pt) {
	dumper::log( "%s NULL ", $key );
	return;
    }

    my $type = $pt->getType();
    &dumper::log( "%s %s", $key, (new oa::oaType($type))->getName());

# This is from the C++ code, but it makes no sense:
#
#    if ($type == $oa::oacStdViaType ||
#	$type == $oa::oacCustomViaType ||
#	$type == $oa::oacInstTermType) {
	
    my $str; 
    if ($type == $oa::oacInstTermType) {
	$pt->getInst()->getName($ns, $str);
	&dumper::log( "[%s",  $str);
	$pt->getTerm()->getName($ns, $str);
	&dumper::log( "/%s] ", $str);
    } elsif ($type == $oa::oacSteinerType) {	
	# no-op
    } elsif ($type == $oa::oacPinType) {
	$pt->getName($str);
	dumper::log( " %s",  $str);
	$pt->getTerm()->getName($ns, $str);
	dumper::log( "[%s] ", $str);
    } else {
	my $bbox;
	$pt->getBBox($bbox);
	my $lnum = $pt->getLayerNum();
	my $pnum = $pt->getPurposeNum();
	dumper::log( " at ((%d, %d) (%d,%d)) on %d/%d ", @$bbox, $lnum, $pnum);
    }
}

sub dumpRtElement ($) {
    my $relem = shift;
    my $type = $relem->getType();

    if ($type == $oa::oacPathSegType) {
	my ($ptSt, $ptEn);
	$relem->getPoints($ptSt, $ptEn);
	dumper::log( "SEG at ((%d,%d)(%d,%d)) ", @$ptSt, @$ptEn);
	dumper::log( "on %d ", $relem->getLayerNum());
    } elsif ($type == $oa::oacStdViaType || $type == $oa::oacCustomViaType) {
	my $ptOrigin = $relem->getOrigin();
	my $orient = new oa::oaOrient($relem->getOrient());
	dumper::log( "VIA %s at (%d, %d) ", $orient->getName(), @$ptOrigin);
    }
}

sub dumpRoute ($) {
    my $block = shift;

    my $riter = new oa::oaIter::oaRoute($block->getRoutes());
    while (my $rt = $riter->getNext()) {
	my $rstat = $rt->getRouteStatus();
	my $net = $rt->getNet();
	&dumper::log( "Route ST %s ", (new oa::oaRouteStatus($rstat))->getName());
	if ($net) {
	    my $nStr;
	    $net->getName($ns, $nStr);
	    &dumper::log( "NET %s ", $nStr);
	}
	my $from = $rt->getBeginConn();
	my $to = $rt->getEndConn();
	dumpRtPoint($rt->getBeginConn(), "START" );
	dumpRtPoint($rt->getEndConn(),   "END"   );
	my $relems = [];
	$rt->getObjects($relems);
	map { &dumpRtElement($_) } @$relems;
	&dumper::log( "\n" );
    }
}

sub sortDumpFile() {
    system("sort --ignore-case -o $dumper::dumpName $dumper::dumpName");
}

package dumper;

use Carp qw(cluck);

our $dumpName;
our $df;

sub new {
    my $pkg = shift;
    my $cellstr = shift;
    my $viewstr = shift;
    $dumpName = "${cellstr}${viewstr}.dump";
    open($df, "> $dumpName") || die "Can't write file $dumpName";
    printf("Writing dump to %s\n", $dumpName);
    return bless {}, $pkg;
}
    
sub closeLog ($) {
    close($df);
}

sub log {
    my $format = shift;
    my @args = @_;
    my $num = ($format =~ s/%/%/g);
    if (scalar(@args) != $num) {
	cluck("Bad format");
    }
    printf { $df } ($format, @args);
}

1;
