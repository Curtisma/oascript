/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%ignore OpenAccess_4::oaTimestamp;

%typemap(in) OpenAccess_4::oaTimestamp
{
    if (SvIOK($input))
	$1.set((oaUInt4) SvIV($input));
}

%typemap(in) OpenAccess_4::oaTimestamp& (oaTimestamp tmpTS)
{
    $1 = tmpTS;
}

%typemap(in) const OpenAccess_4::oaTimestamp& (oaTimestamp tmpTS)
{
    if (SvIOK($input))
	tmpTS.set((oaUInt4) SvIV($input));
    $1 = tmpTS;
}

%typecheck(0) OpenAccess_4::oaTimestamp&
{
    $1 = (input == &PL_sv_undef) ? 0 : 1;
}

%typecheck(0) const OpenAccess_4::oaTimestamp&
{
    $1 = SvIOK($input) ? 1 : 0;
}

%typemap(argout) OpenAccess_4::oaTimestamp&
{
    sv_setiv($input, (IV) (oaUInt4) $1);
}

%typemap(argout) const OpenAccess_4::oaTimestamp&
{
}

%typemap(out) OpenAccess_4::oaTimestamp
{
    $result = sv_2mortal(newSViv((IV) (oaUInt4) $1));
    argvi++;
}

%typemap(out) OpenAccess_4::oaTimestamp&
{
    $result = sv_2mortal(newSViv((IV) (oaUInt4) *$1));
    argvi++;
}
