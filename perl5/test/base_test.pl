#!/usr/bin/perl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

use strict;
use warnings;

use File::Basename;
use FindBin;

use lib dirname($FindBin::RealBin);
use oa::base;

eval {
    oa::oaBaseInit();
    
    my $timer = new oa::oaTimer;

    my $bi_arr;
    oa::oaBuildInfo::getPackages($bi_arr);
    printf("%-15s  %-15s\n", "package", "build name");
    print('-' x 30, "\n");
    for my $bi (@$bi_arr) {
	printf("%-15s  %-15s\n",
	       $bi->getPackageName(),
	       $bi->getBuildName());
    }

    my $dir = new oa::oaDir(".");
    my $path = $dir->getFullName();
    print("\nOA thinks the cwd is $path\n\n");

    sleep(1);

    printf("That took %f seconds.\n\n", $timer->getElapsed());
};

if ($@) {
    print STDERR "Caught exception: $@\n";
}

exit 0;
