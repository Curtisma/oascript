#! /usr/bin/env python2.5
###############################################################################
# Copyright (c) 2002-2011  Si2, Inc.  DUNS 62-191-1718
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   04/01/11  1.0      bpfeil, Si2      OA Scripting Language Labs
#
###############################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
##############################################################################
import sys
import os
import inspect


here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.append(os.path.join(os.path.dirname(here), '../..'))

import oa.design


oa.oaDesignInit()

# ****************************** Globals ******************************

ns = oa.oaNativeNS()

chp_name_lib_netlist  = "Lib"
chp_name_lib_symbol   = "LibSymbol"
chp_name_view_netlist = "netlist"
chp_name_view_symbol  = "symbol"
chp_name_libdefsfile  = oa.oaString("lib.defs")
chp_pathlib_symbol    = oa.oaString("./LibSymbol")
scname_lib_netlist   = oa.oaScalarName(ns, chp_name_lib_netlist)
scname_lib_symbol    = oa.oaScalarName(ns, chp_name_lib_symbol)
scname_view_netlist  = oa.oaScalarName(ns, chp_name_view_netlist)
scname_view_symbol   = oa.oaScalarName(ns, chp_name_view_symbol)

mode_write  = 'w'
mode_read   = 'r'
mode_append = 'a'

  # Layer, purpose settings are tool/methodology-specific.
  # These are rather arbitrary

str_layer_dev    = "device"
str_layer_text   = "text"
str_purpose_dev  = "drawing"
str_purpose_text = "labels"

num_layer_dev  = 230
num_layer_text = 229

num_purpose_dev  = oa.oavPurposeNumberDrawing
num_purpose_text = 1013

lib_netlist = ''
lib_symbol  = ''



# ****************************** Utility functions  ******************************
def lineno():
   # """Returns the current line number in our program."""
    return inspect.currentframe().f_back.f_lineno

def assert_def(condition, msg):
    if not condition:
        print "***ASSERT error %s" % (msg)

def unexpected_exception (excp, lineno):
    print "Unexpected Exception:  %s at line=%d" % (excp, lineno)
    sys.exit(2)


def set_layer_purpose (scname_lib, layer_name, layer_num_in, purp_name_in, purp_num_in, close_tech):
    # First see if a Tech database can be found already open for the lib,
    # i.e., from a prior call to set_layer_purpose (or any other module
    # if this were part of a large application).
    #
    tech = oa.oaTech.find( scname_lib )
    
    strname_lib = oa.oaString() 
    scname_lib.get(ns, strname_lib)
    if not tech:
      #
      # If one's not already open in memory, try opening it from disk.
      #
        try:
            tech = oa.oaTech.open( scname_lib, 'a' )
        #
        # No Tech open in memory, none on disk by that name. So create one.
        #
        except:
#>>>>            print "excp.getMsgId() = %d " % (excp.getMsgId());
            print "Open of tech for lib=%s failed. Will create." % (strname_lib)
#>>>>            if (excp.getMsgId() == oa.oacCannotFindLibraryTech ):
            tech = oa.oaTech.create( scname_lib )
                   #
                   # Set the DBU to 1/3 nanometer for all Netlist type CellViews.
                   #
            view_type = oa.oaViewType.get( oa.oaReservedViewType(oa.oacNetlist) )
            tech.setUserUnits(view_type, oa.oaUserUnitsType(oa.oacMicron) )
            tech.setDBUPerUU( view_type, 3000)
#>>>>                   else:
#>>>>                   print "HERE2"
#>>>>                   unexpected_exception( excp.getMsgId(), lineno )
#>>>>                   unexpected_exception( "error creating Tech", lineno )
#<<<<        else:
#<<<<            msg="Error on oa.oaTech.open for lib=%s" % (strname_lib)
#<<<<            unexpected_exception(msg, lineno)


     # Try locating a Purpose of the given name. If so, overwrite
     # the purp_num defaulted above with the value found in the Tech.
     # Else, create a new Purpose with this name and num in the Tech.
     #
    purpose = oa.oaPurpose.find(tech, purp_name_in)
    if purpose:
      purp_num = purpose.getNumber()
      print "Found Purpose name=%s  num=%x" % (purp_name_in,purp_num)
    else: 
      purpose = oa.oaPurpose.create(tech, purp_name_in, purp_num_in)
      purp_num = purpose.getNumber()
      print "Created Purpose name=%s num=%x" % (purp_name_in,purp_num)
    
    #
    # If there is a Layer by the given name in the Tech already, get its num.
    # Else create one.
    #
    layer = oa.oaLayer.find(tech, layer_name)
    if layer:
      layer_num = layer.getNumber()
      print "Found Layer name=%s  num=%d" % (layer_name,layer_num)
    else:
      layer = oa.oaPhysicalLayer.create(tech, layer_name, layer_num_in)
      layer_num = layer.getNumber()
      print "Created Layer name=%s num=%d" % (layer_name,layer_num)
    
    if close_tech:
      tech.save()
      tech.close()
    
    return


def map_libs (chp_name_libdefsfile, scname_lib_prev, scname_lib_curr, chp_pathlib_curr):
    global mode_append

    ns = oa.oaUnixNS()

  # Perform logical lib name to physical path name mappings for the source library
  # created in the netlist/ lab, then the symbol library to be created in this lab.
  #
  # Make sure the lib.defs is LOCAL to avoid clobbering a real one.

    strLibDefListDef = oa.oaString()    
    oa.oaLibDefList.getDefaultPath( strLibDefListDef )

    if ( strLibDefListDef != chp_name_libdefsfile ):
        print "***Missing local %s file.\n" % (chp_name_libdefsfile)
        print " Please create one with the one line shown below: \n"
        print "       INCLUDE ../netlist/lib.defs\n"
        sys.exit(4)

    oa.oaLibDefList.openLibs()

    lib_prev = oa.oaLib.find(scname_lib_prev)
    if not lib_prev:
        print  "***Either lib.defs file missing or netlist lab was not completed."
        sys_exit(1)
  
    strNameLibCurr=oa.oaString()
    scname_lib_curr.get( ns, strNameLibCurr )

    lib_curr = oa.oaLib.find( scname_lib_curr )

    if lib_curr:
      print "%s Lib definition found in %s and opened." % (strNameLibCurr,chp_name_libdefsfile)
    else:
      # If this program was run once already, a Lib directory will exist.
      # If the lib.defs file was deleted, the Lib for this current lab won't
      # have been opened by oaLibDefFile::load(), so it must be opened now.
      # If the updated lib.defs (with the def for this lab's Lib) remains but
      # this lab's Lib directory was deleted, then the oaLib::find() above would
      # have failed and the exists() test below will also fail;
      # hence, the subsequent create() will be invoked.
      #
      if oa.oaLib.exists( strNameLibCurr ):
         print "Opening from disk existing Lib=%s" % (strNameLibCurr)
         lib_curr = oa.oaLib.open( scname_lib_curr, chp_pathlib_curr)
      else: 
        # If there is no directory for the Lib, then it must be created now.
        #
         file_sys = oa.oaString("oaDMFileSys")
         lib_curr = oa.oaLib.create( scname_lib_curr, chp_pathlib_curr, oa.oaLibMode(oa.oacSharedLibMode), file_sys )

         print "Created new Lib=%s" % (strNameLibCurr)
        #
        # Update the lib.defs file for future labs that reference this Design.
        #
#>>>         ldl = oa.oaLibDefList.get( strLibDefListDef, 'a')
#>>>         oa.oaLibDef.create( ldl, scname_lib_curr, chp_pathlib_curr )
#>>> Problems with oaPerl and oaPython getting an append of the  libMod info to the ./lib.defs file.
#>>> Experiments show that the save() truncated any blank lines after the INCLUDE ../netlist/lib.defs,
         libdefsfile = open ('./lib.defs', 'a')
         str = "\nDEFINE LibSymbol ./LibSymbol\nASSIGN LibSymbol writePath ./LibSymbol\nASSIGN LibSymbol libMode shared\n";
         libdefsfile.write(str)
         libdefsfile.close()


                  
    return lib_prev, lib_curr



def get_block( design ):
    # Locate the top Block for the Design.
    return design.getTopBlock()


def get_cell_name( design ):
    global ns
    cell_name = oa.oaScalarName(ns, " ")
    design.getCellName( cell_name )
    return cell_name


def get_lib_name( design ):
    global ns
    lib_name= oa.oaScalarName(ns, " ")
    design.getLibName( lib_name )
    return lib_name


def get_view_name( design ):
    global ns
    view_name = oa.oaScalarName(ns, " ")
    design.getViewName(  view_name    )
    return view_name


def print_views( lib ):
    ns = oa.oaUnixNS()
    lib_name= oa.oaScalarName(ns, "")
    lib.getName( lib_name )
    print "---Views in lib named $s" % (lib_name)
    
    view_name = oa.oaScalarName(ns, "")
    view_type_name = oa.oaString()
    tempstr = oa.oaString()

    for view in lib.getViews.each():
        view.getName( view_name )
        view_type_name = view.getViewType.getName()

        tempstr=view_name.get(ns,tempstr)
        print "  %s VT=%s" % (tempstr, view_type_name)


def open_design( scname_lib, scname_cell, scname_view, mode ):
    # In Ruby, you don't have to do "scalarName.get" to obtain the string
    # when printing.  The to_s method does it for you.
  
    strname_lib = oa.oaString()
    strname_cell = oa.oaString()
    strname_view = oa.oaString()
    scname_lib.get(ns,strname_lib)
    scname_cell.get(ns,strname_cell)
    scname_view.get(ns,strname_view)    
    print "Opening Design %s   %s   %s" % (strname_lib, strname_cell, strname_view)
  
    new_view_type= oa.oaString( "netlist") 
    viewType = oa.oaViewType.get( oa.oaReservedViewType(new_view_type) )
    try:
        design = oa.oaDesign.open( scname_lib, scname_cell, scname_view, viewType, mode)

    except:
        msg= "Can't find Design. Check lib.defs file." 
        unexpected_exception( msg, lineno )
    
    return design



def make_sch_sym_view( chp_name_cell ):
    global ns
    global scname_lib_netlist, scname_view_netlist, mode_read, mode_append
    global scname_lib_symbol, scname_view_symbol, chp_name_lib_netlist, chp_name_lib_symbol

    scname_cell = oa.oaScalarName(ns, chp_name_cell)
    design = open_design(scname_lib_netlist, scname_cell,
                         scname_view_netlist, mode_read )
  
    block = get_block( design )
    assert_def( block.isValid(), lineno )
  
    # Save the new type of Design under the same cell/view names
    # but with in a new library local to this lab ($GlobalData.scname_lib_symbol).
    # NOTE: there is no way to change the ViewType with which a Design was created.
    # Then close that source Design.
    design.saveAs( scname_lib_symbol, scname_cell, scname_view_symbol )
    block = get_block( design )
    assert_def( block.isValid(), lineno )
  
    design.close()
  
    print "  Saved design from Lib=%s into Lib=%s" % (chp_name_lib_netlist, chp_name_lib_symbol)
  
    # Use open_design() to open the newly created Design (in LibSymbol) in APPEND mode.
    # Return the Design handle.
    design = open_design( scname_lib_symbol, scname_cell,
                          scname_view_symbol, mode_append )
  
    block = get_block( design )
    assert_def( block.isValid(), lineno )
    return design



def save_close_design( design ):
    cell_name=get_cell_name( design )
    lib_name=get_lib_name( design )

    print "  Saving \"%s\" in symbol library named \"%s\"" % (cell_name.get(),lib_name.get() )
    design.save()
    design.close()



def make_label( block, label_position ):
    global ns, num_layer_text, num_purpose_text
    # Using data from the globals singleton, create a Text Object that consists
    # of the Design's cell name. Place it at the label_position, aligned lower left,
    # no rotation, a fixed font, height=4, no overbar, with the flags set to
    # indicate that it is to be visible and displayed in drafting mode.
    design = block.getDesign()

    cell_name=oa.oaString(" ")
    sccell_name=get_cell_name( design )
    sccell_name.get(ns, cell_name)

    text = oa.oaText.create( block,num_layer_text,num_purpose_text,cell_name, label_position, oa.oaTextAlign(oa.oacLowerLeftTextAlign), oa.oaOrient(oa.oacR0), oa.oaFont(oa.oacFixedFont), 4, False, True, True )
  
    # Transfer the text value of the oaText managed object into the str utility variable.
    str = oa.oaString("")
    text.getText( str )
  
    print "  Created label = %s" % (str)
  
    return str
  


# ******************************   Functions        ******************************

# Create Shapes that represent a simple schematic symbol for the leaf cell.
#   (These are not electrical or manufacturing geometries, merely Shapes
#   that comprise the symbol on the schematic.)

def make_fa_symbol():
    global num_layer_dev, num_purpose_dev
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    label = "FullAdder"
    design = make_sch_sym_view( label )
    block = design.getTopBlock()

    # Create a rectangle shape to be the full-adder symbol with the lower left at
    # the origin and the upper right at {60,52}. Use the layer and purpose from
    # the globals singleton object.
    #
    #         3,95_________________159,95
    #            |                 |
    #            |                 |
    #            |                 |
    #            |                 |
    #            |_________________|
    #         3, 3                 159, 3
    #
    box = oa.oaBox( 3, 3, 159, 95 )
    oa.oaRect.create( block, num_layer_dev, num_purpose_dev, box )

    # Use the make_label function to put the cell's name at the location {2,2}
    origin = oa.oaPoint( 2, 2 )
    make_label( block, origin )

    save_close_design( design )


# Create Shapes that represent a simple schematic symbol for the leaf cell.
#   (These are not electrical or manufacturing geometries, merely Shapes
#   that comprise the symbol on the schematic.)

def make_ha_symbol():
    global ns, num_layer_dev, num_purpose_dev
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    label  = "HalfAdder"
    design = make_sch_sym_view( label )
    block  = design.getTopBlock()

    # Create a rectangle shape to be the half-adder symbol with the lower left at
    # the origin and the upper right at {64,50}. Use the layer and purpose from
    # the globals singleton object.
    #
    #      0,50__________64,50
    #         |          |
    #         |          |
    #         |          |
    #         |          |
    #         |__________|
    #       0,0          64,0
    #
    box = oa.oaBox( 0, 0, 64, 50 )
    oa.oaRect.create( block, num_layer_dev, num_purpose_dev, box )

    # Use the make_label function to put the cell's name at the location {2,25}
    pt = oa.oaPoint( 2, 25 )
    make_label( block, pt )

    save_close_design( design )


def make_and_symbol():
    global ns, num_layer_dev,num_purpose_dev
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    label = "And"
#>>>>    scname_label = oa.oaScalarName(ns, label)
    design = make_sch_sym_view( label )
    block = design.getTopBlock()

    # make a crude AND symbol out of two open Shapes
    #
    #    0,24              16,24
    #       .---------------+
    #       |                    +
    #       |                       +
    #       |                         +
    #       |                          +
    #       |                           +
    #       |                           o <--- midpoint is 28,12
    #       |                           +
    #       |                          +
    #       |                         +
    #       |                       +
    #       |                    +
    #       .---------------+
    #     0,0              16,0

    # Create a PointArray representing the 3-sided, open box part on the left.
    #
    pt0 = oa.oaPoint( 16,  0)
    pt1 = oa.oaPoint(  0,  0)
    pt2 = oa.oaPoint(  0, 24)
    pt3 = oa.oaPoint( 16, 24)
    pts = [ pt0, pt1, pt2, pt3 ]

    # Create in the owner Block the appropriate, non-closed oaShape using the array.
    oa.oaLine.create( block, num_layer_dev, num_purpose_dev, pts )

    # Create an arc representing the part on the right.
    # To make it easy, assume the curve is simply the right half of
    # an ellipse centered in a bounding Box 12 to the right and 12 left
    #
    #    0,24   4,24       16,24
    #       |   |           |
    #            ---------- + ----------
    #           |                +      |
    #           |                   +   |
    #           |                     + |
    #           |                      +|
    #           |                       +
    #           |                       o <--- midpoint is 28,12
    #           |                       +
    #           |                      +|
    #           |                     + |
    #           |                   +   |
    #           |                +      |
    #            ---------- + ----------
    #       |   |           |
    #     0,0   4,0         16,0

    # Define the BBox for the right arc of the "And" symbol.
    bbox = oa.oaBox( 4, 0, 28, 24 )

    # Create an appropriate open Shape for this right-hand part of the "And" symbol.
    # Assume the angle sweeps from -90 to 90 degrees to make it easy)
    radiansStart = -1.571
    radiansEnd =    1.571
    oa.oaArc.create( block, num_layer_dev, 
                     num_purpose_dev,bbox, 
                     radiansStart, radiansEnd )

    # Use the make_label function to put the cell's name at the location {2,2}
    pt = oa.oaPoint( 2, 2 )
    make_label( block, pt )

    save_close_design( design )



def make_or_symbol():
    global ns, num_layer_dev, num_purpose_dev
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    label = "Or"
    design = make_sch_sym_view( label )
    block = design.getTopBlock()

    # make a crude OR symbol out of two identical arcs, left and right
    # plus two lines joining them top and bottom
    #
    #    0,24               16,24
    #       .---------------+
    #            +               +
    #               +               +
    #                 +               +
    #                  +               +
    #                   +               +
    #         12,12 --> o               o <--- 28,12
    #                   +               +
    #                  +               +
    #                 +               +
    #               +               +
    #            +               +
    #       .---------------+
    #     0,0               16,0

    # Copy the same code that made the oaArc for the AND symbol and assign it to arc_right.
    bbox = oa.oaBox(4, 0, 28, 24 )
    arc_right = oa.oaArc.create( block, 
                                 num_layer_dev,
                                 num_purpose_dev,
                                 bbox,
                                 -1.571,  1.571 )

    # Reuse this arc by making a copy of it in the Block and moving it left by (16,0)
    trans = oa.oaTransform( -16, 0 )
    arc_right.copy( trans )

    # Create the top Line from PointArray pa
    pa = [ oa.oaPoint( 0, 24 ), oa.oaPoint( 16, 24 ) ]
    oa.oaLine.create( block,
                      num_layer_dev,  
                      num_purpose_dev,
                      pa )

    # Rethe = Y values of pa1 to match those of the bottom line of the symbol
    pa = [ oa.oaPoint( 0, 0 ), oa.oaPoint( 16, 0 ) ]

    # Create another Line reusing pa1
    oa.oaLine.create( block, 
                      num_layer_dev, 
                      num_purpose_dev, 
                      pa )

    # Use the make_label function to put the cell's name at the location {2,2}
    pt = oa.oaPoint( 2, 2 )
    make_label( block, pt )

    save_close_design( design )



def make_xor_symbol():
    global ns, scname_lib_symbol,scname_view_symbol, mode_read
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    label = "Xor"
    design = make_sch_sym_view( label )

    # make a crude XOR symbol that looks just like the OR, moved to the right 6
    # with an extra copy of the arc on the left at the Y-axis position.
    #
    #    0,24                     22,24
    #       +  .  +---------------+ .
    #            +     +               +
    #               +     +               +
    #                 +     +               +
    #                  +     +               +
    #                   +     +               +
    #         12,12 --> o     o <--18,12      o <--- 34,12
    #                   +     +               +
    #                  +     +               +
    #                 +     +               +
    #               +     +               +
    #           .+    .+              .+
    #       +     +---------------+
    #     0,0                     22,0


    # Open the symbol Design of the Or just create above (in read mode).
    scname_lib  = scname_lib_symbol
    scname_cell = oa.oaScalarName( ns, "Or" )
    scname_view = scname_view_symbol
    mode =        mode_read
    view_type =   oa.oaViewType.get( oa.oaReservedViewType( "netlist") )
    design_or =   oa.oaDesign.open( scname_lib, scname_cell, scname_view, view_type, mode )

    # Create an iterator for all the Shapes in the Or's Block
    block = design_or.getTopBlock()

    # Create a loop that retrieves the next Shape.
#<<<<    block.getShapes.each do |shape|

    for shape in block.getShapes():
        # Copy each shape to this Xor Design, moving it to the right by 6,
        # saving the handle of the newly copied Shape in copied_shape.
        trans = oa.oaTransform( 6, 0 )
        copied_shape = shape.copy( trans, design )
        
        copied_shape_type=copied_shape.getType()
        copied_shape_type_name=copied_shape_type.getName()

        # If that shape is an oaArc, then save a pointer to it in saved_arc.
        if (copied_shape.getType().getName() == "Arc" ):
            saved_arc = copied_shape
        # If that shape is the oaText (label) save a pointer to it in saved_text
        if (copied_shape.getType().getName() == "Text"):
            saved_text = copied_shape

    # At this point the new Xor Design Block has a copy of the symbol
    # shapes created for the Or, but moved to the right by 6, and
    #    - saved_arc is one of the arcs
    #    - saved_text is the Text label

    new_arc_box = oa.oaBox( -12, 0, 12, 24 )

    # make one more arc in the Xor Design Block by copying that saved_arc, assigning
    # it to the new_arc variable (the position of it doesn't matter, it will be re). =
    trans = oa.oaTransform( 0, 0 )
    new_arc = saved_arc.copy( trans )

    # Rethe = Box of new_arc to new_arc_box and the start/stop angles to the same angles
    # obtained from that original saved_arc.
    saved_arc.set( new_arc_box, saved_arc.getStartAngle(), saved_arc.getStopAngle() )

    # An oaText label was just copied (with the other Shapes) from the Or Cell,
    # so instead of using make_label to add another one, just change the value
    # of that existing oaText (saved from the loop above) from the lib/cell/view
    # name of the Or to that of the Xor.
    cell_name=get_cell_name( design )
    saved_text.setText( cell_name.get() )

    save_close_design( design )


# ******************************       Main         ******************************

# Create schematicsymbol CellViews from the netlist CVs in lab netlist.
#

def main():
    global chp_name_libdefsfile, scname_lib_netlist, scname_lib_symbol, chp_pathlib_symbol 
    global num_purpose_text, num_layer_dev, num_layer_text
    global str_purpose_dev, str_layer_dev, str_layer_text

    (lib_netlist, lib_symbol) = map_libs (chp_name_libdefsfile, scname_lib_netlist, 
                                          scname_lib_symbol, chp_pathlib_symbol)
    # Set up Layer and Purpose nums for the layers in the Tech database.
    # to be used for the device shapes.
    #

    set_layer_purpose(scname_lib_symbol, str_layer_dev, num_layer_dev, 
                       str_purpose_dev, num_purpose_dev,  False)
    set_layer_purpose( 
                scname_lib_symbol, str_layer_text, num_layer_text, 
                str_purpose_text, num_purpose_text, True)
  

    make_and_symbol()
    make_or_symbol()

    make_xor_symbol()
    make_ha_symbol()
    make_fa_symbol()

if (__name__ == "__main__"):
    sys.exit(main())


