/* -*- mode: python -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%extend OpenAccess_4::oaTransform
{
    %feature("python:slot", "mp_subscript", functype="binaryfunc") __getitem__;
    %feature("python:slot", "mp_ass_subscript", functype="objobjargproc") __setitem__;
    %feature("python:slot", "tp_repr", functype="reprfunc") __repr__;
    %feature("python:slot", "tp_str", functype="reprfunc") __str__;
    %feature("python:slot", "tp_iter", functype="getiterfunc") __iter__;

    %typemap(in) PySliceObject* {
        if (!PySlice_Check($input)) {
            %argument_fail(SWIG_TypeError, "$type", $symname, $argnum);
        }
        $1 = (PySliceObject *) $input;
    }

    %typemap(typecheck,precedence=SWIG_TYPECHECK_POINTER) PySliceObject* {
        $1 = PySlice_Check($input);
    }

    PyObject* __getitem__ (int arg) const
    {
        if (arg < 0) arg += 3;
        if (arg < 0 || arg >= 3) {
            PyErr_SetString(PyExc_IndexError, "oaTransform index out of range.");
            return NULL;
        }
        switch (arg) {
            case 0 : return PyInt_FromLong((long) self->xOffset()); break;
            case 1 : return PyInt_FromLong((long) self->yOffset()); break;
            case 2 : return PyInt_FromLong((long) (oaOrientEnum) self->orient()); break;
            default : assert(0);
        }
        return NULL;
    }

    PyObject* __getitem__ (PySliceObject *arg) const
    {
        Py_ssize_t i, j, k, step, count;
        if (PySlice_GetIndicesEx(arg, 3, &i, &j, &step, &count) == -1)
            return NULL;
        if (count == 1) {
            switch (i) {
            case 0 : return PyInt_FromLong((long) self->xOffset()); break;
            case 1 : return PyInt_FromLong((long) self->yOffset()); break;
            case 2 : return PyInt_FromLong((long) (oaOrientEnum) self->orient()); break;
            default : assert(0);
            }
        }
        PyObject *result = PyTuple_New(count);
        for (k = 0; i < j; i += step) {
            switch (i) {
            case 0 : PyTuple_SET_ITEM(result, k++, PyInt_FromLong((long) self->xOffset())); break;
            case 1 : PyTuple_SET_ITEM(result, k++, PyInt_FromLong((long) self->yOffset())); break;
            case 2 : PyTuple_SET_ITEM(result, k++, PyInt_FromLong((long) (oaOrientEnum) self->orient())); break;
            default : assert(0);
            }
        }
        return result;
    }

    PyObject* __setitem__ (int i, PyObject *arg)
    {
        if (i < 0) i += 3;
        if (i < 0 || i >= 3) {
            PyErr_SetString(PyExc_IndexError, "oaTransform index out of range.");
            return NULL;
        }
        if (!arg || !LANG_INTP(arg)) {
            PyErr_SetString(PyExc_RuntimeError, "Bad value in oaTransform assignment.");
            return NULL;
        }
        switch (i) {
            case 0 : self->xOffset() = PyLong_AsLong(arg); break;
            case 1 : self->yOffset() = PyLong_AsLong(arg); break;
            case 2 : self->orient() = oaOrient((oaOrientEnum) PyLong_AsLong(arg)); break;
            default : assert(0);
        }
        return SWIG_Py_Void();
    }

    PyObject* __setitem__ (PySliceObject *slice, PyObject *arg)
    {
        Py_ssize_t i, j, k, step, count;
        if (PySlice_GetIndicesEx(slice, 3, &i, &j, &step, &count) == -1) {
            PyErr_SetString(PyExc_RuntimeError, "Bad slice indices when assigned to oaTransform");
            return NULL;
        }

        if (count == 1 && arg && LANG_INTP(arg)) {
            switch (i) {
                case 0 : self->xOffset() = PyLong_AsLong(arg); break;
                case 1 : self->yOffset() = PyLong_AsLong(arg); break;
                case 2 : self->orient() = oaOrient((oaOrientEnum) PyLong_AsLong(arg)); break;
                default : assert(0);
            }
            return SWIG_Py_Void();
        }

        if (!arg || !PySequence_Check(arg) || PySequence_Length(arg) != count) {
            PyErr_SetString(PyExc_RuntimeError, "Bad value in oaTransform assignment: wrong number of arguments.");
            return NULL;
        }

        for (k = 0; k < count; i += step) {
            PyObject *val = PySequence_GetItem(arg, k++);
            if (!val || !LANG_INTP(val)) {
                PyErr_SetString(PyExc_RuntimeError, "Bad value in oaTransform assignment.");
                Py_XDECREF(val);
                return NULL;
            }
            switch (i) {
            case 0 : self->xOffset() = PyLong_AsLong(val); break;
            case 1 : self->yOffset() = PyLong_AsLong(val); break;
            case 2 : self->orient() = oaOrient((oaOrientEnum) PyLong_AsLong(val)); break;
            default : assert(0);
            }
            Py_DECREF(val);
        }
        return SWIG_Py_Void();
    }

    PyObject* __repr__ () const
    {
        PyObject *tuple = oaTransform_to_tuple(*self);
        PyObject *repr = PyObject_Repr(tuple);
        Py_DECREF(tuple);
        return repr;
    }

    PyObject* __str__ () const
    {
        PyObject *tuple = oaTransform_to_tuple(*self);
        PyObject *repr = PyObject_Repr(tuple);
        Py_DECREF(tuple);
        return repr;
    }

    PyObject* __iter__ () const
    {
        PyObject *tuple = oaTransform_to_tuple(*self);
        PyObject *iter = PyObject_GetIter(tuple);
        Py_DECREF(tuple);
        return iter;
    }
}

