/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%header {
PyObject *default_namespace = NULL;
}

%include <oa_dcast.i>
%include "python_oa.i"

%include <generic/typemaps/oaTimeStamp.i>
%include <generic/typemaps/oaPoint.i>
%include <generic/typemaps/oaBox.i>
%include <generic/typemaps/oaTransform.i>
%include "python_oaString_typemaps.i"
%include "python_oaTimestamp_typemaps.i"
%include "python_oaPoint_typemaps.i"
%include "python_oaBox_typemaps.i"
%include "python_oaTransform_typemaps.i"
%include "python_oaComplex_typemaps.i"

%ignore OpenAccess_4::oaFeatureArray;
%ignore OpenAccess_4::oaManagedTypeArray;
%ignore oaInitAppBuild;

%include "python_appdefs.i"
