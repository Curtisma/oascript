#!/usr/bin/env python
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

import sys
import os
here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.append(os.path.join(os.path.dirname(here), '..'))

from optparse import OptionParser

import oa.tech

def main():
    parser = OptionParser()
    parser.add_option("--libdefs", action="store", type="string", dest="libdefs", metavar="FILE",
                      help="Location of OA library definition file.")
    parser.add_option("--lib", action="store", type="string", dest="techlib_name", metavar="<libname>", help="OA tech lib to use.")
    (options, args) = parser.parse_args()

    if (not options.techlib_name) :
        print "Missing -lib argument."
        return 1

    oa.oaDMInit()
    oa.oaTechInit()
    if (options.libdefs) :
        oa.oaLibDefList.openLibs(options.libdefs)
    else :
        oa.oaLibDefList.openLibs()

    ns = oa.oaNativeNS()

    techlib_scname = oa.oaScalarName(ns, options.techlib_name)
    techlib = oa.oaLib.find(techlib_scname)
    if not techlib:
        print "Couldn't find %s." % options.techlib_name
        return 1

    if not techlib.getAccess(oa.oaLibAccess(oa.oacReadLibAccess)):
        print "lib %s: No access!" % options.techlib_name
        return 1

    tech = oa.oaTech.open(techlib)
    if not tech:
        print "No tech info!"
        return 1

    for layer in tech.getLayers():
        layer_name = oa.oaString()
        layer.getName(layer_name)
        print "layer %d = %s" % (layer.getNumber(), layer_name)

    return 0


if (__name__ == "__main__"):
    sys.exit(main())
