################################################################################
#  Copyright {c} 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
#  Licensed under the Apache License, Version 2.0 {the "License" you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   04/15/11  10.0     scarver, Si2     Tutorial 10th Edition - Ruby version
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction {CAI} projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library {UNIX man pages}
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation {www.swig.org}
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################


class EmhGlobals
  private_class_method :new
  @@globs              = nil

  # Indentation increment. This does not change.
  @@indent_inc = 2

  # Global indent value for prettier log printing
  @@indent = 0

  # Global flags for clients to set based on how much info they want to see.
  #
  # Default to NOT expanding the Occ hierarchy into other Designs.
  @@expand_occDesignInst_masters = false

  @@dump_block_domain = true
  @@dump_mod_domain   = true
  @@dump_occ_domain   = true

  def skip_if_zero
    return true
  end

  def dont_skip_if_zero
    return false
  end

  def expand_occDesignInst_masters(value={})
    return @@expand_occDesignInst_masters if value == {} 
    @@expand_occDesignInst_masters =  value
  end

  def dump_block_domain(value={})
    return @@dump_block_domain if value == {} 
    @@dump_block_domain = value
  end

  def dump_mod_domain(value={})
    return @@dump_mod_domain if value == {} 
    @@dump_mod_domain = value
  end

  def dump_occ_domain(value={})
    return @@dump_occ_domain if value == {} 
    @@dump_occ_domain = value
  end

  def EmhGlobals.create
    unless @@globs
      @@globs = new
    end
    @@globs
  end

  def add_indent 
    @@indent += @@indent_inc
  end

  def sub_indent 
    @@indent -= @@indent_inc
  end

  def do_indent 
    @@indent.times {print " "}
  end
  
end

$EmhGlobals = EmhGlobals.create

$ns = OaNativeNS.new


def log (mesg) 
  $EmhGlobals.do_indent
  print mesg
end

def assertq (condition, statement_text)
  puts "\n***ASSERT[FAIL] #{statement_text}" unless condition
end

def print_obj_type (obj) 
  print " #{obj.getType.getName}"
end


def new_obj_base_id (obj) 
  puts ""
  $EmhGlobals.do_indent
  print_obj_type(obj)
  print " "
end


def bound_status (ref) 
  if ref.isBound
    print " BOUND "
  else
    print " UNBOUND "
  end
end

def print_obj_name_scalar (obj, prepend={})
  ns = OaNativeNS.new
  scalar_name = OaScalarName.new
  obj.getName(scalar_name)
  scalar_name.get(ns,simple_string='')
  print "#{prepend}\"#{simple_string}\""
end

def print_obj_name (obj, prepend={})
  if obj.isOccObject
    if obj.isOccNet || obj.isOccInst
      print "#{prepend}#{obj.getPathName}"
    else
      print "#{prepend}\"#{obj.getName}\""
    end
  else
    print "#{prepend}\"#{obj.getName}\""
  end
end

def print_transform  (inst) 
  tranny = inst.getTransform
  print " #{tranny.orient.getName}"
  print " (#{tranny.xOffset},#{tranny.yOffset})"
end


def print_only_lcv (container) 
  container.getLibName($ns,lib='')
  container.getCellName($ns,cell='')
  container.getViewName($ns,view='')
  print "\"#{lib}|#{cell}|#{view}\""
end

def get_obj_name (obj) 
  return obj.getName
end


def print_orig_lcv (mod) 
  mod.getOrig(OaScalarName.new($ns,scNameOrig=''), 
              OaScalarName.new($ns,scNameLib=''),
              OaScalarName.new($ns,scNameCell=''), 
              OaScalarName.new($ns,scNameView=''))

  $EmhGlobals.do_indent
  puts "ORIG=#{scNameOrig}[#{scNameLib}|#{scNameCell}|#{scNameView}]"
end

def print_header_line (count, category, skip_if_zero=:true)
  return false if count == 0 && skip_if_zero

  # force a linefeed
  puts ""
  $EmhGlobals.do_indent
  print "[#{count}]#{category}"
  print "s" if count != 1
  return true
end

def dump_busTermDefs (container, descrip) 
  return if print_header_line(container.getBusTermDefs.getCount, descrip)
  container.getBusTermDefs.each do |objdef|
    new_obj_base_id(objdef)
    print ": BITS #{objdef.getNumBits}"
    print " MINIX=#{objdef.getMinIndex}"
    print " MAXIX=#{objdef.getMaxIndex}"
    print "  BASENAME=#{objdef.getName}"
    print "  BITS:"
    objdef.getBusterm_bits.each {|bit| print " #{bit.getBitIndex}"}
  end
end


def print_sss (bus) 
  print "[#{bus.getStart}"
  print  ":#{bus.getStop}"
  print  ":#{bus.getStep}]"
end


def dump_bits (obj) 
  new_obj_base_id (obj)
  print_obj_name (obj)
  if obj.isImplicit
    print " IM"
  else
    print " EX"
  end
  print "BITS=#{obj.getNumBits}"

end


def print_net_summary (net) 
  dump_bits net
  print_sss(net) if net.getType.getName == "BusNet"

  if net.isBitNet
    # The following is programmed to more closely follow the C++ lab,
    # in which you have to downcast the Net
    bitNet = net
    print " ISPREF" if bitNet.isPreferredEquivalent
    unless bitNet.getEquivalentNets.isEmpty
      print " EQUIVALENTS:"
      bitNet.getEquivalentNets.each {|eqNet| print " #{eqNet.getName}"}
    end
  end
  unless net.getTerms.isEmpty
    puts ""
    $EmhGlobals.add_indent
    log "CONNECTS Terms:"
    net.getTerms.each { |term| print_obj_name(term, " ")}
    $EmhGlobals.sub_indent
  end
  unless net.getInstTerms.isEmpty
    puts ""
    $EmhGlobals.add_indent
    log "CONNECTS InstTerms:"
    net.getInstTerms.each do |instTerm|
      instTerm.getTerm
      if instTerm.isBound
        print " "
      else
        print " CANTBIND"
      end
      print "\"#{instTerm.getInst.getName}"
      print "|#{instTerm.getTermName}\""
    end
    $EmhGlobals.sub_indent
  end
end


def print_domain_ref (ref) 
  if ref.isValid
    print "  ->"
    print_obj_type ref
    print " "
    return true
  end
  return false
end


def print_occNet_summary (net)
  dump_bits(net)
  print_sss(net) if net.getType.getName == "OccBusNet"
  if net.getNumBits == 1
    obNet = net
    unless obNet.getEquivalentNets.isEmpty
      print " EQUIVALENTS:"
      obNet.getEquivalentNets.each do |eqNet|
        print " "
        print_obj_name(eqNet, " ")
      end
    end
  end
  unless net.getTerms.isEmpty
    puts ""
    $EmhGlobals.add_indent
    log "CONNECTS OccTerms:"
    net.getTerms.each {|term| print_obj_name(term, " ")}
    $EmhGlobals.sub_indent
  end
  unless net.getInstTerms.isEmpty
    puts ""
    $EmhGlobals.add_indent
    log "CONNECTS OccInstTerms:"
    net.getInstTerms.each do |instTerm| 
      print "\"#{instTerm.getInst.getName}"
      print "|#{instTerm.getTermName}\""
    end
    $EmhGlobals.sub_indent
  end
  # Cross-domain references
  #
  puts ""
  $EmhGlobals.do_indent
  print_obj_name(net.getNet)    if print_domain_ref(net.getNet)
  print_obj_name(net.getModNet) if print_domain_ref(net.getModNet)
end

def print_modNet_summary (net)
  dump_bits(net)
  print_sss(net) if net.getType.getName == "ModBusNet"
  if net.getNumBits == 1
    mbNet = net
    unless mbNet.getEquivalentNets.isEmpty
      print " EQUIVALENTS:"
      mbNet.getEquivalentNets.each {|eqNet| print " #{eqNet.getName}" }
    end
  end
  unless net.getTerms.isEmpty
    puts ""
    $EmhGlobals.add_indent
    log "CONNECTS ModTerms:"
    net.getTerms.each {|term| print_obj_name(term, " ")}
    $EmhGlobals.sub_indent
  end
  if  ! net.getInstTerms.isEmpty
    puts ""
    $EmhGlobals.add_indent
    log  "CONNECTS ModInstTerms:"
    net.getInstTerms.each do |instTerm|
      print "\"#{instTerm.getInst.getName}"
      print "|#{instTerm.getTermName}\""
    end
    $EmhGlobals.sub_indent
  end
end


def dump_net_members (net_collection)
  net_collection.each do |net|
    print_net_summary(net)
    case net.getType.getName
      when "ScalarNet"
      when "BusNetBit"
      when "BusNet"
        $EmhGlobals.add_indent
        dump_net_members(net.getSingleBitMembers)
        $EmhGlobals.sub_indent
      when "BundleNet"
        $EmhGlobals.add_indent
        dump_net_members(net.getMembers)
        $EmhGlobals.sub_indent
      else 
        log "Unrecognized net type: #{net.getType.getName}\n"
    end
  end
end


def dump_modNet_members (net_collection) 
  net_collection.each do |net|
    print_modNet_summary(net)
    case net.getType.getName
      when "ModScalarNet"
      when "ModBusNetBit"
      when "ModBusNet"
        $EmhGlobals.add_indent
        dump_modNet_members(net.getSingleBitMembers)
        $EmhGlobals.sub_indent
      when "ModBundleNet"
        $EmhGlobals.add_indent
        dumpNetModMembers(net.getMembers)
        $EmhGlobals.sub_indent
      else
          log "Unrecognized net type: #{net.getType.getName}\n"
    end
  end
end

def dump_occNet_members (net_collection)
  net_collection.each do |net|
    print_occNet_summary(net)
    case net.getType.getName
      when "OccScalarNet"
      when "OccBusNetBit"
      when "OccBusNet"
        $EmhGlobals.add_indent
        dump_occNet_members(net.getSingleBitMembers)
        $EmhGlobals.sub_indent
      when "OccBundleNet"
        $EmhGlobals.add_indent
        dump_occNet_members(net.getMembers)
        $EmhGlobals.sub_indent
      else
        log "Unrecognized net type: #{net.getType.getName}\n"
    end
  end
end

def print_master_info (ref, nameType) 
  if ref.isBound
    master = ref.getMaster
    print " BOUND=> "
    print_obj_type(master)
    print_only_lcv(master)
  else
    print " UNBOUND=> #{nameType}"
  end
end

# Specialization: Occ[Mod]Module masters have just a name, no LCV.
# Plus the hier traversal name for oaModModuleInstHeader
# is getMasterModule, not getMaster!
#
def print_occ_master_info (ref) 
  if ref.isBound
    modMaster = ref.getMaster
    print " BOUND=>"
    print_obj_type(modMaster)
    print_obj_name(modMaster)
  else
    print " UNBOUND=>MODULE"
  end
end

def print_mod_master_info (ref) 
  if ref.isBound
    modMaster = ref.getMasterModule
    print " BOUND=>"
    print_obj_type(modMaster)
    print_obj_name(modMaster)
  else
    print " UNBOUND=>MODULE"
  end
end


def dump_master_occ (inst, oi_header_array) 

  wasBound = inst.isBound

  puts ""
  $EmhGlobals.add_indent
  if wasBound
    log "MASTEROCC=BOUND2="
  else
    log "MASTEROCC=UNBOUND"
  end

  case inst.getType.getName
    when "OccVectorInst", "OccVectorInstBit"
      print "NOMASTER"
    when "OccVectorInstBit"
    else
      master_occ = inst.getMasterOccurrence
      if master_occ.isValid
        master_type = master_occ.getType.getName
        assertq(eval('master_type == "Occurrence"'),
          'master_type == "Occurrence"')
        #
        # Always expand OccModuleInsts, since they are part of this Design.
        # But only expand beyond the frontier into other Designs if flag is set.
        #
        if inst.isOccModuleInst
          $EmhGlobals.expand_occDesignInst_masters(true)
          unless wasBound 
            print " BINDING2="
          end
          dump_occ(master_occ, oi_header_array)
        end
      else
        print "CANTBIND"
      end
  end
  $EmhGlobals.sub_indent
end

def maybe_add_new_occInstHeader (inst, oi_header_array)
  ih = inst.getHeader

  # Add the Header to the list if it's not already there.
  not_found = true 
  oi_header_array.each {|oih| not_found = false if oih.getCellName == ih.getCellName}

  oi_header_array.push(ih) if not_found
end


def dump_occDesignInsts (occ, oi_header_array="")
  odi_array = []

  # Organize only the OccDesignInsts into a list
  #
  occ.getInsts.each {|inst| odi_array.push(inst) if inst.isOccDesignInst}

  num_insts = odi_array.length

  return unless print_header_line(num_insts, "OCCDESIGNINST")

  odi_array.each do |inst|
    new_obj_base_id(inst)
    print_obj_name(inst)

    # If we were NOT called [indirectly] from dump_occDesignInsts_from_header
    maybe_add_new_occInstHeader(inst, oi_header_array) if oi_header_array != ""

    # Cross-domain references
    #
    puts ""
    $EmhGlobals.do_indent
    print_obj_name(inst.getInst)    if print_domain_ref(inst.getInst)
    print_obj_name(inst.getModInst) if print_domain_ref(inst.getModInst)

    # No XFORMS in Occ Domain!!
  end
end


def dump_occDesignInsts_from_header (ih) 
  return unless print_header_line(ih.getInsts.getCount, "OCCDESIGNINST")

  ih.getInsts.each do |inst|
    new_obj_base_id(inst)
    print_obj_name(inst)

    # Cross-domain references
    #
    puts ""
    $EmhGlobals.do_indent
    print_obj_name(inst.getInst)    if print_domain_ref(inst.getInst)
    print_obj_name(inst.getModInst) if print_domain_ref(inst.getModInst)

    # No XFORMS in Occ Domain!!

    $EmhGlobals.add_indent
    dump_one_inst(inst)
    $EmhGlobals.sub_indent

    dump_master_occ(inst, "")
  end
end


def dump_occInstHeaders (oi_header_array)
  # Need this cheesy code because there's no getInstHeaders in oaOccurrence
  # and OccInstHeaders are scoped to the Design.

  return unless print_header_line(oi_header_array.length, "OCCINSTHEADER")

  oi_header_array.each do |ih|
    # Pcell processing is taken out of this ruby version.
    #
    next if ih.isSubHeader || ih.isSuperHeader
  
    new_obj_base_id(ih)
    print_only_lcv(ih)
    print_master_info(ih, "DESIGN")
    puts ""
    $EmhGlobals.do_indent
    print_domain_ref(ih.getInstHeader)
    print_domain_ref(ih.getModInstHeader)
    $EmhGlobals.add_indent
    dump_occDesignInsts_from_header(ih)
    $EmhGlobals.sub_indent
  end
end


def dump_occModuleInsts (ih, oi_header_array)
  return unless print_header_line(ih.getInsts.getCount, "OCCMODULEINST")

  ih.getInsts.each do |inst|
    new_obj_base_id(inst)
    print_obj_name(inst)
    $EmhGlobals.add_indent
    dump_one_inst(inst)
    $EmhGlobals.sub_indent
    dump_master_occ(inst, oi_header_array)
  end
end


def dump_occModuleInstHeaders (occ, oi_header_array) 
  ih_coll = occ.getModuleInstHeaders
  return unless print_header_line(ih_coll.getCount, "OCCMODULEINSTHEADER")

  ih_coll.each do |ih|
    new_obj_base_id(ih)
    print_obj_name(ih)
    print_occ_master_info(ih)

    # Cross-domain reference
    #
    mmIH = ih.getModModuleInstHeader
    puts ""
    $EmhGlobals.do_indent
    print_domain_ref(mmIH)
    print_obj_name_scalar(mmIH)

    $EmhGlobals.add_indent
    dump_occModuleInsts(ih, oi_header_array)
    $EmhGlobals.sub_indent
  end
end


def dump_occNets (occ) 
  netColl = occ.getNets(OacNetIterAll)
  return unless print_header_line(netColl.getCount, "OCCNET")
  dump_occNet_members(netColl)
end


def print_occTerm_summary (term)
  dump_bits(term)
  print_sss(term) if term.getType.getName == "OccBusTerm"

  print " %-6s" % term.getTermType.getName

  con_net = term.getNet
  print_obj_name(con_net, " CONN2") if con_net.isValid

  # Cross-domain references
  #
  puts ""
  $EmhGlobals.do_indent
  print_obj_name(term.getTerm)    if print_domain_ref(term.getTerm)
  print_obj_name(term.getModTerm) if print_domain_ref(term.getModTerm)
end


def dump_occTerm_members (term_coll)
  term_coll.each do |term|
    print_occTerm_summary(term)
    case term.getType.getName
      when "OccScalarTerm"
      when "OccBusTermBit"
      when "OccBusTerm"
        occ   = term.getOccurrence
        start = term.getStart
        stop  = term.getStop
        incr  = term.getStep
          
        baseName = term.getDef.getName
        $EmhGlobals.add_indent

        start.step(stop, incr) do |ix|
          term_bit = OaOccBusTermBit.find(occ, baseName, ix)
          print_occTerm_summary(term_bit)
        end
        $EmhGlobals.sub_indent
      when "OccBundleTerm"
        $EmhGlobals.add_indent
        term.getMembers.each {|mem_term| print_occTerm_summary(mem_term)}
        $EmhGlobals.sub_indent
      else
        log "Unrecognized OccTerm type: #{term.getType.getName}\n"
    end
  end
end


def dump_occTerms (occ)
  return unless print_header_line(occ.getTerms.getCount, "OCCTERM")
  dump_occTerm_members(occ.getTerms)
end


def dump_occ (occ, oi_header_array) 
  print_obj_type(occ)

  # Cross-domain references
  #
  puts ""
  $EmhGlobals.do_indent
  block = occ.getBlock
  print_only_lcv(block.getDesign) if print_domain_ref(block)

  mod = occ.getModule
  print_obj_name(mod) if print_domain_ref(mod)

  $EmhGlobals.add_indent
  dump_occModuleInstHeaders(occ, oi_header_array)
  dump_occDesignInsts(occ, oi_header_array)
  dump_occNets(occ)
  dump_occTerms(occ)
  $EmhGlobals.sub_indent
end


def dump_nets (block)
  netColl = block.getNets(OacNetIterAll)

  return unless print_header_line(netColl.getCount, "NET")
  dump_net_members(netColl)
end


def dump_modNets (mod)
  netColl = mod.getNets(OacNetIterAll)

  return unless print_header_line(netColl.getCount, "MODNET")
  dump_modNet_members(netColl)
end


def print_term_summary (term) 
  dump_bits(term)
  print_sss(term) if term.getType.getName == "BusTerm"
  print " %-6s" % term.getTermType.getName
  con_net = term.getNet
  print_obj_name(con_net, " CONN2") if con_net.isValid
  print " PINCON=#{term.getPinConnectMethod.getName}" if term.isBitTerm
end

def print_modTerm_summary (term)
  dump_bits(term)
  print_sss(term) if term.getType.getName == "ModBusTerm"
  print " %-6s" % term.getTermType.getName
  con_net = term.getNet
  print_obj_name(con_net, " CONN2") if con_net.isValid
end

def dump_modTerm_members (term_coll)
  term_coll.each do |term|
    print_modTerm_summary(term)
    case term.getType.getName
      when "ModScalarTerm"
      when "ModBusTermBit"
      when "ModBusTerm"
        mod   = term.getModule
        start = term.getStart
        stop  = term.getStop
        incr  = term.getStep
         
        baseName = term.getDef.getName
        $EmhGlobals.add_indent
        start.step(stop, incr) do |ix|
          term_bit = OaModBusTermBit.find(mod, baseName, ix)
          print_modTerm_summary(term_bit)
        end
        $EmhGlobals.sub_indent
      when "ModBundleTerm"
        $EmhGlobals.add_indent
        term.getMembers.each {|mem_term| print_modTerm_summary(mem_term)}
        $EmhGlobals.sub_indent
      else
        log "Unrecognized ModTerm type: #{term.getType.getName}\n"
    end
  end
end

def dump_term_members (term_coll)
  term_coll.each do |term|
    print_term_summary(term)
    case term.getType.getName
      when "ScalarTerm"
      when "BusTermBit"
      when "BusTerm"
        block = term.getBlock
        start = term.getStart
        stop  = term.getStop
        incr  = term.getStep
          
        baseName = term.getDef.getName
        $EmhGlobals.add_indent
        start.step(stop, incr) do |ix|
          term_bit = OaBusTermBit.find(block, baseName, ix)
          print_term_summary(term_bit)
        end
        $EmhGlobals.sub_indent
      when "BundleTerm"
        $EmhGlobals.add_indent
        term.getMembers.each {|mem_term| print_term_summary(mem_term)}
        $EmhGlobals.sub_indent
      else
        log "Unrecognized Term type: #{term.getType.getName}\n"
    end
  end
end

def dump_terms (block)
  return unless print_header_line(block.getTerms.getCount, "TERM")
  dump_term_members(block.getTerms)
end


def dump_modTerms (mod)
  return unless print_header_line(mod.getTerms.getCount, "MODTERM")
  dump_modTerm_members(mod.getTerms)
end


def print_instTerm (instTerm)
  new_obj_base_id(instTerm)

  print "\" #{instTerm.getTermName}\""
  if instTerm.isImplicit
    print " IM"
  else
    print " EX"
  end

  print " BITS=#{instTerm.getNumBits}"

  if instTerm.isBound
    print " BOUND"
  else
    print " UNBOUND"
  end

  print " POS" if  instTerm.usesTermPosition

  con_net = instTerm.getNet
  print_obj_name(con_net, " CONN2") if con_net.isValid
end


def dump_InstTerms (container)
  return unless print_header_line(container.getInstTerms.getCount, "INSTTERM")
  container.getInstTerms.each {|instTerm|  print_instTerm(instTerm)}
end

def dump_one_inst (inst)
  dump_InstTerms(inst)
end


def dump_props (obj)
  return unless print_header_line(obj.getProps.getCount, "PROP")
  $EmhGlobals.add_indent
  obj.getProps.each do |prop|
    puts ""
    log(prop.getType.getName)
    print " #{prop.getName}=#{prop.getValue}"
  end
  $EmhGlobals.sub_indent
end


def dump_insts (container)
  return unless print_header_line(container.getInsts.getCount, "INST")
  container.getInsts.each do |inst|
    new_obj_base_id(inst)
    print_obj_name(inst)
    print_transform(inst)
    $EmhGlobals.add_indent
    dump_one_inst(inst)
    $EmhGlobals.sub_indent
  end
end


def dump_modDesignInsts (container)
  #
  # If container Type is Module the Iter has both kinds of ModInsts; so to get an
  # accurate count of only the ModDesignInsts, do an initial cycle through the iterator.
  #
  count = 0
  container.getInsts.each {|inst| count += 1 if inst.isModDesignInst}

  return unless print_header_line(count, "MODDESIGNINST")
  #
  # Go through the iterater again, this time printing out ModInst information
  #
  container.getInsts.each do |inst|
    # These asserts won't work. Fake it.
    #assertq("#{inst.getDatabase.getType.getName} == Design")
    #assertq("#{inst.getDatabase} == #{container.getDatabase}")
    assertq(eval('inst.getDatabase.getType.getName == "Design"'),
         'inst.getDatabase.getType.getName == "Design"')
    assertq(eval('inst.getDesign.getCellName == container.getDesign.getCellName'),
         'inst.getDesign.getCellName == container.getDesign.getCellName')
    #
    # If the container Type is Module the Iter has both kinds of ModInsts.
    # Skip the ModModuleInsts since they are dumped from the ModModuleInstHeader
    #
    next if inst.isModModuleInst
    new_obj_base_id(inst)
    if container.getType.getName != "Module"
      print_obj_name(inst.getModule)
      print "|" 
    end
    print_obj_name(inst)
    #
    # No XFORMS in Mod Domain.
    # Don't need to repeat the guts in the ModInstHeader.
    # But DO need to in each Module, since the InstTerms could be connected
    # to different Nets, have different annotations, etc.
    #
    if container.getType.getName == "Module"
      $EmhGlobals.add_indent
      dump_one_inst(inst)
      $EmhGlobals.sub_indent
    else
      bound_status(inst)
      if inst.isBound
        mod = inst.getMasterModule
        if mod.isValid
          print_obj_name(mod)
          print "\[TOP\]" if mod.getDesign.getTopModule.getName == mod.getName
        else
          print "***NULL MASTER MODULE!" 
        end
      end
    end
  end
end


def dump_modModuleInsts (container)
  $EmhGlobals.add_indent
  return unless print_header_line(container.getInsts.getCount, "MODMODULEINST")
  container.getInsts.each do |inst|
    new_obj_base_id(inst)
    print_obj_name(inst)
    # No XFORMS in Mod Domain!!
    $EmhGlobals.add_indent
    dump_one_inst(inst)
    $EmhGlobals.sub_indent
  end
  $EmhGlobals.sub_indent
end


def dump_busNetDefs (container, descrip)
  print_header_line(container.getBusNetDefs.getCount, descrip)

  container.getBusNetDefs.each do |objdef|
    new_obj_base_id(objdef)
    print " BITS=#{objdef.getNumBits}"
    print " MINIX#{$objdef.getMinIndex}"
    print " MAXIX=#{objdef.getMaxIndex}"
    print "  BASENAME=#{objdef.getName}"
    print "  BITS:" 
    objdef.getBusNetBits.each {|bit| print " #{bit.getBitIndex}"}
  end
end


def dump_modInstHeaders (design)
  ih_coll = design.getModInstHeaders
  return unless print_header_line(ih_coll.getCount, "MODINSTHEADER",
        $EmhGlobals.dont_skip_if_zero)

  ih_coll.each do |ih|
    # Pcell processing is taken out of this tcl version.
    #
    next if ih.isSubHeader || ih.isSuperHeader
  
    new_obj_base_id(ih)
    print_only_lcv(ih)
    print_master_info(ih, "DESIGN")
    $EmhGlobals.add_indent
    dump_modDesignInsts(ih)
    $EmhGlobals.sub_indent
  end
end


def dump_modModuleInstHeaders (mod)
  ih_coll = mod.getModuleInstHeaders
  return unless print_header_line(ih_coll.getCount, "MODMODULEINSTHEADER")

  ih_coll.each do |ih|
    #
    # ModModules can't have parameters, so no Super/SubHeaders
    #
    new_obj_base_id(ih)
    print_obj_name_scalar(ih)
    print_mod_master_info(ih)
    dump_modModuleInsts (ih)
  end
end


def dump_instHeaders (block)
  ih_coll = block.getInstHeaders
  return if  ! print_header_line(ih_coll.getCount, "INSTHEADER")

  ih_coll.each do |ih|
    # Pcell processing is taken out of this tcl version.
    #
    next if ih.isSubHeader || ih.isSuperHeader
  
    new_obj_base_id(ih)
    print_master_info(ih, "DESIGN")
    $EmhGlobals.add_indent
    dump_insts(ih)
    $EmhGlobals.sub_indent
  end
end


def dump_modules (design)
  modTop = design.getTopModule

  # First dump the ModInstHeaders, which are scoped to the Design,
  # common across all Modules that use them.
  #
  dump_modInstHeaders(design)

  # Next take each Module in turn, start with its unique ModModuleInstHeaders.
  #
  mod_coll = design.getModules
  puts ""
  print_header_line(mod_coll.getCount, "MODULE", $EmhGlobals.dont_skip_if_zero)
  mod_coll.each do |mod|
    new_obj_base_id(mod)
    print_obj_name(mod)
    if mod.getName == modTop.getName
        print " TOP" 
    elsif mod.isEmbedded
        print " EMBEDDED" 
    end
    if mod.isDerived
        print_orig_lcv(mod)
    end
    $EmhGlobals.add_indent
    dump_modModuleInstHeaders(mod)
    dump_modDesignInsts(mod)
    dump_busNetDefs(mod, "MODBUSNETDEF")
    dump_busTermDefs(mod, "MODBUSTERMDEF")
    dump_modNets (mod)
    dump_modTerms(mod)
    $EmhGlobals.sub_indent
  end
end


def separator (mesg)
  puts "\n"
  log "==================== #{mesg} ====================\n" 
end

def dump_block (block)
  if block.isValid
    print_header_line(1, "BLOCK")
    new_obj_base_id(block)
    $EmhGlobals.add_indent
    dump_instHeaders(block)
    dump_busNetDefs(block, "BUSNETDEF")
    dump_busTermDefs(block, "BUSTERMDEF")
    dump_nets(block)
    dump_terms(block)
    $EmhGlobals.sub_indent
  else
    puts ""
    log "NO BLOCK"
  end
end


def dump_design (design)
  puts "\n"
  log "______________________________ "
  print_obj_type(design)
  print_only_lcv(design)
  $EmhGlobals.add_indent
  if design.isSubMaster
    puts ""
    log "SUBMASTER" 
  end
  $EmhGlobals.sub_indent
  dump_props(design)
  if $EmhGlobals.dump_block_domain
    separator ("BLOCK DOMAIN")
    dump_block(design.getTopBlock)
  end
  if $EmhGlobals.dump_mod_domain
    separator "MODULE DOMAIN" 
    dump_modules(design)
  end
  if $EmhGlobals.dump_occ_domain
    separator "OCCURRENCE DOMAIN"
    puts ""
    $EmhGlobals.do_indent
    occ_top = design.getTopOccurrence
    if occ_top.isValid
      print "\[TOP\]" 
      oi_header_array = []
      dump_occ(occ_top, oi_header_array)
      #
      # Dump out any OccInstHeaders we saved while iterating over
      # OccDesignInsts in the Occurrence.
      #
      dump_occInstHeaders(oi_header_array)
    else
        cout << "NO OCCURRENCE" 
    end
  end
  puts ""
end


def dump_open_designs 
  puts ""
  log "Currently open Designs:"
  OaDesign::getOpenDesigns.each {|design| dump_design(design)}
  puts ""
end
