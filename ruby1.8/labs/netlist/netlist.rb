#!/usr/bin/env ruby
################################################################################
#  Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#  
#  Licensed under the Apache License, Version 2.0 (the "License"); you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http://www.apache.org/licenses/LICENSE-2.0
#  
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   08/03/10  1.0      scarver, Si2     OA Scripting Language Labs
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################

# This program builds a library containing a full adder with its component
# half adder and leaf cells, all connected as shown below:
#
#     Leaf cells              HalfAdder
#                               
#         _____               ____________________     
#     A--|     |             |                    |    
#        | And |--Y          |        A ______    |    
#     B--|_____|          A--|-----o---|      |Y  |    
#                            |     |   | And1 |---|--C 
#         ____               |  o------|______|   |    
#     A--|    |              |  |  |  B           |    
#        | Or |--Y           |  |  |              |    
#     B--|____|              |  |  |  A _____     |    
#                            |  |  o---|      |Y  |    
#         _____              |  |      | Xor1 |---|--S 
#     A--|     |          B--|--o------|______|   |    
#        | Xor |--Y          |        B           |    
#     B--|_____|             |____________________|    
#
# 
#         FullAdder block:             
#         ______________________________________Co_______     
#        |                            _____    |         |    
#        |       A_____C   CA_Net   A|     |Y  |         |    
#     A--|-------|     |-------------| Or1 |---          |
#        | A_Net |     |           --|_____| Co_Net      |    
#        |       |     |          |___________           |    
#        |       | Ha1 |                      |          |    
#        | B_Net |     |            A_____C   |          |    
#     B--|-------|_____|------------|     |---CB_Net     |    
#        |       B     S   SA_Net   |     |              |    
#        |                          |     |              |    
#        |                          | Ha2 |              |    
#        |                          |     |    S_net     |    
#        |                     -----|_____|--------------|--S 
#        |                    |    B       S             |    
#        |             Ci_Net |________________          |    
#        |_____________________________________|_________|    
#                                               Ci
#

require 'oa'
include Oa

$ns = OaNativeNS.new
$mode_write   = "w"
$chp_name_lib   = "Lib"
$chp_name_view  = "netlist"
$chp_path_lib   = "./Lib"
$sc_name_lib    = OaScalarName.new( $ns, $chp_name_lib  )
$sc_name_view   = OaScalarName.new( $ns, $chp_name_view )

def get_term_name( term )

  return '' unless term

  # Return a String representation for the term's name.
  # Use the NameSpace from the global_data singleton.
  term.getName( $ns, name='' )
  return name
end


def get_instTerm_name( instTerm )
  return '' unless instTerm

  # Return a name of the form, Instname.Termname, that is, the name
  # of InstTerm's Inst, followed by a ".", followed by the Term's name.
  # Use the NameSpace from the global_data singleton.
  instTerm.getInst.getName( $ns, instname='' )
  instTerm.getTerm.getName( $ns, termname='' )
  name = instname + '.' + termname
  return name
end


def make_net_and_term( block, chname_term, term_type )

  # Create a Net allowing the database to assign a name to it.
  # Create a Term connected to that Net with the name and type passed in
  # as an args. Use the NameSpace from the global_data singleton.

  net = OaScalarNet.create block
  OaScalarTerm.create( net, OaScalarName.new( $ns, chname_term ), term_type )

  return net
end

def print_nets( block )
  # Print the names of all Nets in the specified Block. For each Net,
  # print the Term names of all Terms and InstTerms attached to it.

  return unless block

  # Using the NameSpace from the globals singleton, get the Cell name of
  # the Block's owner Design in the cellName variable.
  block.getDesign.getCellName( $ns, cellName='' )

  puts "\nThe following Nets exist in Cell: #{cellName}"

  # Set up an iteration to look at each Net in the Block
  block.getNets.each do |net|

    # Get the name of the Net into the net_name variable.
    # (HINT: Map via the namespace from the globals singleton.)
    net.getName( $ns, net_name='' )

    term_names = ''

    # Create an if condition to test that there is at least one Term on the Net.
    if ! net.getTerms.isEmpty
      
      # Set up an iteration to look at each Term on the Net
      net.getTerms.each do |term|
        term_names += get_term_name( term ) + "\t"
      end
    end
    # Create an if condition to test that there is at least one InstTerm connected to the Net.
    if ! net.getInstTerms.isEmpty

      # Set up an iteration to look at each InstTerm on the Net
      net.getInstTerms.each do |instTerm|
        term_names += get_instTerm_name( instTerm ) + "\t"
      end
    end
    puts "Net: #{net_name} is connected to Term: #{term_names}";
  end
  puts ""
end


def make_leaf_cell ( strNameCell )
  
  # Create a netlist type Design with the mode that will overwrite any
  # existing Design of that type and names.
  # (HINT: Use GlobalData variables to help build the scalar names 
  # and other needed arguments.)
  design = OaDesign.open( $sc_name_lib, 
                          OaScalarName.new( $ns, strNameCell ),
                          $sc_name_view,
                          OaViewType.get( OaReservedViewType.new('netlist') ),
                          $mode_write )

  # Create the top Block in the Design to own the design objects.
  block = OaBlock.create design

  # Create the Nets and Terms for the leaf gate.
  #
  make_net_and_term( block, 'A', OaTermType.new( OacInputTermType  ) )
  make_net_and_term( block, 'B', OaTermType.new( OacInputTermType  ) )
  make_net_and_term( block, 'Y', OaTermType.new( OacOutputTermType ) )

  print_nets block
  design.save
  design.close
end

def make_half_adder

  # Open a netlist type Design and name "HalfAdder" with the mode that will
  # overwrite any existing Design of that type and name.
  design_HA = OaDesign.open( $sc_name_lib, 
                            OaScalarName.new( $ns, 'HalfAdder' ),
                            $sc_name_view,
                            OaViewType.get( OaReservedViewType.new('netlist') ),
                            $mode_write )

  # Create a top Block for HalfAdder Design.
  block_HA = OaBlock.create design_HA

  puts "Creating HalfAdder with 1 And Inst and 1 Xor Inst"

  # Create the 4 Terms on this halfadder: A and B are inputs, C and S are outputs.
  a_net = make_net_and_term( block_HA, 'A', OaTermType.new( OacInputTermType ) )
  b_net = make_net_and_term( block_HA, 'B', OaTermType.new( OacInputTermType ) )
  c_net = make_net_and_term( block_HA, 'C', OaTermType.new( OacOutputTermType ) )
  s_net = make_net_and_term( block_HA, 'S', OaTermType.new( OacOutputTermType ) )

  # oaTransform trans is necessary for and Inst even though we don't
  # care about placement at this point in the netlist.
  #
  trans = OaTransform.new( OaPoint.new( 0,0), OaOrient.new( OacR0 ) )

  # Create a scalar Inst named "And1" with the master "And" using the
  # method that takes lib/cell/view names of the master.
  and1 = OaScalarInst.create( block_HA,
                              $sc_name_lib, 
                              OaScalarName.new( $ns, 'And' ),
                              $sc_name_view, 
                              OaScalarName.new( $ns, 'And1' ), 
                              trans )

  # Create by names a scalar Inst named "Xor1" with the master "Xor".
  # of the master.
  xor1 = OaScalarInst.create( block_HA, 
                              $sc_name_lib, 
                              OaScalarName.new( $ns, 'Xor' ),
                              $sc_name_view, 
                              OaScalarName.new( $ns, 'Xor1' ), 
                              trans )

  # Create and hook the InstTerms of And1 and Xor1 to the right Nets.
  #
  make_instTerm_connect_to_net( 'A', and1, a_net )
  make_instTerm_connect_to_net( 'B', and1, b_net )
  make_instTerm_connect_to_net( 'Y', and1, c_net )
  make_instTerm_connect_to_net( 'A', xor1, a_net )
  make_instTerm_connect_to_net( 'B', xor1, b_net )
  make_instTerm_connect_to_net( 'Y', xor1, s_net )

  print_nets block_HA
  design_HA.save
  design_HA.close
end

def make_full_adder
  
  # Create a 'netlist' type Design and name "FullAdder" with the mode that
  # will overwrite any existing Design of that type and name.
  design_FA = OaDesign.open( $sc_name_lib, 
                            OaScalarName.new( $ns, 'FullAdder' ),
                            $sc_name_view,
                            OaViewType.get( OaReservedViewType.new('netlist') ),
                            $mode_write )

  # Create a top Block for FullAdder Design.
  block_FA = OaBlock.create design_FA

  puts "Creating FullAdder with two HalfAdder Insts, one Or Inst";

  a_net  = make_net_and_term( block_FA, 'A',  OaTermType.new( OacInputTermType ) )
  b_net  = make_net_and_term( block_FA, 'B',  OaTermType.new( OacInputTermType ) )
  ci_net = make_net_and_term( block_FA, 'Ci', OaTermType.new( OacInputTermType ) )
  co_net = make_net_and_term( block_FA, 'Co', OaTermType.new( OacOutputTermType ) )
  s_net  = make_net_and_term( block_FA, 'S',  OaTermType.new( OacOutputTermType ) )

  # Create internal Nets SA_Net, CA_Net, CB_Net, using the Net create
  # method that lets the implementation assign the Net name.
  sa_net = OaScalarNet.create block_FA
  ca_net = OaScalarNet.create block_FA
  cb_net = OaScalarNet.create block_FA
  
  # oaTransform trans is necessary to create the Inst even though
  # placement at this netlist stage is irrelevant. Create a null
  # Transform that neither translates nor rotates.
  #
  trans = OaTransform.new( OaPoint.new( 0,0), OaOrient.new( OacR0 ) )

  # Create two ScalarInsts "Ha1" and "Ha2" of master "HalfAdder"
  # and one ScalarInst "Or1" of master "Or" using the create method that 
  # takes the lib/cell/view names of the master.
  inst_ha1 = OaScalarInst.create( block_FA, 
                                 $sc_name_lib, 
                                 OaScalarName.new( $ns, 'HalfAdder' ),
                                 $sc_name_view, 
                                 OaScalarName.new( $ns, 'Ha1' ),
                                 trans )

  inst_ha2 = OaScalarInst.create( block_FA,
                                 $sc_name_lib, 
                                 OaScalarName.new( $ns, 'HalfAdder' ),
                                 $sc_name_view, 
                                 OaScalarName.new( $ns, 'Ha2' ),
                                 trans )

  sc_name_inst_cell = OaScalarName.new( $ns, 'Or'  )
  sc_name_inst_name = OaScalarName.new( $ns, 'Or1' )
  inst_or1 = OaScalarInst.create( block_FA,
                                 $sc_name_lib, 
                                 OaScalarName.new( $ns, 'Or' ),
                                 $sc_name_view, 
                                 OaScalarName.new( $ns, 'Or1' ),
                                 trans )

  # Create and hook the InstTerms to the right Nets.
  #
  make_instTerm_connect_to_net( 'A', inst_ha1, a_net )
  make_instTerm_connect_to_net( 'B', inst_ha1, b_net )
  make_instTerm_connect_to_net( 'C', inst_ha1, ca_net )
  make_instTerm_connect_to_net( 'S', inst_ha1, sa_net )

  make_instTerm_connect_to_net( 'A', inst_ha2, sa_net )
  make_instTerm_connect_to_net( 'B', inst_ha2, ci_net )
  make_instTerm_connect_to_net( 'C', inst_ha2, cb_net )
  make_instTerm_connect_to_net( 'S', inst_ha2, s_net )

  make_instTerm_connect_to_net( 'A', inst_or1, ca_net )
  make_instTerm_connect_to_net( 'B', inst_or1, cb_net )
  make_instTerm_connect_to_net( 'Y', inst_or1, co_net )

  print_nets block_FA
  design_FA.save
  design_FA.close
end


def make_instTerm_connect_to_net ( str_name_term, inst, net )
  # Create an InstTerm of the Term named str_name_term
  # (using the NameSpace in the globs singleton) on the Inst and connected to
  # the Net specified by the input arguments.
  name_term = OaName.new( $ns,  str_name_term )
  OaInstTerm.create( net, inst, name_term )
end


# Declare a proc that takes a path argument and returns true if a Lib exists
# at that path; false otherwise
def libAlreadyOnDisk( path ) 
   if OaLib.exists path
      return true
   else
      return false
   end
end

# ##########################################################################
# "main" program
# ##########################################################################

begin
  oaDesignInit

  # No point in trying oaLib::find() since in this simple program there
  # are no other threads or functions that might have opened the Lib alrready.
  # However, the program may have executed already, in which case a
  # Lib directory will exist and attempting create again on it will hurl.
  #
  if libAlreadyOnDisk $chp_path_lib
      puts "Opening existing Lib name=#{$chp_name_lib} mapped to filesystem path=#{$chp_path_lib}"

      # Open an existing Lib using the sc_name_lib and chp_path_lib from GloblData.
      lib = OaLib.open( $sc_name_lib, $chp_path_lib )
  else
      puts "Creating new Library for Lib name=#{$chp_name_lib} mapped to filesystem path=#{$chp_path_lib}"

      # Create a new Lib in shared mode using the DMFileSys PlugIn.
      lib = OaLib.create( $sc_name_lib, $chp_path_lib,  OaLibMode.new(OacSharedLibMode), 'oaDMFileSys' )
  end

  puts "Overwriting lib.defs file" ;

  # Create/overwrite a lib.defs file in the current directory,
  # that has a library definition mapping sc_name_lib to chp_path_lib.
  ldl = OaLibDefList.get(  "lib.defs",  "w" )
  OaLibDef.create( ldl, $sc_name_lib, $chp_path_lib )
  ldl.save

  # Create the "cell library".
  #

  make_leaf_cell  "And"
  make_leaf_cell  "Or"
  make_leaf_cell  "Xor"
  		
  # Create the design hierarchy.
  #
  make_half_adder
  make_full_adder

  rescue OaException => excp
    abort "Caught exception: #{excp}"
end


#eof
