#! /usr/bin/env ruby
###############################################################################
#
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
##############################################################################
# This program dumps the connectivity and shape data from an OpenAccess
# Design into an ASCII format that can be sorted and diff'ed to compare
# it to the expected dump contents. The output of this command should be
# passed through a "sort" command to allow the results to be deterministic
# (oaIter order cannot be guaranteed).
#
# This does not follow the C++ lab 100% in that the output format may differ
# slightly and the coding style is more suited towards Ruby.
##############################################################################

require 'oa'
include Oa


##############################################################################
# BEGIN: Class definitions / extensions
#
# Ideally these extended class definitions would be in a separate Ruby file
# that could be included and reused by multiple scripts.  However, the
# contents are listed here directly to keep the oadump.rb script
# self-contained.
##############################################################################

module Oa

  # Set default namespace once (used often below)
  DNS = OaNativeNS.new

  class OaDesign
    def getLCV
      getLibName(DNS, lib='')
      getCellName(DNS, cell='')
      getViewName(DNS, view='')
      return lib, cell, view
    end

    def lcv_str
      getLCV.join('/')
    end
  end

  class OaBlock
    # Print high-level Design metadata.
    def dump(fh)
      design = self.getDesign
      fh.print "Design #{design.lcv_str}"

      design.getViewType.getName(vtstr='')
      fh.print " #{vtstr} #{design.getCellType.getName} "

      bbox = getBBox()
      fh.puts "BBOX #{bbox}"

      # Dump objects within this block
      dump_insts fh
      dump_nets fh
      dump_shapes fh
      dump_routes fh
    end

    def dump_insts(fh)
      getInsts.each {|inst| inst.dump(fh)}
    end

    def dump_nets(fh)
      getNets.each {|net| net.dump(fh)}
    end

    def dump_shapes(fh)
      getShapes.each {|shape| shape.dump(fh)}
    end

    def dump_routes(fh)
      getRoutes.each {|route| route.dump(fh)}
    end
  end

  class OaBox
    def to_s
      sprintf "((%d, %d) (%d, %d))", left, bottom, right, top
    end
  end

  class OaPoint
    def to_s
      sprintf "(%d, %d)", x, y
    end
  end

  class OaInst 
    def getILCV
      getName(DNS, name='')
      getLibName(DNS, lib='')
      getCellName(DNS, cell='')
      getViewName(DNS, view='')
      [name, lib, cell, view]
    end
    
    def dump(fh)
      name, lib, cell, view = getILCV
      origin = getOrigin()
      bbox = getBBox()

      # Print the data on the Inst
      fh.print "Inst #{name} OF #{lib}/#{cell}/#{view} "
      fh.puts "#{getOrient.getName} LOC #{origin} BBOX #{bbox}"

      getInstTerms.each {|iterm| iterm.dump(fh, name)}
    end
  end

  class OaInstTerm
    def dump(fh, inst_name='')
      net = getNet
      net_name = ''
      net.getName(DNS, net_name) if net
      getTermName(DNS, name='')
      fh.puts "InstTerm #{inst_name} of #{name} is on #{net_name}"
    end

    def dump_rt_point(fh)
      fh.printf("[%s/%s] ", (getInst.getName(DNS, iname='') || iname),
                (getTerm.getName(DNS, tname='') || tname))
    end
  end
  
  class OaNet
    def dump(fh)
      # Print the Net line
      getName(DNS, name='')
      fh.puts "Net #{name} #{getSigType.getName}"

      # Print a line for each Term on this Net
      getTerms.each {|term| term.dump(fh)}
    end
  end

  class OaTerm
    def dump(fh)
      getName(DNS, tname='')
      getNet.getName(DNS, nname='')
      fh.print "Term #{tname} on #{nname} type #{getTermType.getName}"
      getPins.each {|pin| pin.dump(fh)}
      fh.puts
    end
  end

  class OaPin
    def dump(fh)
      getName(name='')
      fh.print " Pin=#{name}"
    end

    def dump_rt_point(fh)
      fh.printf " %s[%s]", (getName(str='') || str), (getTerm.getName(DNS, str='') || str)
    end    
  end

  class OaShape
    def dump(fh)
      # Start the Shape line with its type and bounding box
      bbox = getBBox()
      fh.print "Shape #{getType.getName}"

      fh.printf "\"%s\"", (getText(text='') || text) if getType.to_i == OacTextType
      fh.print " at #{bbox}"

      # Add layer and purpose names to the line
      lnum = getLayerNum
      pnum = getPurposeNum

      if tech=getDesign.getTech
        layer = OaLayer.find tech, lnum
        purp = OaPurpose.find tech, pnum
        lstr = ''
        pstr = ''
        layer.getName lstr if layer
        purp.getName pstr if purp
        fh.print " LPP #{lstr}/#{pstr}"
      else
        # If there is no Tech, can't bind the Layer and Purpose nums to names;
        # so just print the number pair instead.
        fh.print " LPP #{lnum}/#{pnum}"
      end

      # Note if this is a Pin Shape.
      if hasPin and pin=getPin
        fh.printf " on pin %s", (pin.getName(pname='') || pname)
      end

      # Note if this Shape is attached to a Net. 
      if hasNet and net=getNet
        fh.printf " on net %s", (net.getName(DNS, nname='') || nname)
      end

      # TODO: Need casting operator for OaType...
      fh.print ' ROUTE' if getType.getName == 'PathSeg' and hasRoute
      fh.puts
    end

    def dump_rt_point(fh)
      box = getBBox()
      fh.print " at #{box} on #{getLayerNum}/#{getPurposeNum} "
    end    
  end

  class OaRoute
    def dump(fh)
      fh.print "Route ST #{getRouteStatus.getName} "
      if net = getNet
        fh.printf "NET %s ", (net.getName(DNS, nname='') || nname)
      end

      dump_rt_point fh, getBeginConn, 'START'
      dump_rt_point fh, getEndConn, 'END'

      getObjects(relems=[])
      relems.each {|relem| relem.dump_rt_element fh}
      fh.puts
    end

    def dump_rt_point(fh, pt, key)
      if pt
        fh.print "#{key} #{pt.getType.getName}"
        pt.dump_rt_point(fh)
      else
        fh.print "#{key} NULL "
      end
    end
  end

  class OaSteiner
    def dump_rt_point(fh) ; end
  end

  class OaStdVia
    def dump_rt_point(fh) ; end
  end

  class OaCustomVia
    def dump_rt_point(fh) ; end
  end

  class OaPathSeg
    def dump_rt_element(fh)
      getPoints(ptst=OaPoint.new, pten=OaPoint.new)
      fh.print "SEG at (#{ptst} #{pten}) on #{getLayerNum}"
    end
  end

  class OaVia
    def dump_rt_element(fh)
      pt = getOrigin()
      fh.print "VIA #{getOrient.getName} at #{pt} "
    end
  end

  class OaFig
    def dump_rt_element(fh)
      warn "Invalid casting for OaRouteObjectArray (#{inspect})"
    end
  end
  
end

##############################################################################
# END: Class definitions / extensions
##############################################################################
# BEGIN: Main code
##############################################################################

# This "if" statement is in place to allow the above functions to be used as
# a library if this file were to be required and used in another Ruby script.
# The __FILE__ == $0 will be true when this is called as a script.

if __FILE__ == $0

  lib_defs = 'lib.defs'

  oaDesignInit(OacAPIMajorRevNumber, OacAPIMinorRevNumber, OacDataModelRevNumber)

  # Check arguments
  lib, cell, view, cvtype = ARGV
  abort "oadump.rb <libraryName> <cellName> <viewName> [viewType]" unless view

  # Parse the library definitions in the local lib.defs file.
  abort "Missing lib.defs file"  unless File.exists?(lib_defs)
  OaLibDefList.openLibs lib_defs

  unless oalib = OaLib.find(lib)
    abort "Either Lib name=#{lib} or its path mapping in file=#{lib_defs} is invalid."
  end

  begin
    if cvtype
      # In Ruby, both of the following invoke the same oaDesign::open() overload
      # that uses an oaViewType.  The first one (commented out) is the "normal" way, 
      # and the second one illustrates the conversion of the cvtype string to an 
      # OA symbol.
      #design = OaDesign.open(lib, cell, view,
      #         OaViewType.get( OaReservedViewType.new( cvtype ) ), 'r')
      design = OaDesign.open(lib, cell, view, cvtype.to_sym, 'r')
    else
      design = OaDesign.open(lib, cell, view, 'r')
    end
    
    puts "Reading Design #{lib}/#{cell}/#{view}"
    block = design.getTopBlock

    # Make sure bounding box of parent design gets updated with shapes
    # in all its levels of hierarchy
    design.openHier 200

    file_name = cell + view + '.dump'
    File.open(file_name, 'w') do |outf|
      block.dump outf
    end

    # Sort output to same file name
    system "sort --ignore-case -o #{file_name} #{file_name}"
    
  rescue OaException => excp
    abort "Error while reading design: #{lib}/#{cell}/#{view}:\n\t#{excp}"
  end

end
