###############################################################################
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   09/07/10  1.0      scarver, Si2     OA Scripting Language Labs
#
###############################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
##############################################################################

def unexpected_exception (excp, lineno)
  puts "Unexpected Exception at line: #{lineno}"
  puts "#{excp}"
  puts "Quitting."
  exit 2
end

def assert ( condition, lineno) 
    puts "***ASSERT error #{lineno}" if !condition
end

def set_layer_purpose (scname_lib, layer_name, layer_num_in, purp_name_in, 
                       purp_num_in, close_tech)
  # First see if a Tech database can be found already open for the lib,
  # i.e., from a prior call to set_layer_purpose (or any other module
  # if this were part of a large application).
  #
  tech = OaTech.find( scname_lib )
  unless tech
    #
    # If one's not already open in memory, try opening it from disk.
    #
    begin
      tech = OaTech.open( scname_lib, 'a' )
      #
      # No Tech open in memory, none on disk by that name. So create one.
      #
      rescue => excp
      if excp.to_s.include? OacCannotFindLibraryTech.to_s
        tech = OaTech.create( scname_lib )
        #
        # Set the DBU to 1/3 nanometer for all Netlist type CellViews.
        #
        view_type = OaViewType.get( OaReservedViewType.new(OacNetlist) )
        tech.setUserUnits(view_type, OacMicron)
        tech.setDBUPerUU( view_type, 3000)
      else 
        unexpected_exception( excp, __LINE__ )
      end
    end
  end
  # Try locating a Purpose of the given name. If so, overwrite
  # the purp_num defaulted above with the value found in the Tech.
  # Else, create a new Purpose with this name and num in the Tech.
  #
  purpose = OaPurpose.find(tech, purp_name_in)
  if purpose
    purp_num = purpose.getNumber
    puts "Found Purpose name=#{purp_name_in}  num=%x" % purp_num
  else 
    purpose = OaPurpose.create(tech, purp_name_in, purp_num_in)
    purp_num = purpose.getNumber
    puts "Created Purpose name=#{purp_name_in}  num=%x" % purp_num
  end
  #
  # If there is a Layer by the given name in the Tech already, get its num.
  # Else create one.
  #
  layer = OaLayer.find(tech, layer_name)
  if layer
    layer_num = layer.getNumber
    puts "Found Layer name=#{layer_name}  num=#{layer_num}"
  else 
    layer = OaPhysicalLayer.create(tech, layer_name, layer_num_in)
    layer_num = layer.getNumber
    puts "Created Layer name=#{layer_name} num=#{layer_num}"
  end
  if close_tech
    tech.save
    tech.close
  end
  return layer_num, purp_num
end


def map_libs (chp_name_libdefsfile, scname_lib_prev, scname_lib_curr, chp_pathlib_curr)

  ns = OaUnixNS.new

  # Perform logical lib name to physical path name mappings for the source library
  # created in the netlist/ lab, then the symbol library to be created in this lab.
  #
  # Make sure the lib.defs is LOCAL to avoid clobbering a real one.

  OaLibDefList.getDefaultPath( strLibDefListDef='' )

  if strLibDefListDef != chp_name_libdefsfile
    puts "***Missing local #{chp_name_libdefsfile} file."
    puts "   Please create one with the one line shown below: "
    puts "       INCLUDE ../netlist/lib.defs"
    exit 4
  end

  OaLibDefList.openLibs

  lib_prev = OaLib.find(scname_lib_prev)
  unless lib_prev
    puts  "***Either lib.defs file missing or netlist lab was not completed."
    exit 1
  end

  scname_lib_curr.get(ns, strNameLibCurr='' )

  lib_curr = OaLib.find( scname_lib_curr )

  if lib_curr
    puts "#{strNameLibCurr} Lib definition found in #{chp_name_libdefsfile} and opened."
  else 
    # If this program was run once already, a Lib directory will exist.
    # If the lib.defs file was deleted, the Lib for this current lab won't
    # have been opened by oaLibDefFile::load(), so it must be opened now.
    # If the updated lib.defs (with the def for this lab's Lib) remains but
    # this lab's Lib directory was deleted, then the oaLib::find() above would
    # have failed and the exists() test below will also fail;
    # hence, the subsequent create() will be invoked.
    #
    if OaLib.exists( strNameLibCurr )
      puts "Opened from disk existing Lib=#{strNameLibCurr}"
      lib_curr = OaLib.open( scname_lib_curr, chp_pathlib_curr)
    else 
      # If there is no directory for the Lib, then it must be created now.
      #
      lib_curr = OaLib.create( scname_lib_curr, chp_pathlib_curr, OacSharedLibMode, "oaDMFileSys" )
      puts "Created new Lib=#{strNameLibCurr}"
      #
      # Update the lib.defs file for future labs that reference this Design.
      #
      ldl = OaLibDefList.get( strLibDefListDef, 'a' )
      OaLibDef.create( ldl, scname_lib_curr, chp_pathlib_curr )
      ldl.save
    end
  end
  return lib_prev, lib_curr
end

#
# Many of these values are reused by subsequent labs.
#

class GlobalData 
  private_class_method :new
  @@globs              = nil
  @@ns                 = OaNativeNS.new
  @@chp_name_lib_netlist  = "Lib"
  @@chp_name_lib_symbol   = "LibSymbol"
  @@chp_name_view_netlist = "netlist"
  @@chp_name_view_symbol  = "symbol"
  @@chp_name_libdefsfile = "lib.defs"
  @@chp_pathlib_symbol   = "./LibSymbol"
  @@scname_lib_netlist   = OaScalarName.new(@@ns, @@chp_name_lib_netlist)
  @@scname_lib_symbol    = OaScalarName.new(@@ns, @@chp_name_lib_symbol)
  @@scname_view_netlist  = OaScalarName.new(@@ns, @@chp_name_view_netlist)
  @@scname_view_symbol   = OaScalarName.new(@@ns, @@chp_name_view_symbol)

  @@mode_write         = 'w'
  @@mode_read          = 'r'
  @@mode_append        = 'a'

  # Layer, purpose settings are tool/methodology-specific.
  # These are rather arbitrary

  @@str_layer_dev    = "device"
  @@str_layer_text   = "text"
  @@str_purpose_dev  = "drawing"
  @@str_purpose_text = "labels"

  @@num_layer_dev  = 230
  @@num_layer_text = 229

  @@num_purpose_dev  = OavPurposeNumberDrawing
  @@num_purpose_text = 1013

  @@lib_netlist = nil
  @@lib_symbol  = nil

  def init
    lib_netlist, lib_symbol = map_libs chp_name_libdefsfile, scname_lib_netlist,
            scname_lib_symbol, chp_pathlib_symbol

    # Set up Layer and Purpose nums for the layers in the Tech database.
    # to be used for the device shapes.
    #
    num_layer_dev, num_purpose_dev = set_layer_purpose( 
                @@scname_lib_symbol, @@str_layer_dev, @@num_layer_dev, 
                @@str_purpose_dev, @@num_purpose_dev,  false)
    num_layer_text, num_purpose_text = set_layer_purpose( 
                @@scname_lib_symbol, @@str_layer_text, @@num_layer_text, 
                @@str_purpose_text, @@num_purpose_text, true)
  end

  def GlobalData.create
    if ! @@globs
      @@globs = new
      @@globs.init
    end
    @@globs
  end
  def ns
    @@ns
  end
  def chp_name_lib_netlist
    @@chp_name_lib_netlist
  end
  def chp_name_lib_symbol
    @@chp_name_lib_symbol
  end
  def chp_name_view_netlist
    @@chp_name_view_netlist
  end
  def chp_name_view_symbol
    @@chp_name_view_symbol
  end
  def chp_name_libdefsfile
    @@chp_name_libdefsfile
  end
  def chp_pathlib_symbol
    @@chp_pathlib_symbol
  end
  def scname_lib_netlist
    @@scname_lib_netlist
  end
  def scname_lib_symbol
    @@scname_lib_symbol
  end
  def scname_view_netlist
    @@scname_view_netlist
  end
  def scname_view_symbol
    @@scname_view_symbol
  end

  def mode_write
    @@mode_write
  end
  def mode_read
    @@mode_read
  end
  def mode_append
    @@mode_append
  end

  def str_layer_dev
    @@str_layer_dev
  end
  def str_layer_text
    @@str_layer_text
  end
  def str_purpose_dev
    @@str_purpose_dev
  end
  def str_purpose_text
    @@str_purpose_text
  end

  def num_layer_dev
    @@num_layer_dev
  end
  def num_layer_text
    @@num_layer_text
  end

  def num_purpose_dev
    @@num_purpose_dev
  end
  def num_purpose_text
    @@num_purpose_text
  end

  def lib_netlist
    @@lib_netlist
  end
  def lib_symbol
    @@lib_symbol
  end
end

# GlobalData is a singleton class.

$GlobalData = GlobalData.create

