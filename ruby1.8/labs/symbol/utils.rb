###############################################################################
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   09/07/10  1.0      scarver, Si2     OA Scripting Language Labs
#
###############################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
##############################################################################

def get_block( design )
  # Locate the top Block for the Design.
  return design.getTopBlock
end

def get_cell_name( design )
  design.getCellName( $GlobalData.ns, cell_name='' )
  return cell_name
end

def get_lib_name( design )
  design.getLibName( $GlobalData.ns, lib_name='' )
  return lib_name
end

def get_view_name( design )
  design.getViewName( $GlobalData.ns, view_name='' )
  return view_name
end

def print_views( lib )
  ns = OaUnixNS.new
  lib.getName( ns, lib_name='' )
  puts "---Views in lib named #{lib_name}"
  lib.getViews.each do | view |
    view.getName( $GlobalData.ns, view_name='' )
    view_type_name = view.getViewType.getName
    puts "  #{view_name} VT=#{view_type_name}"
  end
end


def open_design( scname_lib, scname_cell, scname_view, mode )
  # In Ruby, you don't have to do "scalarName.get" to obtain the string
  # when printing.  The to_s method does it for you.

  puts "Opening Design #{scname_lib} #{scname_cell} #{scname_view}" 

  viewType = OaViewType.get( OaReservedViewType.new( "netlist" ) )
  begin
    design = OaDesign.open( scname_lib, scname_cell, scname_view, viewType, mode)
    rescue => excp
      lineno = __LINE__ - 2
      puts "Can't find Design. Check lib.defs file." 
      unexpected_exception( excp, lineno )
  end
  return design
end


def make_sch_sym_view( chp_name_cell )
  scname_cell = OaScalarName.new( $GlobalData.ns, chp_name_cell )
  design = open_design( $GlobalData.scname_lib_netlist, scname_cell,
             $GlobalData.scname_view_netlist, $GlobalData.mode_read )

  block = get_block( design )
  assert( block.isValid, __LINE__ )

  # Save the new type of Design under the same cell/view names
  # but with in a new library local to this lab ($GlobalData.scname_lib_symbol).
  # NOTE: there is no way to change the ViewType with which a Design was created.
  # Then close that source Design.
  design.saveAs( $GlobalData.scname_lib_symbol, scname_cell, $GlobalData.scname_view_symbol )
  block = get_block( design )
  assert( block.isValid, __LINE__ )

  design.close

  puts "  Saved design from Lib=#{$GlobalData.chp_name_lib_netlist} " +
           "into Lib=#{$GlobalData.chp_name_lib_symbol}"

  # Use open_design() to open the newly created Design (in LibSymbol) in APPEND mode.
  # Return the Design handle.
  design = open_design( $GlobalData.scname_lib_symbol, scname_cell,
             $GlobalData.scname_view_symbol, $GlobalData.mode_append )

  block = get_block( design )
  assert( block.isValid, __LINE__ )
  return design
end


def save_close_design( design )
  puts "  Saving \"#{get_cell_name( design )}\" in symbol library named \"#{get_lib_name( design )}\""
  design.save
  design.close
end


def make_label( block, label_position )
  # Using data from the globals singleton, create a Text Object that consists
  # of the Design's cell name. Place it at the label_position, aligned lower left,
  # no rotation, a fixed font, height=4, no overbar, with the flags set to
  # indicate that it is to be visible and displayed in drafting mode.
  design = block.getDesign
  text = OaText.create( block, $GlobalData.num_layer_text,
           $GlobalData.num_purpose_text, get_cell_name( design ),
           label_position,  OacLowerLeftTextAlign, OacR0,
           OacFixedFont, 4 )

  # Transfer the text value of the oaText managed object into the str utility variable.
  text.getText( str='' )

  puts "  Created label = #{str}"

  return str
end
