###############################################################################
#
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   02/06/2010: Copied from Python bindings (authored by AMD) and modified by
#               Intel Corporation for Ruby.
#
###############################################################################


module Oa

  # Base class for template specializations of oa*AppDef<class T>
  class OaAppDefTemplate

    # Create the template wrapper for appDefType and objType
    def initialize(appDefType, objType)
      unless (objType.ancestors.member?(OaObject))
        raise TypeError, 'usage: Oa::Oa*AppDef[type], e.g., Oa::OaIntAppDef[Oa::OaNet]'
      end
      
      unless ((objType.constants & ['Domain', 'DtIndex', 'DbType']).length == 3)
        raise TypeError, "Can't use type #{objType.class} because it doesn't have a Domain, DtIndex, and/or DbType constant"
      end

      @appDefType = appDefType
      @objType = objType
    end

    # More natural way of specifying the Oa*AppDefTemplate class - to be used
    # by the inherited classes; e.g. Oa::OaIntAppDef[Oa::OaNet]
    def self.[](objType) ; self.new(objType) ; end

    # Class method to find an AppDef common to all oa*AppDef types
    def find (name, objDef=nil)
      if objDef
        OaAppDefProxy.findProxy(name, @objType::DbType, @objType::DtIndex, OaDomain.new(@objType::Domain), objDef)
      else
        result = OaAppDefProxy.findProxy(name, @objType::DbType, @objType::DtIndex, OaDomain.new(@objType::Domain))
        if (result and (result.getType.to_i == @appDefType))
          result
        else
          nil
        end
      end
    end

    protected

    # Helper method to get the appdef from an argument listing if the appdef is
    # the first element of the argument list.  Used internally for method
    # dispatching.
    def split_appdef(args)
      if (not args.empty? and args[0].class == OaAppObjectDef)
        [args[0], args[1..-1]]
      else
        [nil, args]
      end
    end
    
  end

  ############################################################################

  # Represents a template specialization of oaIntAppDef<class T>
  class OaIntAppDefTemplate < OaAppDefTemplate

    def initialize(objType)
      super(OacIntAppDefType, objType)
    end

    def get(name, *args)
      appObjDef, args = split_appdef(args)

      case args.length
      when 0 : defValue, persist = 0, true
      when 1 : defValue, persist = args[0], true
      when 2 : defValue, persist = args
      else
        raise TypeError, "Too many arguments to OaIntAppDefTemplate.get"
      end

      if appObjDef
        getArgs = [name, Oa::OacNullIndex, Oa::OacNullIndex, Oa::OacNoDomain, persist, defValue, appObjDef]
      else
        getArgs = [name, @objType::DbType, @objType::DtIndex, OaDomain.new(@objType::Domain), persist, defValue, nil]
      end
      
      return OaIntAppDefProxy.getProxy(*getArgs)
    end
    
  end
  
  ############################################################################

  # Represents a template specialization of oaFloatAppDef<class T>
  class OaFloatAppDefTemplate < OaAppDefTemplate
    
    def initialize(objType)
      super(OacFloatAppDefType, objType)
    end
    
    def get(name, *args)
      appObjDef, args = split_appdef(args)

      case args.length
      when 0 : defValue, persist = 0.0, true
      when 1 : defValue, persist = args[0], true
      when 2 : defValue, persist = args
      else
        raise TypeError, "Too many arguments to OaFloatAppDefTemplate.get"
      end

      if appObjDef
        getArgs = [name, Oa::OacNullIndex, Oa::OacNullIndex, Oa::OacNoDomain, persist, defValue, appObjDef]
      else
        getArgs = [name, @objType::DbType, @objType::DtIndex, OaDomain.new(@objType::Domain), persist, defValue, nil]
      end
      
      return OaFloatAppDefProxy.getProxy(*getArgs)
    end
    
  end
  
  ############################################################################

  # Represents a template specialization of oaStringAppDef<class T>
  class OaStringAppDefTemplate < OaAppDefTemplate
    
    def initialize(objType)
      super(OacStringAppDefType, objType)
    end
    
    def get(name, *args)
      appObjDef, args = split_appdef(args)

      case args.length
      when 0 : defValue, persist = '', true
      when 1 : defValue, persist = args[0], true
      when 2 : defValue, persist = args
      else
        raise TypeError, "Too many arguments to OaStringAppDefTemplate.get"
      end

      if appObjDef
        getArgs = [name, Oa::OacNullIndex, Oa::OacNullIndex, Oa::OacNoDomain, persist, defValue, appObjDef]
      else
        getArgs = [name, @objType::DbType, @objType::DtIndex, OaDomain.new(@objType::Domain), persist, defValue, nil]
      end
      
      return OaStringAppDefProxy.getProxy(*getArgs)
    end
    
  end

  ############################################################################

  # Represents a template specialization of oaIntraPointerAppDef<class T>
  class OaIntraPointerAppDefTemplate < OaAppDefTemplate
    
    def initialize(objType)
      super(OacIntraPointerAppDefType, objType)
    end
    
    def get(name, *args)
      appObjDef, args = split_appdef(args)

      case args.length
      when 0 : persist = true
      when 1 : persist = args[0]
      else
        raise TypeError, "Too many arguments to OaIntraPointerAppDefTemplate.get"
      end

      if appObjDef
        getArgs = [name, Oa::OacNullIndex, Oa::OacNullIndex, Oa::OacNoDomain, persist, appObjDef]
      else
        getArgs = [name, @objType::DbType, @objType::DtIndex, OaDomain.new(@objType::Domain), persist, nil]
      end
      
      return OaIntraPointerAppDefProxy.getProxy(*getArgs)
    end
    
  end

  ############################################################################

  # Represents a template specialization of oaInterPointerAppDef<class T>
  class OaInterPointerAppDefTemplate < OaAppDefTemplate
    
    def initialize(objType)
      super(OacInterPointerAppDefType, objType)
    end
    
    def get(name, *args)
      appObjDef, args = split_appdef(args)

      case args.length
      when 0 : persist = true
      when 1 : persist = args[0]
      else
        raise TypeError, "Too many arguments to OaInterPointerAppDefTemplate.get"
      end

      if appObjDef
        getArgs = [name, Oa::OacNullIndex, Oa::OacNullIndex, Oa::OacNoDomain, persist, appObjDef]
      else
        getArgs = [name, @objType::DbType, @objType::DtIndex, OaDomain.new(@objType::Domain), persist, nil]
      end
      
      return OaInterPointerAppDefProxy.getProxy(*getArgs)
    end
    
  end

  ############################################################################

  # Represents a template specialization of oaDataAppDef<class T>
  class OaDataAppDefTemplate < OaAppDefTemplate
    
    def initialize(objType)
      super(OacDataAppDefType, objType)
    end
    
    def get(name, *args)
      appObjDef, args = split_appdef(args)

      case args.length
      when (0..1) : raise TypeError, "Too few arguments to OaDataAppDefTemplate.get"
      when 2: dataSize, defValue, persist = args[0], args[1], true
      when 3: dataSize, defValue, persist = args
      else
        raise TypeError, "Too many arguments to OaDataAppDefTemplate.get"
      end

      if appObjDef
        getArgs = [name, Oa::OacNullIndex, Oa::OacNullIndex, Oa::OacNoDomain, dataSize, defValue, persist, appObjDef]
      else
        getArgs = [name, @objType::DbType, @objType::DtIndex, OaDomain.new(@objType::Domain), dataSize, defValue, persist, nil]
      end
      
      return OaDataAppDefProxy.getProxy(*getArgs)
    end
    
  end
  
  ############################################################################

  # Represents a template specialization of oaVarDataAppDef<class T>
  class OaVarDataAppDefTemplate < OaAppDefTemplate
    
    def initialize(objType)
      super(OacVarDataAppDefType, objType)
    end
    
    def get(name, *args)
      appObjDef, args = split_appdef(args)

      case args.length
      when 0 : defSize, defValue, persist = nil, nil, true
      when 1
        if args[0].is_a?(TrueClass) or args[0].is_a?(FalseClass)
          defSize, defValue, persist = nil, nil, args[0]
        else
          defSize, defValue, persist = args[0], nil, true
        end
      when 2 : defSize, defValue, persist = args[0], args[1], true
      when 3 : defSize, defValue, persist = args
      else
        raise TypeError, "Too many arguments to OaVarDataAppDefTemplate.get"
      end

      if appObjDef
        getArgs = [name, Oa::OacNullIndex, Oa::OacNullIndex, Oa::OacNoDomain, dataSize, defValue, persist, appObjDef]
      else
        getArgs = [name, @objType::DbType, @objType::DtIndex, OaDomain.new(@objType::Domain), dataSize, defValue, persist, nil]
      end
      
      return OaVarDataAppDefProxy.getProxy(*getArgs)
    end
    
  end

  ############################################################################

  # Represents a template specialization of oaTimeAppDef<class T>
  class OaTimeAppDefTemplate < OaAppDefTemplate

    def initialize(objType)
      super(OacTimeAppDefType, objType)
    end

    def get(name, *args)
      appObjDef, args = split_appdef(args)

      case args.length
      when 0 : defValue, persist = Time.at(0), true
      when 1 : defValue, persist = args[0], true
      when 2 : defValue, persist = args
      else
        raise TypeError, "Too many arguments to OaTimeAppDefTemplate.get"
      end

      if appObjDef
        getArgs = [name, Oa::OacNullIndex, Oa::OacNullIndex, Oa::OacNoDomain, persist, defValue, appObjDef]
      else
        getArgs = [name, @objType::DbType, @objType::DtIndex, OaDomain.new(@objType::Domain), persist, defValue, nil]
      end
      
      return OaTimeAppDefProxy.getProxy(*getArgs)
    end
    
  end

  ############################################################################

  # Represents a template specialization of oaDoubleAppDef<class T>
  class OaDoubleAppDefTemplate < OaAppDefTemplate

    def initialize(objType)
      super(OacDoubleAppDefType, objType)
    end

    def get(name, *args)
      appObjDef, args = split_appdef(args)

      case args.length
      when 0 : defValue, persist = 0.0, true
      when 1 : defValue, persist = args[0], true
      when 2 : defValue, persist = args
      else
        raise TypeError, "Too many arguments to OaDoubleAppDefTemplate.get"
      end

      if appObjDef
        getArgs = [name, Oa::OacNullIndex, Oa::OacNullIndex, Oa::OacNoDomain, persist, defValue, appObjDef]
      else
        getArgs = [name, @objType::DbType, @objType::DtIndex, OaDomain.new(@objType::Domain), persist, defValue, nil]
      end
      
      return OaDoubleAppDefProxy.getProxy(*getArgs)
    end
    
  end
  
  ############################################################################

  # Represents a template specialization of oaVoidPointerAppDef<class T>
  class OaVoidPointerAppDefTemplate < OaAppDefTemplate

    def initialize(objType)
      super(OacVoidPointerAppDefType, objType)
    end

    def get(name, *args)
      appObjDef, args = split_appdef(args)

      if appObjDef
        getArgs = [name, Oa::OacNullIndex, Oa::OacNullIndex, Oa::OacNoDomain, appObjDef]
      else
        getArgs = [name, @objType::DbType, @objType::DtIndex, OaDomain.new(@objType::Domain), nil]
      end
      
      return OaVoidPointerAppDefProxy.getProxy(*getArgs)
    end
    
  end

  ############################################################################

  # Represents a template specialization of oaBooleanAppDef<class T>
  class OaBooleanAppDefTemplate < OaAppDefTemplate

    def initialize(objType)
      super(OacBooleanAppDefType, objType)
    end

    def get(name, *args)
      appObjDef, args = split_appdef(args)

      case args.length
      when 0 : defValue, persist = false, true
      when 1 : defValue, persist = args[0], true
      when 2 : defValue, persist = args
      else
        raise TypeError, "Too many arguments to OaBooleanAppDefTemplate.get"
      end

      if appObjDef
        getArgs = [name, Oa::OacNullIndex, Oa::OacNullIndex, Oa::OacNoDomain, persist, defValue, appObjDef]
      else
        getArgs = [name, @objType::DbType, @objType::DtIndex, OaDomain.new(@objType::Domain), persist, defValue, nil]
      end
      
      return OaBooleanAppDefProxy.getProxy(*getArgs)
    end
    
  end

  # Create Oa*AppDef classes mapping to the Oa*AppDefTemplate equivalents
  OaIntAppDef = OaIntAppDefTemplate
  OaFloatAppDef = OaFloatAppDefTemplate
  OaStringAppDef = OaStringAppDefTemplate
  OaIntraPointerAppDef = OaIntraPointerAppDefTemplate
  OaInterPointerAppDef = OaInterPointerAppDefTemplate
  OaDataAppDef = OaDataAppDefTemplate
  OaVarDataAppDef = OaVarDataAppDefTemplate
  OaTimeAppDef = OaTimeAppDefTemplate
  OaDoubleAppDef = OaDoubleAppDefTemplate
  OaVoidPointerAppDef = OaVoidPointerAppDefTemplate
  OaBooleanAppDef = OaBooleanAppDefTemplate
  
end
