/* ****************************************************************************

   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Intel Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************

   Change Log:
     02/16/2010: Copied from Python bindings (authored by AMD) and modified by
                 Intel Corporation for Ruby.

 *************************************************************************** */

#undef INTARRAY

%define INTARRAY(safetype,paramtype)

%ignore OpenAccess_4::oaArrayBase<paramtype>;
%ignore OpenAccess_4::oaArray<paramtype>;

%typemap(in) OpenAccess_4::oaArray<paramtype>& (paramtype tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) const OpenAccess_4::oaArray<paramtype>& (oaArray<paramtype> tmpArr)
{
    if (TYPE($input) != T_ARRAY) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    size_t arrSize = RARRAY($input)->len;
    size_t i;
    for (i = 0; i < arrSize; ++i) {
	VALUE rbobj = rb_ary_entry($input, i);
	long intVal = NUM2LONG(rbobj);
	tmpArr.append((paramtype) intVal);
    }

    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<paramtype>&, const OpenAccess_4::oaArray<paramtype>&
{
    $1 = (TYPE($input) == T_ARRAY);
}

%typemap(argout) OpenAccess_4::oaArray<paramtype>&
{
    oaUInt4 i, len;
    len = $1->getNumElements();
    for (i = 0; i < len; ++i) {
	paramtype val = $1->get(i);
	VALUE rbobj = LONG2NUM((long) val);
	rb_ary_push($input, rbobj);
    }
}

%typemap(argout) const OpenAccess_4::oaArray<paramtype>& {}

%apply OpenAccess_4::oaArray<paramtype>& { OpenAccess_4::oaArray<paramtype>* };
%apply const OpenAccess_4::oaArray<paramtype>& { const OpenAccess_4::oaArray<paramtype>* };

%enddef

/**/

#undef INTARRAYEXT
%define INTARRAYEXT(ns,class,safetype,paramtype)

%ignore ns::class;

%typemap(in) (ns::class&) (ns::class tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const ns::class&) (ns::class tmpArr)
{
    if (TYPE($input) != T_ARRAY) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    size_t arrSize = RARRAY($input)->len;
    size_t i;
    for (i = 0; i < arrSize; ++i) {
	VALUE rbobj = rb_ary_entry($input, i);
	long intVal = NUM2LONG(rbobj);
	tmpArr.append((paramtype) intVal);
    }

    $1 = &tmpArr;
}

%typecheck(0) ns::class&
{
    $1 = (TYPE($input) == T_ARRAY);
}

%typemap(argout) ns::class&
{
    oaUInt4 i, len;
    len = $1->getNumElements();
    for (i = 0; i < len; ++i) {
	paramtype val = $1->get(i);
	VALUE rbobj = LONG2NUM((long) val);
	rb_ary_push($input, rbobj);
    }
}

%typemap(argout) const ns::class& {}

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

/**/

#undef STRINGARRAY
%define STRINGARRAY

%ignore OpenAccess_4::oaArrayBase<OpenAccess_4::oaString>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaString>;

%typemap(in) OpenAccess_4::oaArray<OpenAccess_4::oaString>& (oaArray<oaString> tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) const OpenAccess_4::oaArray<OpenAccess_4::oaString>& (oaArray<oaString> tmpArr)
{
    if (TYPE($input) != T_ARRAY) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    size_t arrSize = RARRAY($input)->len;
    size_t i;
    VALUE rbobj;
    void *argp;

    for (i = 0; i < arrSize; ++i) {
	VALUE rbobj = rb_ary_entry($input, i);
	if (TYPE(rbobj) == T_STRING) {
	  tmpArr.append(oaString(RSTRING(rbobj)->ptr));
	} else {
	    %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
	}
    }

    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<OpenAccess_4::oaString>&, const OpenAccess_4::oaArray<OpenAccess_4::oaString>&
{
    $1 = (TYPE($input) == T_ARRAY);
}

%typemap(argout) OpenAccess_4::oaArray<OpenAccess_4::oaString>&
{
  oaUInt4 i, len;
  len = $1->getNumElements();
  for (i = 0; i < len; ++i) {
    oaString *oaobj = new oaString($1->get(i));
    VALUE rbobj = rb_str_new((const oaChar*) *oaobj, (const unsigned int)oaobj->getLength());
    rb_ary_push($input, rbobj);
  }
}

%typemap(argout) const OpenAccess_4::oaArray<OpenAccess_4::oaString>& {}

%apply (OpenAccess_4::oaArray<OpenAccess_4::oaString>&) { OpenAccess_4::oaArray<OpenAccess_4::oaString>* };
%apply (const OpenAccess_4::oaArray<OpenAccess_4::oaString>&) { const OpenAccess_4::oaArray<OpenAccess_4::oaString>* };

%enddef

/**/

#undef STRINGARRAYEXT
%define STRINGARRAYEXT(ns,class)

%ignore ns::class;

%typemap(in) (ns::class&) (ns::class tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const ns::class&) (ns::class tmpArr)
{
  if (TYPE($input) != T_ARRAY) {
    %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
  }

  size_t arrSize = RARRAY($input)->len;
  size_t i;
  VALUE rbobj;
  void *argp;

  for (i = 0; i < arrSize; ++i) {
    VALUE rbobj = rb_ary_entry($input, i);
    if (TYPE(rbobj) == T_STRING) {
      tmpArr.append(oaString(RSTRING(rbobj)->ptr));
    } else {
      %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
  }

  $1 = &tmpArr;
}

%typecheck(0) (ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaString>&;
%typecheck(0) (const ns::class&) = const OpenAccess_4::oaArray<OpenAccess_4::oaString>&;
%typemap(argout) (ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaString>&;
%typemap(argout) (const ns::class&) = const OpenAccess_4::oaArray<OpenAccess_4::oaString>&;

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

/**/

#undef POINTARRAY
%define POINTARRAY

%ignore OpenAccess_4::oaArrayBase<OpenAccess_4::oaPoint>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaPoint>;

%typemap(in) (OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&) (oaArray<oaPoint> tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&) (oaArray<oaPoint> tmpArr)
{
    if (TYPE($input) != T_ARRAY) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    size_t arrSize = RARRAY($input)->len;
    size_t i;
    void *argp;
    oaPoint point;
    for (i = 0; i < arrSize; ++i) {
	VALUE rbobj = rb_ary_entry($input, i);    
 	if (!SWIG_IsOK(langobj_to_oaPoint(rbobj, point))) {
	    %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
	}
	tmpArr.append(point);
    }

    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&, const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&
{
    $1 = (TYPE($input) == T_ARRAY);
}

%typemap(argout) (OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&)
{
    oaUInt4 i, len;
    len = $1->getNumElements();
    for (i = 0; i < len; ++i) {
	oaPoint *oaObj = new oaPoint($1->get(i));
	VALUE rbobj = SWIG_NewPointerObj(SWIG_as_voidptr(oaObj), SWIG_TypeDynamicCast($descriptor(OpenAccess_4::oaPoint *), (void**) &oaObj), 0);
	rb_ary_push($input, rbobj);
    }
}

%typemap(argout) (const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&) {}

%apply (OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&) { OpenAccess_4::oaArray<OpenAccess_4::oaPoint>* };
%apply (const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&) { const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>* };

%enddef

/**/

#undef POINTARRAYEXT
%define POINTARRAYEXT(ns,class)
 
%ignore ns::class;

%typemap(in) (ns::class&) (ns::class tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const ns::class&) (ns::class tmpArr)
{
    if (TYPE($input) != T_ARRAY) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    size_t arrSize = RARRAY($input)->len;
    size_t i;
    for (i = 0; i < arrSize; ++i) {
	VALUE rbobj = rb_ary_entry($input, i);
	oaPoint point;
	if (SWIG_IsOK(langobj_to_oaPoint(rbobj, point))) {
	    tmpArr.append(point);
	} else {
	    %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
	}
    }

    $1 = &tmpArr;
}

%typecheck(0) (ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&;
%typecheck(0) (const ns::class&) = const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&;
%typemap(argout) (ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&;
%typemap(argout) (const ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&;

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

 
%enddef

/**/

#undef BOXARRAY
%define BOXARRAY
 
%ignore OpenAccess_4::oaArrayBase<OpenAccess_4::oaBox>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaBox>;

%typemap(in) (OpenAccess_4::oaArray<OpenAccess_4::oaBox>&) (oaArray<oaBox> tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const OpenAccess_4::oaArray<OpenAccess_4::oaBox>&) (oaArray<oaBox> tmpArr)
{
    if (TYPE($input) != T_ARRAY) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    size_t arrSize = RARRAY($input)->len;
    size_t i;
    int res;
    void *argp;
    oaBox box;
    for (i = 0; i < arrSize; ++i) {
	VALUE rbobj = rb_ary_entry($input, i);    
	if (SWIG_IsOK((res = langobj_to_oaBox(rbobj, box)))) {
	    tmpArr.append(box);
	} else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0)))) {
	    tmpArr.append(*(reinterpret_cast<oaBox*>(argp)));
	} else {
	    %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
	}
    }

    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<OpenAccess_4::oaBox>&, const OpenAccess_4::oaArray<OpenAccess_4::oaBox>&
{
    $1 = PyList_Check($input);
}

%typemap(argout) (OpenAccess_4::oaArray<OpenAccess_4::oaBox>&)
{
    oaUInt4 i, len;
    len = $1->getNumElements();
    for (i = 0; i < len; ++i) {
	oaBox *oaobj = new oaBox($1->get(i));
	VALUE rbobj = SWIG_NewPointerObj(SWIG_as_voidptr(oaobj), SWIG_TypeDynamicCast($descriptor(OpenAccess_4::oaBox *), (void**) &oaobj), 0);
	rb_ary_push($input, rbobj);
    }
}

%typemap(argout) (const OpenAccess_4::oaArray<OpenAccess_4::oaBox>&) {}

%apply (OpenAccess_4::oaArray<OpenAccess_4::oaBox>&) { OpenAccess_4::oaArray<OpenAccess_4::oaBox>* };
%apply (const OpenAccess_4::oaArray<OpenAccess_4::oaBox>&) { const OpenAccess_4::oaArray<OpenAccess_4::oaBox>* };
 
%enddef

#undef BOXARRAYEXT
%define BOXARRAYEXT(ns,class)
 
%ignore ns::class;

%typemap(in) (ns::class&) (ns::class tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const ns::class&) (ns::class tmpArr)
{
    if (TYPE($input) != T_ARRAY) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    size_t arrSize = RARRAY($input)->len;
    size_t i;
    for (i = 0; i < arrSize; ++i) {
	VALUE rbobj = rb_ary_entry($input, i);
	oaBox box;
	if (SWIG_IsOK(langobj_to_oaBox(rbobj, box))) {
	    tmpArr.append(box);
	} else {
	    %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
	}
    }

    $1 = &tmpArr;
}

%typecheck(0) (ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaBox>&;
%typecheck(0) (const ns::class&) = const OpenAccess_4::oaArray<OpenAccess_4::oaBox>&;
%typemap(argout) (ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaBox>&;
%typemap(argout) (const ns::class&) = const OpenAccess_4::oaArray<OpenAccess_4::oaBox>&;

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };
 
%enddef

#undef ARRAYCLASS
%define ARRAYCLASS(ns,paramtype)

%ignore OpenAccess_4::oaArray<ns::paramtype>;
%ignore OpenAccess_4::oaArrayBase<ns::paramtype>;

%typemap(in) (OpenAccess_4::oaArray<ns::paramtype>&) (oaArray<ns::paramtype> tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const OpenAccess_4::oaArray<ns::paramtype>&) (oaArray<ns::paramtype> tmpArr)
{
    if (TYPE($input) != T_ARRAY) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    size_t arrSize = RARRAY($input)->len;
    size_t i;
    VALUE rbobj;
    void *argp;

    for (i = 0; i < arrSize; ++i) {
	VALUE rbobj = rb_ary_entry($input, i);
	if (SWIG_IsOK(SWIG_ConvertPtr(rbobj, &argp, $descriptor(ns::paramtype*),  0))) {
	    tmpArr.append(*(reinterpret_cast<ns::paramtype *>(argp)));
	} else {
	    %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
	}
    }

    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<ns::paramtype>&, const OpenAccess_4::oaArray<ns::paramtype>&
{
    $1 = (TYPE($input) == T_ARRAY);
}

%typemap(argout) (OpenAccess_4::oaArray<ns::paramtype>&)
{
    oaUInt4 i, len;
    len = $1->getNumElements();
    for (i = 0; i < len; ++i) {
	ns::paramtype *oaobj = new ns::paramtype($1->get(i));
	VALUE rbobj = SWIG_NewPointerObj(SWIG_as_voidptr(oaobj), SWIG_TypeDynamicCast($descriptor(ns::paramtype*), (void**) &oaobj), 0);
	rb_ary_push($input, rbobj);
    }
}

%typemap(argout) (const OpenAccess_4::oaArray<ns::paramtype>&) {}

%apply (OpenAccess_4::oaArray<ns::paramtype>&) { OpenAccess_4::oaArray<ns::paramtype>* };
%apply (const OpenAccess_4::oaArray<ns::paramtype>&) { const OpenAccess_4::oaArray<ns::paramtype>* };

%enddef

#undef ARRAYEXT
%define ARRAYEXT(ns,class,paramtype)

%ignore ns::class;

%typemap(in) (ns::class&) (ns::class tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const ns::class&) (ns::class tmpArr)
{
    if (TYPE($input) != T_ARRAY) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    size_t arrSize = RARRAY($input)->len;
    size_t i;
    for (i = 0; i < arrSize; ++i) {
	VALUE rbobj = rb_ary_entry($input, i);
	void *argp;
	if (SWIG_IsOK(SWIG_ConvertPtr(rbobj, &argp, $descriptor(paramtype *),  0))) {
	    tmpArr.append(*(reinterpret_cast<paramtype *>(argp)));
	} else {
	    %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
	}
    }

    $1 = &tmpArr;
}

%typecheck(0) ns::class& = OpenAccess_4::oaArray<paramtype>&;
%typemap(argout) (ns::class&) = OpenAccess_4::oaArray<paramtype>&;
%typemap(argout) (const ns::class&) {}

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

#undef PTRARRAYCLASS
%define PTRARRAYCLASS(ns,paramtype)

%ignore OpenAccess_4::oaArray<ns::paramtype *>;
%ignore OpenAccess_4::oaArrayBase<ns::paramtype *>;

%typemap(in) (OpenAccess_4::oaArray<ns::paramtype *>&) (oaArray<ns::paramtype *> tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const OpenAccess_4::oaArray<ns::paramtype *>&) (oaArray<ns::paramtype *> tmpArr)
{
    if (TYPE($input) != T_ARRAY) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    size_t arrSize = RARRAY($input)->len;
    size_t i;
    VALUE rbobj;
    void *argp;

    for (i = 0; i < arrSize; ++i) {
	VALUE rbobj = rb_ary_entry($input, i);
	if (SWIG_IsOK(SWIG_ConvertPtr(rbobj, &argp, $descriptor(ns::paramtype*),  0))) {
	    tmpArr.append(reinterpret_cast<ns::paramtype *>(argp));
	} else {
	    %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
	}
    }

    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<ns::paramtype *>&, const OpenAccess_4::oaArray<ns::paramtype *>&
{
    $1 = (TYPE($input) == T_ARRAY);
}

%typemap(argout) (OpenAccess_4::oaArray<ns::paramtype *>&)
{
    oaUInt4 i, len;
    len = $1->getNumElements();
    for (i = 0; i < len; ++i) {
	ns::paramtype *oaobj = $1->get(i);
	VALUE rbobj = SWIG_NewPointerObj(SWIG_as_voidptr(oaobj), SWIG_TypeDynamicCast($descriptor(ns::paramtype *), (void**) &oaobj), 0);
	rb_ary_push($input, rbobj);
    }
}

%typemap(argout) (const OpenAccess_4::oaArray<ns::paramtype>&) {}

%apply (OpenAccess_4::oaArray<ns::paramtype *>&) { OpenAccess_4::oaArray<ns::paramtype *>* };
%apply (const OpenAccess_4::oaArray<ns::paramtype *>&) { const OpenAccess_4::oaArray<ns::paramtype *>* };

%enddef

#undef PTRARRAYEXT
%define PTRARRAYEXT(ns,class,basetype)

%ignore ns::class;

%typemap(in) (ns::class&) (ns::class tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const ns::class&) (ns::class tmpArr)
{
    if (TYPE($input) != T_ARRAY) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    size_t arrSize = RARRAY($input)->len;
    size_t i;
    for (i = 0; i < arrSize; ++i) {
	VALUE rbobj = rb_ary_entry($input, i);
	void *argp;
	if (SWIG_IsOK(SWIG_ConvertPtr(rbobj, &argp, $descriptor(basetype *),  0))) {
	    tmpArr.append(reinterpret_cast<basetype *>(argp));
	} else {
	    %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
	}
    }

    $1 = &tmpArr;
}

%typecheck(0) (ns::class&) = OpenAccess_4::oaArray<basetype *>&;
%typemap(argout) (ns::class&) = OpenAccess_4::oaArray<basetype *>&;
%typemap(argout) (const ns::class&) {}

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

#undef INTSUBSET
%define INTSUBSET(safetype,paramtype)
%enddef

#undef INTSUBSETEXT
%define INTSUBSETEXT(ns,class,safetype,paramtype)
%enddef

#undef STRINGSUBSET
%define STRINGSUBSET
%enddef

#undef STRINGSUBSETEXT
%define STRINGSUBSETEXT(ns,class)
%enddef

#undef POINTSUBSET
%define POINTSUBSET
%enddef

#undef POINTSUBSETEXT
%define POINTSUBSETEXT(ns,class)
%enddef

#undef BOXSUBSET
%define BOXSUBSET
%enddef

#undef BOXSUBSETEXT
%define BOXSUBSETEXT(ns,class)
%enddef

#undef SUBSETCLASS
%define SUBSETCLASS(ns,paramtype)
%enddef

#undef SUBSETEXT
%define SUBSETEXT(ns,class,paramtype)
%enddef

#undef PTRSUBSETCLASS
%define PTRSUBSETCLASS(ns,paramtype)
%enddef

#undef PTRSUBSETEXT
%define PTRSUBSETEXT(ns,class,basetype)
%enddef

%include <macros/oa_arrays.i>
