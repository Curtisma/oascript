#! /usr/bin/env ruby
###############################################################################
#
# Copyright 2011 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################

require 'oa'
include Oa

path = ARGV[0]
abort "oa_libdef_check.rb <lib.defs>" unless path

# Ensure that the lib.defs list is readable.
begin
  oaDesignInit
  OaLibDefList.openLibs(path)
rescue OaException => excp
  if excp.getMsgId == OacLibDefListFileNotReadable
    abort "ERROR: Provided lib.def is not readable: #{path}"
  else
    raise
  end
end

# Test that the top lib.defs list can be found and is good
unless list = OaLibDefList.getTopList
  abort "ERROR: Bad lib.def (missing file or bad contents)"
end

# Walk through the lib.defs list and show ok/BAD for each definition
list.each_def_pair do |name, path|
  status = (File.exists?(path)) ? 'ok' : 'BAD'
  printf("%-6s %-20s %s\n", status, name, path)
end
