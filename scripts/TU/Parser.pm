package TU::Parser;

use strict;
use warnings;
use Carp;
use TU;
use TU::Node;

our %TOKEN_MAP = (
    'error_mark'			=> TU::ERROR_MARK,
    'identifier_node'			=> TU::IDENTIFIER_NODE,
    'tree_list'				=> TU::TREE_LIST,
    'tree_vec'				=> TU::TREE_VEC,
    'block'				=> TU::BLOCK,
    'offset_type'			=> TU::OFFSET_TYPE,
    'enumeral_type'			=> TU::ENUMERAL_TYPE,
    'boolean_type'			=> TU::BOOLEAN_TYPE,
    'integer_type'			=> TU::INTEGER_TYPE,
    'real_type'				=> TU::REAL_TYPE,
    'pointer_type'			=> TU::POINTER_TYPE,
    'fixed_point_type'			=> TU::FIXED_POINT_TYPE,
    'reference_type'			=> TU::REFERENCE_TYPE,
    'complex_type'			=> TU::COMPLEX_TYPE,
    'vector_type'			=> TU::VECTOR_TYPE,
    'array_type'			=> TU::ARRAY_TYPE,
    'record_type'			=> TU::RECORD_TYPE,
    'union_type'			=> TU::UNION_TYPE,
    'qual_union_type'			=> TU::QUAL_UNION_TYPE,
    'void_type'				=> TU::VOID_TYPE,
    'function_type'			=> TU::FUNCTION_TYPE,
    'method_type'			=> TU::METHOD_TYPE,
    'lang_type'				=> TU::LANG_TYPE,
    'integer_cst'			=> TU::INTEGER_CST,
    'real_cst'				=> TU::REAL_CST,
    'fixed_cst'				=> TU::FIXED_CST,
    'complex_cst'			=> TU::COMPLEX_CST,
    'vector_cst'			=> TU::VECTOR_CST,
    'string_cst'			=> TU::STRING_CST,
    'function_decl'			=> TU::FUNCTION_DECL,
    'label_decl'			=> TU::LABEL_DECL,
    'field_decl'			=> TU::FIELD_DECL,
    'var_decl'				=> TU::VAR_DECL,
    'const_decl'			=> TU::CONST_DECL,
    'parm_decl'				=> TU::PARM_DECL,
    'type_decl'				=> TU::TYPE_DECL,
    'result_decl'			=> TU::RESULT_DECL,
    'struct_field_tag'			=> TU::STRUCT_FIELD_TAG,
    'name_memory_tag'			=> TU::NAME_MEMORY_TAG,
    'symbol_memory_tag'			=> TU::SYMBOL_MEMORY_TAG,
    'memory_partition_tag'		=> TU::MEMORY_PARTITION_TAG,
    'namespace_decl'			=> TU::NAMESPACE_DECL,
    'translation_unit_decl'		=> TU::TRANSLATION_UNIT_DECL,
    'component_ref'			=> TU::COMPONENT_REF,
    'bit_field_ref'			=> TU::BIT_FIELD_REF,
    'indirect_ref'			=> TU::INDIRECT_REF,
    'align_indirect_ref'		=> TU::ALIGN_INDIRECT_REF,
    'misaligned_indirect_ref'		=> TU::MISALIGNED_INDIRECT_REF,
    'array_ref'				=> TU::ARRAY_REF,
    'array_range_ref'			=> TU::ARRAY_RANGE_REF,
    'obj_type_ref'			=> TU::OBJ_TYPE_REF,
    'exc_ptr_expr'			=> TU::EXC_PTR_EXPR,
    'filter_expr'			=> TU::FILTER_EXPR,
    'constructor'			=> TU::CONSTRUCTOR,
    'compound_expr'			=> TU::COMPOUND_EXPR,
    'modify_expr'			=> TU::MODIFY_EXPR,
    'init_expr'				=> TU::INIT_EXPR,
    'target_expr'			=> TU::TARGET_EXPR,
    'cond_expr'				=> TU::COND_EXPR,
    'vec_cond_expr'			=> TU::VEC_COND_EXPR,
    'bind_expr'				=> TU::BIND_EXPR,
    'call_expr'				=> TU::CALL_EXPR,
    'with_cleanup_expr'			=> TU::WITH_CLEANUP_EXPR,
    'cleanup_point_expr'		=> TU::CLEANUP_POINT_EXPR,
    'placeholder_expr'			=> TU::PLACEHOLDER_EXPR,
    'plus_expr'				=> TU::PLUS_EXPR,
    'minus_expr'			=> TU::MINUS_EXPR,
    'mult_expr'				=> TU::MULT_EXPR,
    'pointer_plus_expr'			=> TU::POINTER_PLUS_EXPR,
    'trunc_div_expr'			=> TU::TRUNC_DIV_EXPR,
    'ceil_div_expr'			=> TU::CEIL_DIV_EXPR,
    'floor_div_expr'			=> TU::FLOOR_DIV_EXPR,
    'round_div_expr'			=> TU::ROUND_DIV_EXPR,
    'trunc_mod_expr'			=> TU::TRUNC_MOD_EXPR,
    'ceil_mod_expr'			=> TU::CEIL_MOD_EXPR,
    'floor_mod_expr'			=> TU::FLOOR_MOD_EXPR,
    'round_mod_expr'			=> TU::ROUND_MOD_EXPR,
    'rdiv_expr'				=> TU::RDIV_EXPR,
    'exact_div_expr'			=> TU::EXACT_DIV_EXPR,
    'fix_trunc_expr'			=> TU::FIX_TRUNC_EXPR,
    'float_expr'			=> TU::FLOAT_EXPR,
    'negate_expr'			=> TU::NEGATE_EXPR,
    'min_expr'				=> TU::MIN_EXPR,
    'max_expr'				=> TU::MAX_EXPR,
    'abs_expr'				=> TU::ABS_EXPR,
    'lshift_expr'			=> TU::LSHIFT_EXPR,
    'rshift_expr'			=> TU::RSHIFT_EXPR,
    'lrotate_expr'			=> TU::LROTATE_EXPR,
    'rrotate_expr'			=> TU::RROTATE_EXPR,
    'bit_ior_expr'			=> TU::BIT_IOR_EXPR,
    'bit_xor_expr'			=> TU::BIT_XOR_EXPR,
    'bit_and_expr'			=> TU::BIT_AND_EXPR,
    'bit_not_expr'			=> TU::BIT_NOT_EXPR,
    'truth_andif_expr'			=> TU::TRUTH_ANDIF_EXPR,
    'truth_orif_expr'			=> TU::TRUTH_ORIF_EXPR,
    'truth_and_expr'			=> TU::TRUTH_AND_EXPR,
    'truth_or_expr'			=> TU::TRUTH_OR_EXPR,
    'truth_xor_expr'			=> TU::TRUTH_XOR_EXPR,
    'truth_not_expr'			=> TU::TRUTH_NOT_EXPR,
    'lt_expr'				=> TU::LT_EXPR,
    'le_expr'				=> TU::LE_EXPR,
    'gt_expr'				=> TU::GT_EXPR,
    'ge_expr'				=> TU::GE_EXPR,
    'eq_expr'				=> TU::EQ_EXPR,
    'ne_expr'				=> TU::NE_EXPR,
    'unordered_expr'			=> TU::UNORDERED_EXPR,
    'ordered_expr'			=> TU::ORDERED_EXPR,
    'unlt_expr'				=> TU::UNLT_EXPR,
    'unle_expr'				=> TU::UNLE_EXPR,
    'ungt_expr'				=> TU::UNGT_EXPR,
    'unge_expr'				=> TU::UNGE_EXPR,
    'uneq_expr'				=> TU::UNEQ_EXPR,
    'ltgt_expr'				=> TU::LTGT_EXPR,
    'range_expr'			=> TU::RANGE_EXPR,
    'convert_expr'			=> TU::CONVERT_EXPR,
    'fixed_convert_expr'		=> TU::FIXED_CONVERT_EXPR,
    'nop_expr'				=> TU::NOP_EXPR,
    'non_lvalue_expr'			=> TU::NON_LVALUE_EXPR,
    'view_convert_expr'			=> TU::VIEW_CONVERT_EXPR,
    'save_expr'				=> TU::SAVE_EXPR,
    'addr_expr'				=> TU::ADDR_EXPR,
    'fdesc_expr'			=> TU::FDESC_EXPR,
    'complex_expr'			=> TU::COMPLEX_EXPR,
    'conj_expr'				=> TU::CONJ_EXPR,
    'realpart_expr'			=> TU::REALPART_EXPR,
    'imagpart_expr'			=> TU::IMAGPART_EXPR,
    'predecrement_expr'			=> TU::PREDECREMENT_EXPR,
    'preincrement_expr'			=> TU::PREINCREMENT_EXPR,
    'postdecrement_expr'		=> TU::POSTDECREMENT_EXPR,
    'postincrement_expr'		=> TU::POSTINCREMENT_EXPR,
    'va_arg_expr'			=> TU::VA_ARG_EXPR,
    'try_catch_expr'			=> TU::TRY_CATCH_EXPR,
    'try_finally'			=> TU::TRY_FINALLY_EXPR,
    'decl_expr'				=> TU::DECL_EXPR,
    'label_expr'			=> TU::LABEL_EXPR,
    'goto_expr'				=> TU::GOTO_EXPR,
    'return_expr'			=> TU::RETURN_EXPR,
    'exit_expr'				=> TU::EXIT_EXPR,
    'loop_expr'				=> TU::LOOP_EXPR,
    'switch_expr'			=> TU::SWITCH_EXPR,
    'case_label_expr'			=> TU::CASE_LABEL_EXPR,
    'resx_expr'				=> TU::RESX_EXPR,
    'asm_expr'				=> TU::ASM_EXPR,
    'ssa_name'				=> TU::SSA_NAME,
    'phi_node'				=> TU::PHI_NODE,
    'catch_expr'			=> TU::CATCH_EXPR,
    'eh_filter_expr'			=> TU::EH_FILTER_EXPR,
    'change_dynamic_type_expr'		=> TU::CHANGE_DYNAMIC_TYPE_EXPR,
    'scev_known'			=> TU::SCEV_KNOWN,
    'scev_not_known'			=> TU::SCEV_NOT_KNOWN,
    'polynomial_chrec'			=> TU::POLYNOMIAL_CHREC,
    'statement_list'			=> TU::STATEMENT_LIST,
    'value_handle'			=> TU::VALUE_HANDLE,
    'assert_expr'			=> TU::ASSERT_EXPR,
    'binfo'				=> TU::BINFO,
    'with_size_expr'			=> TU::WITH_SIZE_EXPR,
    'realign_load'			=> TU::REALIGN_LOAD_EXPR,
    'target_mem_ref'			=> TU::TARGET_MEM_REF,
    'omp_parallel'			=> TU::OMP_PARALLEL,
    'omp_for'				=> TU::OMP_FOR,
    'omp_sections'			=> TU::OMP_SECTION,
    'omp_sections_switch'		=> TU::OMP_SECTIONS_SWITCH,
    'omp_single'			=> TU::OMP_SINGLE,
    'omp_section'			=> TU::OMP_SECTION,
    'omp_master'			=> TU::OMP_MASTER,
    'omp_ordered'			=> TU::OMP_ORDERED,
    'omp_critical'			=> TU::OMP_CRITICAL,
    'omp_return'			=> TU::OMP_RETURN,
    'omp_continue'			=> TU::OMP_CONTINUE,
    'omp_atomic'			=> TU::OMP_ATOMIC,
    'omp_atomic_load'			=> TU::OMP_ATOMIC_LOAD,
    'omp_atomic_store'			=> TU::OMP_ATOMIC_STORE,
    'omp_clause'			=> TU::OMP_CLAUSE,
    'reduc_max_expr'			=> TU::REDUC_MAX_EXPR,
    'reduc_min_expr'			=> TU::REDUC_MIN_EXPR,
    'reduc_plus_expr'			=> TU::REDUC_PLUS_EXPR,
    'dot_prod_expr'			=> TU::DOT_PROD_EXPR,
    'widen_sum_expr'			=> TU::WIDEN_SUM_EXPR,
    'widen_mult_expr'			=> TU::WIDEN_MULT_EXPR,
    'vec_lshift_expr'			=> TU::VEC_LSHIFT_EXPR,
    'vec_rshift_expr'			=> TU::VEC_RSHIFT_EXPR,
    'gimple_modify_stmt'		=> TU::GIMPLE_MODIFY_STMT,
    'widen_mult_hi_expr'		=> TU::VEC_WIDEN_MULT_HI_EXPR,
    'widen_mult_lo_expr'		=> TU::VEC_WIDEN_MULT_LO_EXPR,
    'vec_unpack_hi_expr'		=> TU::VEC_UNPACK_HI_EXPR,
    'vec_unpack_lo_expr'		=> TU::VEC_UNPACK_LO_EXPR,
    'vec_unpack_float_hi_expr'		=> TU::VEC_UNPACK_FLOAT_HI_EXPR,
    'vec_unpack_float_lo_expr'		=> TU::VEC_UNPACK_FLOAT_LO_EXPR,
    'vec_pack_trunc_expr'		=> TU::VEC_PACK_TRUNC_EXPR,
    'vec_pack_sat_expr'			=> TU::VEC_PACK_SAT_EXPR,
    'vec_pack_fix_trunc_expr'		=> TU::VEC_PACK_FIX_TRUNC_EXPR,
    'vec_extracteven_expr'		=> TU::VEC_EXTRACT_EVEN_EXPR,
    'vec_extractodd_expr'		=> TU::VEC_EXTRACT_ODD_EXPR,
    'vec_interleavehigh_expr'		=> TU::VEC_INTERLEAVE_HIGH_EXPR,
    'vec_interleavelow_expr'		=> TU::VEC_INTERLEAVE_LOW_EXPR,
    'offset_ref'			=> TU::OFFSET_REF,
    'ptrmem_cst'			=> TU::PTRMEM_CST,
    'nw_expr'				=> TU::NEW_EXPR,
    'vec_nw_expr'			=> TU::VEC_NEW_EXPR,
    'dl_expr'				=> TU::DELETE_EXPR,
    'vec_dl_expr'			=> TU::VEC_DELETE_EXPR,
    'scope_ref'				=> TU::SCOPE_REF,
    'member_ref'			=> TU::MEMBER_REF,
    'type_expr'				=> TU::TYPE_EXPR,
    'aggr_init_expr'			=> TU::AGGR_INIT_EXPR,
    'throw_expr'			=> TU::THROW_EXPR,
    'empty_class_expr'			=> TU::EMPTY_CLASS_EXPR,
    'baselink'				=> TU::BASELINK,
    'template_decl'			=> TU::TEMPLATE_DECL,
    'template_parm_index'		=> TU::TEMPLATE_PARM_INDEX,
    'template_template_parm'		=> TU::TEMPLATE_TEMPLATE_PARM,
    'template_type_parm'		=> TU::TEMPLATE_TYPE_PARM,
    'typename_type'			=> TU::TYPENAME_TYPE,
    'typeof_type'			=> TU::TYPEOF_TYPE,
    'bound_template_template_parm'	=> TU::BOUND_TEMPLATE_TEMPLATE_PARM,
    'unbound_class_template'		=> TU::UNBOUND_CLASS_TEMPLATE,
    'using_decl'			=> TU::USING_DECL,
    'using_directive'			=> TU::USING_STMT,
    'default_arg'			=> TU::DEFAULT_ARG,
    'template_id_expr'			=> TU::TEMPLATE_ID_EXPR,
    'overload'				=> TU::OVERLOAD,
    'pseudo_dtor_expr'			=> TU::PSEUDO_DTOR_EXPR,
    'modop_expr'			=> TU::MODOP_EXPR,
    'cast_expr'				=> TU::CAST_EXPR,
    'reinterpret_cast_expr'		=> TU::REINTERPRET_CAST_EXPR,
    'const_cast_expr'			=> TU::CONST_CAST_EXPR,
    'static_cast_expr'			=> TU::STATIC_CAST_EXPR,
    'dynamic_cast_expr'			=> TU::DYNAMIC_CAST_EXPR,
    'dotstar_expr'			=> TU::DOTSTAR_EXPR,
    'typeid_expr'			=> TU::TYPEID_EXPR,
    'non_dependent_expr'		=> TU::NON_DEPENDENT_EXPR,
    'ctor_initializer'			=> TU::CTOR_INITIALIZER,
    'try_block'				=> TU::TRY_BLOCK,
    'eh_spec_block'			=> TU::EH_SPEC_BLOCK,
    'handler'				=> TU::HANDLER,
    'must_not_throw_expr'		=> TU::MUST_NOT_THROW_EXPR,
    'cleanup_stmt'			=> TU::CLEANUP_STMT,
    'if_stmt'				=> TU::IF_STMT,
    'for_stmt'				=> TU::FOR_STMT,
    'while_stmt'			=> TU::WHILE_STMT,
    'do_stmt'				=> TU::DO_STMT,
    'break_stmt'			=> TU::BREAK_STMT,
    'continue_stmt'			=> TU::CONTINUE_STMT,
    'switch_stmt'			=> TU::SWITCH_STMT,
    'expr_stmt'				=> TU::EXPR_STMT,
    'tag_defn'				=> TU::TAG_DEFN,
    'offsetof_expr'			=> TU::OFFSETOF_EXPR,
    'sizeof_expr'			=> TU::SIZEOF_EXPR,
    'arrow_expr'			=> TU::ARROW_EXPR,
    'alignof_expr'			=> TU::ALIGNOF_EXPR,
    'stmt_expr'				=> TU::STMT_EXPR,
    'unary_plus_expr'			=> TU::UNARY_PLUS_EXPR,
    'static_assert'			=> TU::STATIC_ASSERT,
    'type_argument_pack'		=> TU::TYPE_ARGUMENT_PACK,
    'nontype_argument_pack'		=> TU::NONTYPE_ARGUMENT_PACK,
    'type_pack_expansion'		=> TU::TYPE_PACK_EXPANSION,
    'expr_pack_expansion'		=> TU::EXPR_PACK_EXPANSION,
    'argument_pack_select'		=> TU::ARGUMENT_PACK_SELECT,
    'trait_expr'			=> TU::TRAIT_EXPR,
    'decltype_type'			=> TU::DECLTYPE_TYPE
);

our @TOKEN_REVERSE_MAP = (
    '',
    'error_mark',
    'identifier_node',
    'tree_list',
    'tree_vec',
    'block',
    'offset_type',
    'enumeral_type',
    'boolean_type',
    'integer_type',
    'real_type',
    'pointer_type',
    'fixed_point_type',
    'reference_type',
    'complex_type',
    'vector_type',
    'array_type',
    'record_type',
    'union_type',
    'qual_union_type',
    'void_type',
    'function_type',
    'method_type',
    'lang_type',
    'integer_cst',
    'real_cst',
    'fixed_cst',
    'complex_cst',
    'vector_cst',
    'string_cst',
    'function_decl',
    'label_decl',
    'field_decl',
    'var_decl',
    'const_decl',
    'parm_decl',
    'type_decl',
    'result_decl',
    'struct_field_tag',
    'name_memory_tag',
    'symbol_memory_tag',
    'memory_partition_tag',
    'namespace_decl',
    'translation_unit_decl',
    'component_ref',
    'bit_field_ref',
    'indirect_ref',
    'align_indirect_ref',
    'misaligned_indirect_ref',
    'array_ref',
    'array_range_ref',
    'obj_type_ref',
    'exc_ptr_expr',
    'filter_expr',
    'constructor',
    'compound_expr',
    'modify_expr',
    'init_expr',
    'target_expr',
    'cond_expr',
    'vec_cond_expr',
    'bind_expr',
    'call_expr',
    'with_cleanup_expr',
    'cleanup_point_expr',
    'placeholder_expr',
    'plus_expr',
    'minus_expr',
    'mult_expr',
    'pointer_plus_expr',
    'trunc_div_expr',
    'ceil_div_expr',
    'floor_div_expr',
    'round_div_expr',
    'trunc_mod_expr',
    'ceil_mod_expr',
    'floor_mod_expr',
    'round_mod_expr',
    'rdiv_expr',
    'exact_div_expr',
    'fix_trunc_expr',
    'float_expr',
    'negate_expr',
    'min_expr',
    'max_expr',
    'abs_expr',
    'lshift_expr',
    'rshift_expr',
    'lrotate_expr',
    'rrotate_expr',
    'bit_ior_expr',
    'bit_xor_expr',
    'bit_and_expr',
    'bit_not_expr',
    'truth_andif_expr',
    'truth_orif_expr',
    'truth_and_expr',
    'truth_or_expr',
    'truth_xor_expr',
    'truth_not_expr',
    'lt_expr',
    'le_expr',
    'gt_expr',
    'ge_expr',
    'eq_expr',
    'ne_expr',
    'unordered_expr',
    'ordered_expr',
    'unlt_expr',
    'unle_expr',
    'ungt_expr',
    'unge_expr',
    'uneq_expr',
    'ltgt_expr',
    'range_expr',
    'convert_expr',
    'fixed_convert_expr',
    'nop_expr',
    'non_lvalue_expr',
    'view_convert_expr',
    'save_expr',
    'addr_expr',
    'fdesc_expr',
    'complex_expr',
    'conj_expr',
    'realpart_expr',
    'imagpart_expr',
    'predecrement_expr',
    'preincrement_expr',
    'postdecrement_expr',
    'postincrement_expr',
    'va_arg_expr',
    'try_catch_expr',
    'try_finally',
    'decl_expr',
    'label_expr',
    'goto_expr',
    'return_expr',
    'exit_expr',
    'loop_expr',
    'switch_expr',
    'case_label_expr',
    'resx_expr',
    'asm_expr',
    'ssa_name',
    'phi_node',
    'catch_expr',
    'eh_filter_expr',
    'change_dynamic_type_expr',
    'scev_known',
    'scev_not_known',
    'polynomial_chrec',
    'statement_list',
    'value_handle',
    'assert_expr',
    'tree_binfo',
    'with_size_expr',
    'realign_load',
    'target_mem_ref',
    'omp_parallel',
    'omp_for',
    'omp_sections',
    'omp_sections_switch',
    'omp_single',
    'omp_section',
    'omp_master',
    'omp_ordered',
    'omp_critical',
    'omp_return',
    'omp_continue',
    'omp_atomic',
    'omp_atomic_load',
    'omp_atomic_store',
    'omp_clause',
    'reduc_max_expr',
    'reduc_min_expr',
    'reduc_plus_expr',
    'dot_prod_expr',
    'widen_sum_expr',
    'widen_mult_expr',
    'vec_lshift_expr',
    'vec_rshift_expr',
    'gimple_modify_stmt',
    'widen_mult_hi_expr',
    'widen_mult_lo_expr',
    'vec_unpack_hi_expr',
    'vec_unpack_lo_expr',
    'vec_unpack_float_hi_expr',
    'vec_unpack_float_lo_expr',
    'vec_pack_trunc_expr',
    'vec_pack_sat_expr',
    'vec_pack_fix_trunc_expr',
    'vec_extracteven_expr',
    'vec_extractodd_expr',
    'vec_interleavehigh_expr',
    'vec_interleavelow_expr',
    'offset_ref',
    'ptrmem_cst',
    'nw_expr',
    'vec_nw_expr',
    'dl_expr',
    'vec_dl_expr',
    'scope_ref',
    'member_ref',
    'type_expr',
    'aggr_init_expr',
    'throw_expr',
    'empty_class_expr',
    'baselink',
    'template_decl',
    'template_parm_index',
    'template_template_parm',
    'template_type_parm',
    'typename_type',
    'typeof_type',
    'bound_template_template_parm',
    'unbound_class_template',
    'using_decl',
    'using_directive',
    'default_arg',
    'template_id_expr',
    'overload',
    'pseudo_dtor_expr',
    'modop_expr',
    'cast_expr',
    'reinterpret_cast_expr',
    'const_cast_expr',
    'static_cast_expr',
    'dynamic_cast_expr',
    'dotstar_expr',
    'typeid_expr',
    'non_dependent_expr',
    'ctor_initializer',
    'try_block',
    'eh_spec_block',
    'handler',
    'must_not_throw_expr',
    'cleanup_stmt',
    'if_stmt',
    'for_stmt',
    'while_stmt',
    'do_stmt',
    'break_stmt',
    'continue_stmt',
    'switch_stmt',
    'expr_stmt',
    'tag_defn',
    'offsetof_expr',
    'sizeof_expr',
    'arrow_expr',
    'alignof_expr',
    'stmt_expr',
    'unary_plus_expr',
    'static_assert',
    'type_argument_pack',
    'nontype_argument_pack',
    'type_pack_expansion',
    'expr_pack_expansion',
    'argument_pack_select',
    'trait_expr',
    'decltype_type'
    );

sub new {
    my $pkg = shift;
    my $file = shift;

    my $self = [];
    my $result = bless $self, $pkg;

    my $line = '';
    my $node;

    my $fh;
    open($fh, $file) or croak "Couldn't open $file for read";
    while(<$fh>) {
	chomp;
	my $new_node;
	my $new_line;
	if (s/^@([0-9]+)\s+//) {
	    $new_node = $1;
	    $new_line = $_;
	} else {
	    $line .= $_;
	    next;
	}

	my $node_obj = $self->_parseNode($line, $node);
	if ($node_obj) {
	    $self->[0]->[$node] = $node_obj;
	    push(@{$self->[1]->[$node_obj->getType()]}, $node_obj);
	}

	$node = $new_node;
	$line = $new_line;
    }

    # Do the last line
    my $node_obj = $self->_parseNode($line, $node);
    if ($node_obj) {
	$self->[0]->[$node] = $node_obj;
	push(@{$self->[1]->[$node_obj->getType()]}, $node_obj);
    }

    close($fh);

    return $result;
}

sub getNode {
    my ($self, $node) = @_;
    return unless defined($node);
    $node = int($node);
    return unless ($node && defined($self->[0]->[$node]));
    return $self->[0]->[$node];
}

sub getNodesByType {
    my ($self, $type) = @_;
    return () unless (defined($self->[1]->[$type]));
    return @{$self->[1]->[$type]};
}

#-------------------------------------------------------------------------------

sub _parseNode {
    my ($self, $line, $node) = @_;

    $line =~ s/\s+$//;
    return unless ($line and $line =~ s/^\s*(\S+)\s*//);
    my $node_tag = $1;
    my $node_type = $TOKEN_MAP{$1} || croak "Don't know about token '$node_tag'";
    my $fields = $self->_parseFields($node_type, $line);
    return new TU::Node($node, $node_type, $fields, $self);
}

sub _parseFields {
    my ($self, $node_type, $line) = @_;

    my %fields;

    if ($node_type == TU::IDENTIFIER_NODE) {
	if ($line =~ /^strg: (.*\S)\s+lngt: ([0-9]+)/) {
	    %fields = ( 'strg' => $1, 'lngt' => $2 );
	} elsif ($line =~ /^note: (.*\S)\s*$/) {
	    %fields = ( 'note' => [ $1 ] );
	} else {
	    print STDERR "$. : Couldn't parse identifier_node fields\n";
	}
	return \%fields;
    } elsif ($node_type == TU::STRING_CST) {
	if ($line =~ /type: (\S+)\s+strg: (.*)\s+lngt: ([0-9]+)/) {
	    %fields = ( 'type' => $1, 'strg' => $2, 'lngt' => $3 );
	} else {
	    print STDERR "$. : Couldn't parse string_cst fields\n";
	}
	return \%fields;
    }

    my $key;
    my $lastbase;
    my $lastbinf;

    if ($line =~ /^(.{4}):\s/) {
	$key = $1;
	$key =~ s/\s+$//;
	$line = substr($line, 6);
    }
    
    while (defined($key)) {

	my $nextkey;
	my $val;

	if ($line =~ /((.{4})|(bases)):\s/) {
	    $nextkey = $1;
	    my $keystart = index($line, $nextkey);
	    $val = substr($line, 0, $keystart - 1);
	    $line = substr($line, $keystart + length($nextkey) + 2);
	    $nextkey =~ s/\s+$//;
	} else {
	    $nextkey = undef;
	    $val = $line;
	}

	$val =~ s/^@//;
	$val =~ s/^\s+//;
	$val =~ s/\s+$//;

	if ($val =~ s/\s+spec$//) {
	    $fields{spec} = 1;
	}
	if ($key eq 'note') {
	    push(@{$fields{'note'}}, $val);
	} elsif ($key eq 'base') {
	    $lastbase = [$val, {}];
	    push(@{$fields{'base'}}, $lastbase);
	} elsif ($lastbase && $key eq 'spec') {
	    $lastbase->[1]->{spec} = $val;
	} elsif ($lastbase && $key eq 'accs') {
	    $lastbase->[1]->{accs} = $val;
	    $lastbase = undef;
	} elsif ($node_type == TU::BINFO && $key eq 'accs') {
	    $lastbinf = [$val];
	    push(@{$fields{'binf'}}, $lastbinf);
	} elsif ($lastbinf && $key eq 'binf') {
	    push(@$lastbinf, $val);
	    $lastbinf = undef;
	} else {
	    $fields{$key} = $val;
	}

        # Restore 'accs' value
        if ($key eq 'accs') {
            $fields{$key} = $val;
        }

	$key = $nextkey;
    }

    return \%fields;
}

1;
