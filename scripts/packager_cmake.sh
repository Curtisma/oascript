#!/bin/bash
#
# Copyright 2010 Advanced Micro Devices
# Copyright 2010 Synopsys, Inc.
# Copyright 2011 Voom, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Revision History
# 2010-11-18 r401 Original for GNU Make based releases by Stefan Zager
# 2011-05-06 Forked for CMake based releases, John McGehee

# Create releases for the CMake-based build.

if [ -z $1 ]; then
    echo "Usage: packager_cmake.sh <version>"
	echo "    An example <version> is 'v0.7'.  Use version 'latest' for the SVN head."
	echo "    To see all versions:"
	echo "      svn ls svn://oacscripting-svn.si2.org/srv/svn/repos/amd-init-tags"
    exit 1;
fi

version=$1
platform=rhel40
reldate=`date +%Y.%m.%d`
topdir=`pwd`
if [ $version == "latest" ]; then
	relname=oaScript-${version}
    repos=svn://oacscripting-svn.si2.org/srv/svn/repos/amd-init
else
	# "${version#[^0-9\.]}" means to remove prefix characters except 0-9 and .
	relname=oaScript-${version#[^0-9\.]}
    repos=svn://oacscripting-svn.si2.org/srv/svn/repos/amd-init-tags/${version}
fi

echo "Downloading source code for ${relname}..."
if ! svn co ${repos} ${relname}; then
    exit 1;
fi

echo "Creating ${relname}/Changelog"
if ! svn log ${relname} > ${relname}/Changelog; then
    exit 1;
fi

# tar option to exclude GNU Makefiles and languages not currently supported by the CMake build
alwaysExclude="--exclude-vcs --exclude=Makefile --exclude=make.variables --exclude=perl5 --exclude=python2.5 --exclude=README --exclude=ruby1.8 --exclude=tcl8.4 --exclude=testtarget.mk"

echo "Creating ${relname}-cmake-src.tar.gz..."
if ! tar cfz ${relname}-cmake-src.tar.gz $alwaysExclude \
		${relname} \
		; then
    exit 1;
fi

# Build for both the wrappers and bin packages
mkdir -p ${topdir}/${relname}/Release
cd       ${topdir}/${relname}/Release
cmake -DCMAKE_INSTALL_PREFIX:PATH=${topdir}/binPackage/${relname} ..

echo "Building binaries..."
if ! make; then
    exit 1;
fi

cd ${topdir}
echo "Creating ${relname}-cmake-wrappers.tar.gz"
if ! tar cfz ${relname}-cmake-wrappers.tar.gz $alwaysExclude \
	${relname}/README_CMake.txt \
	${relname}/Changelog \
	${relname}/Apache_LICENSE-2.0.txt \
	${relname}/copyright.txt \
	${relname}/csharp3.0/README.txt \
	${relname}/Release/*_wrap.cxx \
	${relname}/Release/csharp3.0/*.cs \
	${relname}/Release/munged_headers/*.h \
	${relname}/Release/macros/*.i \
	${relname}/Release/macros/tu.cc.tu \
	; then
    exit 1;
fi


echo "Creating ${relname}-cmake-tu.tar.gz"
if ! tar cfz ${relname}-cmake-tu.tar.gz $alwaysExclude \
		${relname}/Release/macros/tu.cc.tu \
        ; then
    exit 1;
fi

make -C ${topdir}/${relname}/Release install

echo "Creating ${relname}-cmake-${platform}-bin.tar.gz"
cd ${topdir}/binPackage
if ! tar cfz ../${relname}-cmake-${platform}-bin.tar.gz $alwaysExclude \
        ${relname} \
        ; then
    exit 1;
fi

echo "Done!"

