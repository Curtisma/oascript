#!/usr/bin/env tcl
################################################################################
#  Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#  
#  Licensed under the Apache License, Version 2.0 (the "License"); you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http://www.apache.org/licenses/LICENSE-2.0
#  
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   08/03/10  10.0     scarver, Si2     Tutorial 10th Edition - Tcl version
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################

# This program builds a library containing a full adder with its component
# half adder and leaf cells, all connected as shown below:
#
#     Leaf cells              HalfAdder
#
#         _____               ____________________
#     A--|     |             |                    |
#        | And |--Y          |        A ______    |
#     B--|_____|          A--|-----o---|      |Y  |
#                            |     |   | And1 |---|--C
#         ____               |  o------|______|   |
#     A--|    |              |  |  |  B           |
#        | Or |--Y           |  |  |              |
#     B--|____|              |  |  |  A _____     |
#                            |  |  o---|      |Y  |
#         _____              |  |      | Xor1 |---|--S
#     A--|     |          B--|--o------|______|   |
#        | Xor |--Y          |        B           |
#     B--|_____|             |____________________|
#
#
#         FullAdder block:
#         ______________________________________Co_______
#        |                            _____    |         |
#        |       A_____C   CA_Net   A|     |Y  |         |
#     A--|-------|     |-------------| Or1 |---          |
#        | A_Net |     |           --|_____| Co_Net      |
#        |       |     |          |___________           |
#        |       | Ha1 |                      |          |
#        | B_Net |     |            A_____C   |          |
#     B--|-------|_____|------------|     |---CB_Net     |
#        |       B     S   SA_Net   |     |              |
#        |                          |     |              |
#        |                          | Ha2 |              |
#        |                          |     |    S_net     |
#        |                     -----|_____|--------------|--S
#        |                    |    B       S             |
#        |             Ci_Net |________________          |
#        |_____________________________________|_________|
#                                               Ci
#

package require oa


namespace eval globalData {
    variable ns           [oa::oaNativeNS]
    variable MODE_WRITE   "w"
    variable chpNameLib   "Lib"
    variable chpNameView  "netlist"
    variable chpPathLib   "./Lib"
    variable scNameLib    [oa::oaScalarName $ns $chpNameLib]
    variable scNameView   [oa::oaScalarName $ns $chpNameView]
}

proc getTermName { term } {

    if { $term == "NULL" } {
        return "" 
    }

    # Return a String representation for the term's name.
    # Use the NameSpace from the globalData singleton.
    set name [$term getName $globalData::ns]
    return $name;
}


proc getInstTermName { instTerm } {
    if  { $instTerm == "NULL" }  {
        return "" 
    }

    # Return a name of the form, Instname.Termname, that is, the name
    # of InstTerm's Inst, followed by a ".", followed by the Term's name.
    # Use the NameSpace from the globalData singleton.
    set instname  [[$instTerm getInst] getName $globalData::ns]
    set termname  [[$instTerm getTerm] getName $globalData::ns]
    set name $instname.$termname
    return $name
}


proc makeNetAndTerm { block chNameTerm termType } {

    # Create a Net allowing the database to assign a name to it.
    # Create a Term connected to that Net with the name and type passed in
    # as an args. Use the NameSpace from the globalData singleton.
    set net [oa::oaScalarNet_create $block]
    set termName [oa::oaScalarName $globalData::ns $chNameTerm]
    oa::oaScalarTerm_create $net $termName $termType

    return $net;
}

proc printNets { block } {
    # Print the names of all Nets in the specified Block. For each Net,
    # print the Term names of all Terms and InstTerms attached to it.

    if { $block == "NULL" } {
        return
    }

    # Using the NameSpace from the globals singleton, get the Cell name of
    # the Block's owner Design in the cellName variable.
    set cellName [[$block  getDesign] getCellName $globalData::ns]

    puts ""
    puts "The following Nets exist in Cell: $cellName"

    # Iterate over the nets in the bloack
    oa::foreach net [$block getNets] {
        # get the name of the Net into the netName variable.
        # (HINT: Map via the namespace from the globals singleton.)
        set netName [$net getName $globalData::ns]

        set termNames ""

        # Get a list of terminal names
        oa::foreach term [$net getTerms] {
            lappend termNames [getTermName $term]
        }

        # Iterate over the instTerms on the net
        oa::foreach instTerm [$net getInstTerms] {
            lappend termNames [getInstTermName $instTerm]
        }
        puts "Net: $netName is connected to Term: $termNames";
    }
    puts ""
}


proc makeLeafCell { strNameCell } {
    # Create a netlist type Design with the mode that will overwrite any
    # existing Design of that type and names.
    # (HINT: Use globalData namespace variables to help build the scalar names
    # and other needed arguments.)
    set scNameCell [oa::oaScalarName $globalData::ns $strNameCell]
    set scNameLib  [oa::oaScalarName $globalData::ns $globalData::chpNameLib]
    set scNameView [oa::oaScalarName $globalData::ns $globalData::chpNameView]
    set viewType   [oa::oaViewType_get [oa::oaReservedViewType "netlist"]]
    set mode       $globalData::MODE_WRITE
    set design [oa::oaDesign_open  $scNameLib $scNameCell $scNameView $viewType $mode]

    # Create the top Block in the Design to own the design objects.
    set block  [oa::oaBlock_create $design]

    # Create the Nets and Terms for the leaf gate.
    #
    makeNetAndTerm $block "A" $oa::oacInputTermType
    makeNetAndTerm $block "B" $oa::oacInputTermType
    makeNetAndTerm $block "Y" $oa::oacOutputTermType

    printNets $block
    $design save
    $design close
}

proc makeHalfAdder {} {

    # Open a netlist type Design and name "HalfAdder" with the mode that will
    # overwrite any existing Design of that type and name.
    set scNameCell [oa::oaScalarName $globalData::ns "HalfAdder"]
    set scNameLib  [oa::oaScalarName $globalData::ns $globalData::chpNameLib]
    set scNameView [oa::oaScalarName $globalData::ns $globalData::chpNameView]
    set viewType   [oa::oaViewType_get [oa::oaReservedViewType "netlist"]]
    set mode       $globalData::MODE_WRITE
    set designHA [oa::oaDesign_open  $scNameLib $scNameCell $scNameView $viewType $mode]

    # Create a top Block for HalfAdder Design.
    set blockHA [oa::oaBlock_create $designHA]

    puts "Creating HalfAdder with 1 And Inst and 1 Xor Inst"

    # Create the 4 Terms on this halfadder: A and B are inputs, C and S are outputs.
    set A_Net [makeNetAndTerm $blockHA "A" $oa::oacInputTermType]
    set B_Net [makeNetAndTerm $blockHA "B" $oa::oacInputTermType]
    set C_Net [makeNetAndTerm $blockHA "C" $oa::oacOutputTermType]
    set S_Net [makeNetAndTerm $blockHA "S" $oa::oacOutputTermType]

    # oaTransform trans is necessary for and Inst even though we don't
    # care about placement at this point in the netlist.
    #
    set originX 0
    set originY 0

    set trans [oa::oaTransform $originX $originY [oa::oaOrient "R0"]]


    # Create a scalar Inst named "And1" with the master "And" using the
    # method that takes lib/cell/view names of the master.
    set scNameAnd  [oa::oaScalarName $globalData::ns "And"]
    set scNameAnd1 [oa::oaScalarName $globalData::ns "And1"]
    set and1 [oa::oaScalarInst_create $blockHA $scNameLib $scNameAnd $scNameView $scNameAnd1 $trans]

    # Create by names a scalar Inst named "Xor1" with the master "Xor".
    # of the master.
    set scNameXor  [oa::oaScalarName $globalData::ns "Xor"]
    set scNameXor1 [oa::oaScalarName $globalData::ns "Xor1"]
    set xor1 [oa::oaScalarInst_create $blockHA $scNameLib $scNameXor $scNameView $scNameXor1 $trans]

    # Create and hook the InstTerms of And1 and Xor1 to the right Nets.
    #
    makeInstTermConnectToNet "A" $and1 $A_Net
    makeInstTermConnectToNet "B" $and1 $B_Net
    makeInstTermConnectToNet "Y" $and1 $C_Net
    makeInstTermConnectToNet "A" $xor1 $A_Net
    makeInstTermConnectToNet "B" $xor1 $B_Net
    makeInstTermConnectToNet "Y" $xor1 $S_Net

    printNets $blockHA
    $designHA save
    $designHA close
}

proc makeFullAdder {} {

    # Create a 'netlist' type Design and name "FullAdder" with the mode that
    # will overwrite any existing Design of that type and name.
    set scNameCell [oa::oaScalarName $globalData::ns "FullAdder"]
    set scNameLib  [oa::oaScalarName $globalData::ns $globalData::chpNameLib]
    set scNameView [oa::oaScalarName $globalData::ns $globalData::chpNameView]
    set viewType   [oa::oaViewType_get [oa::oaReservedViewType "netlist"]]
    set mode       $globalData::MODE_WRITE
    set designFA [oa::oaDesign_open  $scNameLib $scNameCell $scNameView $viewType $mode]

    # Create a top Block for FullAdder Design.
    set blockFA [oa::oaBlock_create $designFA]

    puts "Creating FullAdder with two HalfAdder Insts, one Or Inst";

    set A_Net  [makeNetAndTerm $blockFA "A"  $oa::oacInputTermType]
    set B_Net  [makeNetAndTerm $blockFA "B"  $oa::oacInputTermType]
    set Ci_Net [makeNetAndTerm $blockFA "Ci" $oa::oacInputTermType]
    set Co_Net [makeNetAndTerm $blockFA "Co" $oa::oacOutputTermType]
    set S_Net  [makeNetAndTerm $blockFA "S"  $oa::oacOutputTermType]

    # Create internal Nets SA_Net, CA_Net, CB_Net, using the Net create
    # method that lets the implementation assign the Net name.
    set SA_Net [oa::oaScalarNet_create $blockFA]
    set CA_Net [oa::oaScalarNet_create $blockFA]
    set CB_Net [oa::oaScalarNet_create $blockFA]

    # oaTransform trans is necessary to create the Inst even though
    # placement at this netlist stage is irrelevant. Create a null
    # Transform that neither translates nor rotates.
    #
    set originX 0
    set originY 0

    set trans [oa::oaTransform $originX $originY [oa::oaOrient "R0"]]

    # Create two ScalarInsts "Ha1" and "Ha2" of master "HalfAdder"
    # and one ScalarInst "Or1" of master "Or" using the create method that
    # takes the lib/cell/view names of the master.
    set scNameInstCell [oa::oaScalarName $globalData::ns "HalfAdder"]
    set scNameInstName [oa::oaScalarName $globalData::ns "Ha1"]
    set instHa1 [oa::oaScalarInst_create $blockFA $scNameLib $scNameInstCell $scNameView $scNameInstName $trans]

    set scNameInstName [oa::oaScalarName $globalData::ns "Ha2"]
    set instHa2 [oa::oaScalarInst_create $blockFA $scNameLib $scNameInstCell $scNameView $scNameInstName $trans]

    set scNameInstCell [oa::oaScalarName $globalData::ns "Or"]
    set scNameInstName [oa::oaScalarName $globalData::ns "Or1"]
    set instOr1 [oa::oaScalarInst_create $blockFA $scNameLib $scNameInstCell $scNameView $scNameInstName $trans]

    # Create and hook the InstTerms to the right Nets.
    #
    makeInstTermConnectToNet "A" $instHa1 $A_Net
    makeInstTermConnectToNet "B" $instHa1 $B_Net
    makeInstTermConnectToNet "C" $instHa1 $CA_Net
    makeInstTermConnectToNet "S" $instHa1 $SA_Net

    makeInstTermConnectToNet "A" $instHa2 $SA_Net
    makeInstTermConnectToNet "B" $instHa2 $Ci_Net
    makeInstTermConnectToNet "C" $instHa2 $CB_Net
    makeInstTermConnectToNet "S" $instHa2 $S_Net

    makeInstTermConnectToNet "A" $instOr1 $CA_Net
    makeInstTermConnectToNet "B" $instOr1 $CB_Net
    makeInstTermConnectToNet "Y" $instOr1 $Co_Net

    printNets $blockFA
    $designFA save
    $designFA close
}


proc makeInstTermConnectToNet { strNameTerm inst net } {
    # Create an InstTerm of the Term named strNameTerm
    # (using the NameSpace in the globalData singleton) on the Inst and connected to
    # the Net specified by the input arguments.
    set nameTerm [ oa::oaName $globalData::ns $strNameTerm ]
    oa::oaInstTerm_create $net $inst $nameTerm
}


# Declare a proc that takes a path argument and returns true if a Lib exists
# at that path; false otherwise
proc libAlreadyOnDisk { path } {
    if { [oa::oaLib_exists $path]  } {
        return 1
     } else {
        return 0
     }
}

# #############################################################
# "main" program
# #############################################################

oa::oaDesignInit

# No point in trying oaLib::find() since in this simple program there
# are no other threads or functions that might have opened the Lib alrready.
# However, the program may have executed already, in which case a
# Lib directory will exist and attempting create again on it will hurl.
#
if { [libAlreadyOnDisk $globalData::chpPathLib]  } {
    puts "Opening existing Lib name=$globalData::chpNameLib mapped to filesystem path=$globalData::chpPathLib"

    # Open an existing Lib using the scNameLib and chpPathLib from globalData.
    set lib [oa::oaLib_open $globalData::scNameLib $globalData::chpPathLib]
} else {
    puts "Creating new Library for Lib name=$globalData::chpNameLib mapped to filesystem path=$globalData::chpPathLib"

    # Create a new Lib in shared mode using the DMFileSys PlugIn.
    set lib [oa::oaLib_create $globalData::scNameLib $globalData::chpPathLib \
            [oa::oaLibMode shared] "oaDMFileSys"]
}

puts "Overwriting lib.defs file" ;

# Create/overwrite a lib.defs file in the current directory,
# that has a library definition mapping scNameLib to chpPathLib.
set ldl [oa::oaLibDefList_get  "lib.defs"  "w"]
oa::oaLibDef_create $ldl  $globalData::scNameLib $globalData::chpPathLib 
$ldl save

# Create the "cell library".
#

makeLeafCell  "And"
makeLeafCell  "Or"
makeLeafCell  "Xor"

# Create the design hierarchy.
#
makeHalfAdder
makeFullAdder

#eof
