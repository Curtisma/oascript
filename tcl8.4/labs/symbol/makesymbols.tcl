###############################################################################
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   09/07/10  1.0      scarver, Si2     OA Scripting Language Labs
#
###############################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
##############################################################################

# Create Shapes that represent a simple schematic symbol for the leaf cell.
#   (These are not electrical or manufacturing geometries, merely Shapes
#   that comprise the symbol on the schematic.)

proc makeFaSymbol {} {
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    set label "FullAdder"
    set design [ makeSchSymView $label  ]
    set block [ $design getTopBlock ]

    # Create a rectangle shape to be the full-adder symbol with the lower left at
    # the origin and the upper right at {60,52}. Use the layer and purpose from
    # the globals singleton object.
    #
    #         3,95_________________159,95
    #            |                 |
    #            |                 |
    #            |                 |
    #            |                 |
    #            |_________________|
    #         3, 3                 159, 3
    #
    set box [list 3 3  159 95 ]
    oa::oaRect_create $block $globalData::numLayerDev $globalData::numPurposeDev $box

    # Use the makeLabel function to put the cell's name at the location {2,2}
    set origin { 2 2 }
    makeLabel $block $origin

    saveCloseDesign $design
}

# Create Shapes that represent a simple schematic symbol for the leaf cell.
#   (These are not electrical or manufacturing geometries, merely Shapes
#   that comprise the symbol on the schematic.)

proc makeHaSymbol {} {
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    set label "HalfAdder"
    set design [ makeSchSymView $label  ]
    set block [ $design getTopBlock ]

    # Create a rectangle shape to be the half-adder symbol with the lower left at
    # the origin and the upper right at {64,50}. Use the layer and purpose from
    # the globals singleton object.
    #
    #      0,50__________64,50
    #         |          |
    #         |          |
    #         |          |
    #         |          |
    #         |__________|
    #       0,0          64,0
    #
    set box [list 0 0  64 50 ]
    oa::oaRect_create $block $globalData::numLayerDev $globalData::numPurposeDev $box

    # Use the makeLabel function to put the cell's name at the location {2,25}
    set pt { 2 25 }
    makeLabel $block $pt

    saveCloseDesign $design
}

proc makeAndSymbol {} {
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    set label "And"
    set design [ makeSchSymView $label ]
    set block [ $design getTopBlock ]

    # make a crude AND symbol out of two open Shapes
    #
    #    0,24              16,24
    #       .---------------+
    #       |                    +
    #       |                       +
    #       |                         +
    #       |                          +
    #       |                           +
    #       |                           o <--- midpoint is 28,12
    #       |                           +
    #       |                          +
    #       |                         +
    #       |                       +
    #       |                    +
    #       .---------------+
    #     0,0              16,0

    # Create a PointArray representing the 3-sided, open box part on the left.
    #
    set pt0 [ list 16  0 ]
    set pt1 [ list  0  0 ]
    set pt2 [ list  0 24 ]
    set pt3 [ list 16 24 ]
    set pts [ list $pt0 $pt1 $pt2 $pt3 ]

    # Create in the owner Block the appropriate, non-closed oaShape using the array.
    oa::oaLine_create $block $globalData::numLayerDev $globalData::numPurposeDev $pts

    # Create an arc representing the part on the right.
    # To make it easy, assume the curve is simply the right half of
    # an ellipse centered in a bounding Box 12 to the right and 12 left
      #
    #    0,24   4,24       16,24
    #       |   |           |
    #            ---------- + ----------
    #           |                +      |
    #           |                   +   |
    #           |                     + |
    #           |                      +|
    #           |                       +
    #           |                       o <--- midpoint is 28,12
    #           |                       +
    #           |                      +|
    #           |                     + |
    #           |                   +   |
    #           |                +      |
    #            ---------- + ----------
    #       |   |           |
    #     0,0   4,0         16,0

    # Define the BBox for the right arc of the "And" symbol.
    set bBox [ list 4 0 28 24 ]

    # Create an appropriate open Shape for this right-hand part of the "And" symbol.
    # Assume the angle sweeps from -90 to 90 degrees to make it easy)
    set radiansStart -1.571
    set radiansEnd    1.571
    oa::oaArc_create $block $globalData::numLayerDev $globalData::numPurposeDev \
                     $bBox $radiansStart $radiansEnd

    # Use the makeLabel function to put the cell's name at the location {2,2}
    set pt { 2 2 }
    makeLabel $block $pt

    saveCloseDesign $design
}


proc makeOrSymbol {} {
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    set label "Or"
    set design [ makeSchSymView $label ]
    set block [ $design getTopBlock ]

    # make a crude OR symbol out of two identical arcs, left and right
    # plus two lines joining them top and bottom
    #
    #    0,24               16,24
    #       .---------------+
    #            +               +
    #               +               +
    #                 +               +
    #                  +               +
    #                   +               +
    #         12,12 --> o               o <--- 28,12
    #                   +               +
    #                  +               +
    #                 +               +
    #               +               +
    #            +               +
    #       .---------------+
    #     0,0               16,0

    # Copy the same code that made the oaArc for the AND symbol and assign it to arcRight.
    set bBox [ list 4 0 28 24 ]
    set arcRight [ oa::oaArc_create $block \
                              $globalData::numLayerDev  $globalData::numPurposeDev \
                              $bBox -1.571  1.571 ]

    # Reuse this arc by making a copy of it in the Block and moving it left by (16,0)
    set trans [ oa::oaTransform -16 0 ]
    $arcRight copy $trans

    # Create the top Line from PointArray pa
    set pa [ list [ list 0 24 ] [ list 16 24 ] ]
    oa::oaLine_create $block  $globalData::numLayerDev  $globalData::numPurposeDev  $pa

    # Reset the Y values of pa1 to match those of the bottom line of the symbol
    set pa [ list [ list 0 0 ] [ list 16 0 ] ]

    # Create another Line reusing pa1
    oa::oaLine_create $block  $globalData::numLayerDev  $globalData::numPurposeDev  $pa

    # Use the makeLabel function to put the cell's name at the location {2,2}
    set pt [ list 2 2 ]
    makeLabel $block $pt

    saveCloseDesign $design
}


proc makeXorSymbol {} {
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    set label "Xor"
    set design [ makeSchSymView $label ]

    # make a crude XOR symbol that looks just like the OR, moved to the right 6
    # with an extra copy of the arc on the left at the Y-axis position.
    #
    #    0,24                     22,24
    #       +  .  +---------------+ .
    #            +     +               +
    #               +     +               +
    #                 +     +               +
    #                  +     +               +
    #                   +     +               +
    #         12,12 --> o     o <--18,12      o <--- 34,12
    #                   +     +               +
    #                  +     +               +
    #                 +     +               +
    #               +     +               +
    #           .+    .+              .+
    #       +     +---------------+
    #     0,0                     22,0


    # Open the symbol Design of the Or just create above (in read mode).
    set scNameLib $globalData::scNameLibSymbol
    set scNameCell [ oa::oaScalarName $globalData::ns "Or" ]
    set scNameView [ oa::oaScalarName $globalData::scNameViewSymbol]
    set mode       $globalData::mode_read
    set viewType   [ oa::oaViewType_get [oa::oaReservedViewType "netlist"] ]
    set design_or  [ oa::oaDesign_open $scNameLib $scNameCell $scNameView $viewType $mode ]

    # Create an iterator for all the Shapes in the Or's Block
    set block [$design_or getTopBlock]
    set shapes [ $block getShapes ]

    set savedArc  NULL
    set savedText NULL

    # Iterate over the shapes
    oa::foreach shape $shapes {
        # Copy each shape to this Xor Design, moving it to the right by 6,
        # saving the handle of the newly copied Shape in copiedShape.
        set trans [ oa::oaTransform 6 0 ]
        set copiedShape [ $shape copy $trans $design ]

        # If that shape is an oaArc, then save a pointer to it in savedArc.
        if { [[$copiedShape getType] getName] == "Arc" } { set savedArc $copiedShape }

        # If that shape is the oaText (label) save a pointer to it in savedText
        if { [[$copiedShape getType ] getName] == "Text" } { set savedText $copiedShape }
    }

    # At this point the new Xor Design Block has a copy of the symbol
    # shapes created for the Or, but moved to the right by 6, and
    #    - savedArc is one of the arcs
    #    - savedText is the Text label

    set newArcBox [list -12 0 12 24 ]

    # make one more arc in the Xor Design Block by copying that savedArc, assigning
    # it to the newArc variable (the position of it doesn't matter, it will be reset).
    set trans [ oa::oaTransform 0 0 ]
    set newArc [ $savedArc copy $trans ]

    # Reset the Box of newArc to newArcBox and the start/stop angles to the same angles
    # obtained from that original savedArc.
    $savedArc set $newArcBox [ $savedArc getStartAngle ] [ $savedArc getStopAngle ]

    # An oaText label was just copied (with the other Shapes) from the Or Cell,
    # so instead of using makeLabel to add another one, just change the value
    # of that existing oaText (saved from the loop above) from the lib/cell/view
    # name of the Or to that of the Xor.
    $savedText setText [ getCellName $design ]

    saveCloseDesign $design
}
