#
# Copyright 2011 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


namespace eval oa {

    proc oaNameSpace_eval { ns script } {
        oa::oaNameSpace_push $ns
        ::set code [catch {uplevel 1 $script} result]
        oa::oaNameSpace_pop
        return -code $code -errorinfo $::errorInfo $result
    }

    proc foreach { varName coll script } {
        upvar $varName local
        ::set iter [$coll createIter]
        while {[::set obj [$iter getNext]] != "NULL"} {
                ::set local $obj
                uplevel 1 $script
        }
    }

}

