#!/usr/bin/env tcl
##############################################################################
# Copyright 2011 Synopsys, Inc.
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

package forget oa

set path [file dirname [info script]]
set libpath [file normalize [file join $path .. ]]
lappend auto_path $libpath

package require oa

# Test each oa*AppDef by creating it and then returning the name.  The
# getSession() method is called to test inheritance which was a problem
# previously on the oa*AppDef proxies.

## Boolean

set boolean [oa::oaBooleanAppDef_get oa::oaNet "boolean"]
puts "name: [$boolean getName], default: [$boolean getDefault]"
$boolean getSession
oa::oaBooleanAppDef_find oa::oaNet "boolean"

## Data

#set data [oa::oaDataAppDef_get oa::oaNet "data" 8 abc]
#puts "name: [$data getName], size: [$data getSize]"
#$data getSession
#oa::oaDataAppDef_find oa::oaNet "data"

## Double

set double [oa::oaDoubleAppDef_get oa::oaNet "double"]
puts "name: [$double getName], default: [$double getDefault]"
$double getSession
oa::oaDoubleAppDef_find oa::oaNet "double"

## Float

set float [oa::oaFloatAppDef_get oa::oaNet "float"]
puts "name: [$float getName], default: [$float getDefault]"
$float getSession
oa::oaFloatAppDef_find oa::oaNet "float"

## Int

set int [oa::oaIntAppDef_get oa::oaNet "int"]
puts "name: [$int getName], default: [$int getDefault]"
$int getSession
oa::oaIntAppDef_find oa::oaNet "int"

## InterPointer

set interPointer [oa::oaInterPointerAppDef_get oa::oaNet "interPointer"]
puts "name: [$interPointer getName], no default"
$interPointer getSession
oa::oaInterPointerAppDef_find oa::oaNet "interPointer"

## IntraPointer

set intraPointer [oa::oaIntraPointerAppDef_get oa::oaNet "intraPointer"]
puts "name: [$intraPointer getName], no default"
$intraPointer getSession
oa::oaIntraPointerAppDef_find oa::oaNet "intraPointer"

## String

set string [oa::oaStringAppDef_get oa::oaNet "string" NULL "foobar"]
puts "name: [$string getName], default: [$string getDefault]"
$string getSession
oa::oaStringAppDef_find oa::oaNet "string"

## Time

set time [oa::oaTimeAppDef_get oa::oaNet "time"]
puts "name: [$time getName], default: [$time getDefault]"
$time getSession
oa::oaTimeAppDef_find oa::oaNet "time"

## VarData

set varData [oa::oaVarDataAppDef_get oa::oaNet "varData"]
puts "name: [$varData getName], defaultSize: [$varData getDefaultSize]"
$varData getSession
oa::oaVarDataAppDef_find oa::oaNet "varData"

## VoidPointer

set voidPointer [oa::oaVoidPointerAppDef_get oa::oaDesignInst "voidPointer"]
puts "name: [$voidPointer getName], no default"
$voidPointer getSession
puts [oa::oaVoidPointerAppDef_find oa::oaDesignInst "voidPointer"]

