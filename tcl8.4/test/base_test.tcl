#!/usr/bin/env tcl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

package forget oa

set path [file dirname [info script]]
set libpath [file normalize [file join $path .. ]]
lappend auto_path $libpath

package require oa

proc main {} {
    oa::oaBaseInit
    set timer [oa::oaTimer]
    puts [format "%-15s %-15s" "package" "build name"]
    puts [string repeat - 30]
    set bi_arr [oa::oaBuildInfo_getPackages]
    foreach bi $bi_arr {
        puts [format "%-15s %-15s" [$bi getPackageName] [$bi getBuildName]]
    }
    after 1000
    puts "That took [$timer getElapsed] seconds.\n"
}

main

