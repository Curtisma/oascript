#!/usr/bin/env tcl
##############################################################################
# Copyright 2011 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

package forget oa

set path [file dirname [info script]]
set libpath [file normalize [file join $path .. ]]
lappend auto_path $libpath

package require oa

set path [file join [file dirname [info script]] oaUUTestLib]
exec rm -rf $path

proc check { testpoint expected actual } {
    if {[expr {$expected != $actual}]} {
        puts "'$testpoint' : expected $expected, got $actual"
        incr ::numFails 1
    }
}

set numFails 0

set lib [oa::oaLib_create "testLib" $path]
set tech [oa::oaTech_create $lib]
set viewType [oa::oaViewType_find "schematic"]
set dbuPerUU [$tech getDBUPerUU $viewType]

set uuBox [oa::oaUUBox {100 200 400 500} $tech $viewType]
check "Constructor from oaBox left" [$tech dbuToUU $viewType 100] [$uuBox left]
check "Constructor from oaBox bottom" [$tech dbuToUU $viewType 200] [$uuBox bottom]
check "Constructor from oaBox right" [$tech dbuToUU $viewType 400] [$uuBox right]
check "Constructor from oaBox top" [$tech dbuToUU $viewType 500] [$uuBox top]

set left [$tech dbuToUU $viewType 200]
set bottom [$tech dbuToUU $viewType 100]
set right [$tech dbuToUU $viewType 300]
set top [$tech dbuToUU $viewType 600]
set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
check "Constructor from coords left" $left [$uuBox left]
check "Constructor from coords bottom" $bottom [$uuBox bottom]
check "Constructor from coords right" $right [$uuBox right]
check "Constructor from coords top" $top [$uuBox top]

set lowerLeft [oa::oaUUPoint $left $bottom $tech $viewType]
set upperRight [oa::oaUUPoint $right $top $tech $viewType]
set uuBox [oa::oaUUBox $lowerLeft $upperRight $tech $viewType]
check "Constructor from points left" $left [$uuBox left]
check "Constructor from points bottom" $bottom [$uuBox bottom]
check "Constructor from points right" $right [$uuBox right]
check "Constructor from points top" $top [$uuBox top]

set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
set xform [oa::oaTransform {100 200}]
set uuBox2 [oa::oaUUBox $uuBox $xform]
check "Constructor from transform left" [$tech dbuToUU $viewType 300] [$uuBox2 left]
check "Constructor from transform bottom" [$tech dbuToUU $viewType 300] [$uuBox2 bottom]
check "Constructor from transform right" [$tech dbuToUU $viewType 400] [$uuBox2 right]
check "Constructor from transform top" [$tech dbuToUU $viewType 800] [$uuBox2 top]

set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
set lowerLeft [$uuBox lowerLeft]
set upperRight [$uuBox upperRight]
check "lowerLeft x" $left [$lowerLeft x]
check "lowerLeft y" $bottom [$lowerLeft y]
check "upperRight x" $right [$upperRight x]
check "upperRight y" $top [$upperRight y]

set uuBox [oa::oaUUBox $tech $viewType]
$uuBox set $left $bottom $right $top
check "set from coords left" $left [$uuBox left]
check "set from coords bottom" $bottom [$uuBox bottom]
check "set from coords right" $right [$uuBox right]
check "set from coords top" $top [$uuBox top]

set uuBox [oa::oaUUBox $tech $viewType]
set lowerLeft [oa::oaUUPoint $left $bottom $tech $viewType]
set upperRight [oa::oaUUPoint $right $top $tech $viewType]
$uuBox set $lowerLeft $upperRight
check "set from points left" $left [$uuBox left]
check "set from points bottom" $bottom [$uuBox bottom]
check "set from points right" $right [$uuBox right]
check "set from points top" $top [$uuBox top]

set uuBox [oa::oaUUBox $tech $viewType]
set centerX [$tech dbuToUU $viewType 200]
set centerY [$tech dbuToUU $viewType 300]
set size [$tech dbuToUUDistance $viewType 100]
set center [oa::oaUUPoint $centerX $centerY $tech $viewType]
$uuBox set $center $size
check "set from center left" [$tech dbuToUU $viewType 100] [$uuBox left]
check "set from center bottom" [$tech dbuToUU $viewType 200] [$uuBox bottom]
check "set from center right" [$tech dbuToUU $viewType 300] [$uuBox right]
check "set from center top" [$tech dbuToUU $viewType 400] [$uuBox top]

set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
set uuBox2 [oa::oaUUBox $tech $viewType]
set xform [oa::oaTransform {300 500}]
$uuBox2 set $uuBox $xform
check "set from transform left" [$tech dbuToUU $viewType 500] [$uuBox2 left]
check "set from transform bottom" [$tech dbuToUU $viewType 600] [$uuBox2 bottom]
check "set from transform right" [$tech dbuToUU $viewType 600] [$uuBox2 right]
check "set from transform top" [$tech dbuToUU $viewType 1100] [$uuBox2 top]

set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
check "getWidth" [$tech dbuToUUDistance $viewType 100] [$uuBox getWidth]
check "getHeight" [$tech dbuToUUDistance $viewType 500] [$uuBox getHeight]

set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
set center [$uuBox getCenter]
check "getCenter x" [$tech dbuToUU $viewType 250] [$center x]
check "getCenter y" [$tech dbuToUU $viewType 350] [$center y]
set lowerRight [$uuBox getLowerRight]
check "getLowerRight x" $right [$lowerRight x]
check "getLowerRight y" $bottom [$lowerRight y]
set upperLeft [$uuBox getUpperLeft]
check "getUpperLeft x" $left [$upperLeft x]
check "getUpperLeft y" $top [$upperLeft y]

set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
check "hasNoArea false" 0 [$uuBox hasNoArea]
set uuBox [oa::oaUUBox 1 2 1 2 $tech $viewType]
check "hasNoArea true" 1 [$uuBox hasNoArea]

set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
check "isInverted false" 0 [$uuBox isInverted]
set uuBox [oa::oaUUBox $right $top $left $bottom $tech $viewType]
check "isInverted true" 1 [$uuBox isInverted]

set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
set uuBox2 [oa::oaUUBox $uuBox [oa::oaTransform {100 500}]]
check "overlaps box false" 0 [$uuBox overlaps $uuBox2 0]
check "overlaps box true" 1 [$uuBox overlaps $uuBox2]

set box [oa::oaBox 100 200 300 500]
set uuBox [oa::oaUUBox $box $tech $viewType]
set segment [oa::oaSegment {100 200} {100 500}]
set uuSegment [oa::oaUUSegment $segment $tech $viewType]
check "overlaps segment (incEdges==true)" 1 [$uuBox overlaps $uuSegment]
check "overlaps segment (incEdges==false)" 0 [$uuBox overlaps $uuSegment 0]

set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
set uuBox2 [oa::oaUUBox $left $bottom $right $top $tech $viewType]
check "contains box false" 0 [$uuBox contains $uuBox2 0]
check "contains box true" 1 [$uuBox contains $uuBox2]

set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
set uuPoint [oa::oaUUPoint $left $bottom $tech $viewType]
check "contains point false" 0 [$uuBox contains $uuPoint 0]
check "contains point true" 1 [$uuBox contains $uuPoint]

set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
set uuPoint [oa::oaUUPoint 0 0 $tech $viewType]
set distance2Dbu [expr {int(pow(200, 2) + pow(100, 2))}]
check "distanceFrom2" [$tech dbuToUUDistance $viewType $distance2Dbu] [$uuBox distanceFrom2 $uuPoint]

set uuBox [oa::oaUUBox $right $top $left $bottom $tech $viewType]
set uuBox2 [oa::oaUUBox $tech $viewType]
$uuBox fix $uuBox2
# make sure uuBox was not modified (uuBox is created as inverted!)
check "fix other this left" $right [$uuBox left]
check "fix other this bottom" $top [$uuBox bottom]
check "fix other this right" $left [$uuBox right]
check "fix other this top" $bottom [$uuBox top]
# make sure uuBox2 was fixed
check "fix other other left" $left [$uuBox2 left]
check "fix other other bottom" $bottom [$uuBox2 bottom]
check "fix other other right" $right [$uuBox2 right]
check "fix other other top" $top [$uuBox2 top]
# now fix uuBox
$uuBox fix
check "fix this left" $left [$uuBox left]
check "fix this bottom" $bottom [$uuBox bottom]
check "fix this right" $right [$uuBox right]
check "fix this top" $top [$uuBox top]

set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
set uuBox2 [oa::oaUUBox $tech $viewType]
$uuBox transform [oa::oaTransform {100 200}] $uuBox2
# make sure uuBox was not modified
check "transform xform other this left" $left [$uuBox left]
check "transform xform other this bottom" $bottom [$uuBox bottom]
check "transform xform other this right" $right [$uuBox right]
check "transform xform other this top" $top [$uuBox top]
# make sure uuBox2 was transformed
check "transform xfrom other other left" [$tech dbuToUU $viewType 300] [$uuBox2 left]
check "transform xfrom other other bottom" [$tech dbuToUU $viewType 300] [$uuBox2 bottom]
check "transform xfrom other other right" [$tech dbuToUU $viewType 400] [$uuBox2 right]
check "transform xfrom other other top" [$tech dbuToUU $viewType 800] [$uuBox2 top]
# now transfrom uuBox
$uuBox transform [oa::oaTransform {100 200}]
check "transform xfrom left" [$uuBox2 left] [$uuBox left]
check "transform xfrom bottom" [$uuBox2 bottom] [$uuBox bottom]
check "transform xfrom right" [$uuBox2 right] [$uuBox right]
check "transform xfrom top" [$uuBox2 top] [$uuBox top]

set uuBox [oa::oaUUBox $left $bottom $right $top $tech $viewType]
set uuBox2 [oa::oaUUBox $tech $viewType]
set uuPoint [oa::oaUUPoint {100 200} $tech $viewType]
$uuBox transform $uuPoint $uuBox2
# make sure uuBox was not modified
check "transform point other this left" $left [$uuBox left]
check "transform point other this bottom" $bottom [$uuBox bottom]
check "transform point other this right" $right [$uuBox right]
check "transform point other this top" $top [$uuBox top]
# make sure uuBox2 was transformed
check "transform point other left" [$tech dbuToUU $viewType 300] [$uuBox2 left]
check "transform point other bottom" [$tech dbuToUU $viewType 300] [$uuBox2 bottom]
check "transform point other right" [$tech dbuToUU $viewType 400] [$uuBox2 right]
check "transform point other top" [$tech dbuToUU $viewType 800] [$uuBox2 top]
# now transform uuBox
$uuBox transform $uuPoint
check "transform point left" [$uuBox2 left] [$uuBox left]
check "transform point bottom" [$uuBox2 bottom] [$uuBox bottom]
check "transform point right" [$uuBox2 right] [$uuBox right]
check "transform point top" [$uuBox2 top] [$uuBox top]

set box [oa::oaBox 100 200 300 500]
set box2 [oa::oaBox]
set uuBox [oa::oaUUBox $box $tech $viewType]
set uuBox2 [oa::oaUUBox $tech $viewType]
$box transform -1.0 180 $box2
$uuBox transform -1.0 180 $uuBox2
# make sure uuBox was not modified
check "transform scale other this left" [$tech dbuToUU $viewType [$box left]] [$uuBox left]
check "transform scale other this bottom" [$tech dbuToUU $viewType [$box bottom]] [$uuBox bottom]
check "transform scale other this right" [$tech dbuToUU $viewType [$box right]] [$uuBox right]
check "transform scale other this top" [$tech dbuToUU $viewType [$box top]] [$uuBox top]
# make sure uuBox2 was transformed
check "transform scale other left" [$tech dbuToUU $viewType [$box2 left]] [$uuBox2 left]
check "transform scale other bottom" [$tech dbuToUU $viewType [$box2 bottom]] [$uuBox2 bottom]
check "transform scale other right" [$tech dbuToUU $viewType [$box2 right]] [$uuBox2 right]
check "transform scale other top" [$tech dbuToUU $viewType [$box2 top]] [$uuBox2 top]
# now transform uuBox
$uuBox transform -1.0 180
check "transform scale left" [$uuBox2 left] [$uuBox left]
check "transform scale bottom" [$uuBox2 bottom] [$uuBox bottom]
check "transform scale right" [$uuBox2 right] [$uuBox right]
check "transform scale top" [$uuBox2 top] [$uuBox top]

set box [oa::oaBox 100 200 300 500]
set uuBox [oa::oaUUBox $box $tech $viewType]
$box scale 2.0
$uuBox scale 2.0
check "scale left" [$tech dbuToUU $viewType [$box left]] [$uuBox left]
check "scale bottom" [$tech dbuToUU $viewType [$box bottom]] [$uuBox bottom]
check "scale right" [$tech dbuToUU $viewType [$box right]] [$uuBox right]
check "scale top" [$tech dbuToUU $viewType [$box top]] [$uuBox top]

set box [oa::oaBox 100 200 300 500]
set box2 [oa::oaBox 200 300 400 600]
set box3 [oa::oaBox]
set uuBox [oa::oaUUBox $box $tech $viewType]
set uuBox2 [oa::oaUUBox $box2 $tech $viewType]
set uuBox3 [oa::oaUUBox $tech $viewType]
$box merge $box2 $box3
$uuBox merge $uuBox2 $uuBox3
# make sure uuBox was not modified
check "merge box other this left" [$tech dbuToUU $viewType [$box left]] [$uuBox left]
check "merge box other this bottom" [$tech dbuToUU $viewType [$box bottom]] [$uuBox bottom]
check "merge box other this right" [$tech dbuToUU $viewType [$box right]] [$uuBox right]
check "merge box other this top" [$tech dbuToUU $viewType [$box top]] [$uuBox top]
# make sure uuBox3 was modified
check "merge box other left" [$tech dbuToUU $viewType [$box3 left]] [$uuBox3 left]
check "merge box other bottom" [$tech dbuToUU $viewType [$box3 bottom]] [$uuBox3 bottom]
check "merge box other right" [$tech dbuToUU $viewType [$box3 right]] [$uuBox3 right]
check "merge box other top" [$tech dbuToUU $viewType [$box3 top]] [$uuBox3 top]
# now merge uuBox
$uuBox merge $uuBox $uuBox2
check "merge box left" [$uuBox2 left] [$uuBox left]
check "merge box bottom" [$uuBox2 bottom] [$uuBox bottom]
check "merge box right" [$uuBox2 right] [$uuBox right]
check "merge box top" [$uuBox2 top] [$uuBox top]

set box [oa::oaBox 100 200 300 500]
set uuBox [oa::oaUUBox $box $tech $viewType]
set point [oa::oaPoint 100 200]
set uuPoint [oa::oaUUPoint $point $tech $viewType]
set box2 [oa::oaBox]
set uuBox2 [oa::oaUUBox $box $tech $viewType]
$box merge $point $box2
$uuBox merge $uuPoint $uuBox2
# make sure uuBox was not modified
check "merge point other this left" [$tech dbuToUU $viewType [$box left]] [$uuBox left]
check "merge point other this bottom" [$tech dbuToUU $viewType [$box bottom]] [$uuBox bottom]
check "merge point other this right" [$tech dbuToUU $viewType [$box right]] [$uuBox right]
check "merge point other this top" [$tech dbuToUU $viewType [$box top]] [$uuBox top]
# make sure uuBox2 was modified
check "merge point other left" [$tech dbuToUU $viewType [$box2 left]] [$uuBox2 left]
check "merge point other bottom" [$tech dbuToUU $viewType [$box2 bottom]] [$uuBox2 bottom]
check "merge point other right" [$tech dbuToUU $viewType [$box2 right]] [$uuBox2 right]
check "merge point other top" [$tech dbuToUU $viewType [$box2 top]] [$uuBox2 top]
# now merge uuBox
$uuBox merge $uuPoint
check "merge point left" [$uuBox2 left] [$uuBox left]
check "merge point bottom" [$uuBox2 bottom] [$uuBox bottom]
check "merge point right" [$uuBox2 right] [$uuBox right]
check "merge point top" [$uuBox2 top] [$uuBox top]

set box [oa::oaBox 100 200 300 500]
set uuBox [oa::oaUUBox $box $tech $viewType]
set box2 [oa::oaBox 200 300 400 600]
set uuBox2 [oa::oaUUBox $box2 $tech $viewType]
set box3 [oa::oaBox]
set uuBox3 [oa::oaUUBox $tech $viewType]
$box intersection $box2 $box3
$uuBox intersection $uuBox2 $uuBox3
# make sure uuBox was not modified
check "intersection other this left" [$tech dbuToUU $viewType [$box left]] [$uuBox left]
check "intersection other this bottom" [$tech dbuToUU $viewType [$box bottom]] [$uuBox bottom]
check "intersection other this right" [$tech dbuToUU $viewType [$box right]] [$uuBox right]
check "intersection other this top" [$tech dbuToUU $viewType [$box top]] [$uuBox top]
# make sure uuBox3 was modified
check "intersection other left" [$tech dbuToUU $viewType [$box3 left]] [$uuBox3 left]
check "intersection other bottom" [$tech dbuToUU $viewType [$box3 bottom]] [$uuBox3 bottom]
check "intersection other right" [$tech dbuToUU $viewType [$box3 right]] [$uuBox3 right]
check "intersection other top" [$tech dbuToUU $viewType [$box3 top]] [$uuBox3 top]
# now intersect uuBox
$uuBox intersection $uuBox $uuBox2
check "intersection left" [$uuBox2 left] [$uuBox left]
check "intersection bottom" [$uuBox2 bottom] [$uuBox bottom]
check "intersection right" [$uuBox2 right] [$uuBox right]
check "intersection top" [$uuBox2 top] [$uuBox top]

set box [oa::oaBox 100 200 300 500]
set uuBox [oa::oaUUBox $box $tech $viewType]
$box makeZero
$uuBox makeZero
check "makeZero left" [$tech dbuToUU $viewType [$box left]] [$uuBox left]
check "makeZero bottom" [$tech dbuToUU $viewType [$box bottom]] [$uuBox bottom]
check "makeZero right" [$tech dbuToUU $viewType [$box right]] [$uuBox right]
check "makeZero top" [$tech dbuToUU $viewType [$box top]] [$uuBox top]

set box [oa::oaBox 100 200 300 500]
set uuBox [oa::oaUUBox $box $tech $viewType]
$box makeInvertedZero
$uuBox makeInvertedZero
check "makeInvertedZero (not-inverted) left" [$tech dbuToUU $viewType [$box left]] [$uuBox left]
check "makeInvertedZero (not-inverted) bottom" [$tech dbuToUU $viewType [$box bottom]] [$uuBox bottom]
check "makeInvertedZero (not-inverted) right" [$tech dbuToUU $viewType [$box right]] [$uuBox right]
check "makeInvertedZero (not-inverted) top" [$tech dbuToUU $viewType [$box top]] [$uuBox top]
set box2 [oa::oaBox 300 500 100 200]
set uuBox2 [oa::oaUUBox $box2 $tech $viewType]
$box2 makeInvertedZero
$uuBox2 makeInvertedZero
check "makeInvertedZero (inverted) left" [$tech dbuToUU $viewType [$box2 left]] [$uuBox2 left]
check "makeInvertedZero (inverted) bottom" [$tech dbuToUU $viewType [$box2 bottom]] [$uuBox2 bottom]
check "makeInvertedZero (inverted) right" [$tech dbuToUU $viewType [$box2 right]] [$uuBox2 right]
check "makeInvertedZero (inverted) top" [$tech dbuToUU $viewType [$box2 top]] [$uuBox2 top]

set box [oa::oaBox]
set uuBox [oa::oaUUBox $box $tech $viewType]
$box init
$uuBox init
check "init left" [$tech dbuToUU $viewType [$box left]] [$uuBox left]
check "init bottom" [$tech dbuToUU $viewType [$box bottom]] [$uuBox bottom]
check "init right" [$tech dbuToUU $viewType [$box right]] [$uuBox right]
check "init top" [$tech dbuToUU $viewType [$box top]] [$uuBox top]

set uuBox [oa::oaUUBox $tech $viewType]
check "getTech" $tech [$uuBox getTech]
check "getViewType" $viewType [$uuBox getViewType]

if {$numFails == 0} {
    puts "All tests passed."
} else {
    error "$numFails tests failed."
}

exec rm -rf $path

