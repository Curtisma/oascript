#!/usr/bin/env tcl
##############################################################################
# Copyright 2011 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

package forget oa

set path [file dirname [info script]]
set libpath [file normalize [file join $path .. ]]
lappend auto_path $libpath

package require oa

set path [file join [file dirname [info script]] oaUUTestLib]
exec rm -rf $path

proc check { testpoint expected actual } {
    if {[expr {$expected != $actual}]} {
        puts "'$testpoint' : expected $expected, got $actual"
        incr ::numFails 1
    }
}

set numFails 0

set lib [oa::oaLib_create "testLib" $path]
set tech [oa::oaTech_create $lib]
set viewType [oa::oaViewType_find "schematic"]
set dbuPerUU [$tech getDBUPerUU $viewType]

set uuSegment [oa::oaUUSegment [oa::oaSegment {100 200} {300 400}] $tech $viewType]
check "Constructor from oaSegment x1" [$tech dbuToUU $viewType 100] [$uuSegment x1]
check "Constructor from oaSegment x2" [$tech dbuToUU $viewType 300] [$uuSegment x2]
check "Constructor from oaSegment y1" [$tech dbuToUU $viewType 200] [$uuSegment y1]
check "Constructor from oaSegment y2" [$tech dbuToUU $viewType 400] [$uuSegment y2]

set uuPoint [oa::oaUUPoint 1.0 2.0 $tech $viewType]
set uuPoint2 [oa::oaUUPoint 3.0 4.0 $tech $viewType]
set uuSegment [oa::oaUUSegment $uuPoint $uuPoint2 $tech $viewType]
check "Constructor from oaUUPoint x1" 1.0 [$uuSegment x1]
check "Constructor from oaUUPoint x2" 3.0 [$uuSegment x2]
check "Constructor from oaUUPoint y1" 2.0 [$uuSegment y1]
check "Constructor from oaUUPoint y2" 4.0 [$uuSegment y2]
set head [$uuSegment head]
check "head x" 1.0 [$head x]
check "head y" 2.0 [$head y]
set tail [$uuSegment tail]
check "tail x" 3.0 [$tail x]
check "tail y" 4.0 [$tail y]

set vector [oa::oaVector {100 100}]
set uuVector [oa::oaUUVector $vector $tech $viewType]
set uuPoint [oa::oaUUPoint {100 100} $tech $viewType]
set uuSegment [oa::oaUUSegment $uuVector $uuPoint $tech $viewType]
check "Constructor from oaUUVector and oaUUPoint x1" [$tech dbuToUU $viewType 100] [$uuSegment x1]
check "Constructor from oaUUVector and oaUUPoint x2" [$tech dbuToUU $viewType 200] [$uuSegment x2]
check "Constructor from oaUUVector and oaUUPoint y1" [$tech dbuToUU $viewType 100] [$uuSegment y1]
check "Constructor from oaUUVector and oaUUPoint y2" [$tech dbuToUU $viewType 200] [$uuSegment y2]

set uuSegment [oa::oaUUSegment $tech $viewType]
set uuPoint [oa::oaUUPoint 2.0 3.0 $tech $viewType]
set uuPoint2 [oa::oaUUPoint 4.0 5.0 $tech $viewType]
$uuSegment set $uuPoint $uuPoint2
check "set oaUUPoint x1" 2.0 [$uuSegment x1]
check "set oaUUPoint x2" 4.0 [$uuSegment x2]
check "set oaUUPoint y1" 3.0 [$uuSegment y1]
check "set oaUUPoint y2" 5.0 [$uuSegment y2]

set uuSegment [oa::oaUUSegment $tech $viewType]
set vector [oa::oaVector {100 100}]
set uuVector [oa::oaUUVector $vector $tech $viewType]
set point [oa::oaPoint 100 100]
set uuPoint [oa::oaUUPoint $point $tech $viewType]
$uuSegment set $uuVector $uuPoint
check "set point vector x1" [$tech dbuToUU $viewType 100] [$uuSegment x1]
check "set point vector x2" [$tech dbuToUU $viewType 200] [$uuSegment x2]
check "set point vector y1" [$tech dbuToUU $viewType 100] [$uuSegment y1]
check "set point vector y2" [$tech dbuToUU $viewType 200] [$uuSegment y2]

set segment [oa::oaSegment {100 100} {200 100}]
set uuSegment [oa::oaUUSegment $segment $tech $viewType]
check "isHorizontal true" 1 [$uuSegment isHorizontal]
set segment2 [oa::oaSegment {100 100} {200 200}]
set uuSegment2 [oa::oaUUSegment $segment2 $tech $viewType]
check "isHorizontal false" 0 [$uuSegment2 isHorizontal]

set segment [oa::oaSegment {100 100} {100 200}]
set uuSegment [oa::oaUUSegment $segment $tech $viewType]
check "isVectical true" 1 [$uuSegment isVertical]
set segment2 [oa::oaSegment {100 100} {200 200}]
set uuSegment2 [oa::oaUUSegment $segment2 $tech $viewType]
check "isVectical false" 0 [$uuSegment2 isVertical]

set segment [oa::oaSegment {100 100} {100 200}]
set uuSegment [oa::oaUUSegment $segment $tech $viewType]
check "isOrthogonal true" 1 [$uuSegment isOrthogonal]
set segment2 [oa::oaSegment {100 100} {200 200}]
set uuSegment2 [oa::oaUUSegment $segment2 $tech $viewType]
check "isOrthogonal false" 0 [$uuSegment2 isOrthogonal]

set segment [oa::oaSegment {100 100} {200 300}]
set uuSegment [oa::oaUUSegment $segment $tech $viewType]
check "getDeltaX" [$tech dbuToUU $viewType 100] [$uuSegment getDeltaX]
check "getDeltaY" [$tech dbuToUU $viewType 200] [$uuSegment getDeltaY]

set segment [oa::oaSegment {500 600} {700 800}]
set uuSegment [oa::oaUUSegment $segment $tech $viewType]
set point [oa::oaPoint 100 100]
set uuPoint [oa::oaUUPoint $point $tech $viewType]
set expectedDbu [expr {int([$segment distanceFrom2 $point])}]
set distanceFrom2 [$uuSegment distanceFrom2 $uuPoint]
check "distanceFrom2" [$tech dbuToUUDistance $viewType $expectedDbu] $distanceFrom2

set segment [oa::oaSegment {500 600} {700 800}]
set uuSegment [oa::oaUUSegment $segment $tech $viewType]
set point [oa::oaPoint 100 100]
set uuPoint [oa::oaUUPoint $point $tech $viewType]
set point2 [oa::oaPoint]
set expectedDbU [$segment distanceFrom2 $point $point2]
set uuPoint2 [oa::oaUUPoint $tech $viewType]
set distanceFrom2UU [$uuSegment distanceFrom2 $uuPoint $uuPoint2]
check "distanceFrom2 point" [$tech dbuToUUDistance $viewType $expectedDbu] $distanceFrom2UU
check "distanceFrom2 point x" [$tech dbuToUU $viewType [$point x]] [$uuPoint x]
check "distanceFrom2 point y" [$tech dbuToUU $viewType [$point y]] [$uuPoint y]

set segment [oa::oaSegment {400 300} {700 800}]
set uuSegment [oa::oaUUSegment $segment $tech $viewType]
set uuPoint [oa::oaUUPoint {400 300} $tech $viewType]
check "contains includeEnds=false" 0 [$uuSegment contains $uuPoint 0]
check "contains includeEnds=true" 1 [$uuSegment contains $uuPoint 1]

set segment [oa::oaSegment {400 300} {700 800}]
set uuSegment [oa::oaUUSegment $segment $tech $viewType]
set uuPoint [oa::oaUUPoint {400 300} $tech $viewType]
check "collinearContains includeEnds=false" 0 [$uuSegment collinearContains $uuPoint 0]
check "collinearCcontains includeEnds=true" 1 [$uuSegment collinearContains $uuPoint 1]

set segment [oa::oaSegment {100 200} {300 400}]
set uuSegment [oa::oaUUSegment $tech $viewType]
set segment2 [oa::oaSegment {0 0} {100 200}]
set uuSegment2 [oa::oaUUSegment $segment2 $tech $viewType]
check "intersects includesEnds=true" 1 [$uuSegment intersects $uuSegment2]
check "intersects includesEnds=false" 0 [$uuSegment intersects $uuSegment2 0]

set segment [oa::oaSegment {100 100} {400 400}]
set uuSegment [oa::oaUUSegment $segment $tech $viewType]
set segment2 [oa::oaSegment {200 200} {300 300}]
set uuSegment2 [oa::oaUUSegment $segment2 $tech $viewType]
check "intersects includeEnds=true includeOverlap=false" 0 [$uuSegment intersects $uuSegment2 1 0]
check "intersects includeEnds=false includeOverlap=false" 0 [$uuSegment intersects $uuSegment2 0 0]
check "intersects includeEnds=true includeOverlap=true" 1 [$uuSegment intersects $uuSegment2 1 1]
check "intersects includeEnds=false includeOverlap=true" 1 [$uuSegment intersects $uuSegment2 0 1]

set segment [oa::oaSegment {100 100} {400 400}]
set uuSegment [oa::oaUUSegment $segment $tech $viewType]
set segment2 [oa::oaSegment {100 400} {400 100}]
set uuSegment2 [oa::oaUUSegment $segment2 $tech $viewType]
set point [oa::oaPoint]
set uuPoint [oa::oaUUPoint $tech $viewType]
set expected [$segment intersects $segment2 $point]
set actual [$uuSegment intersects $uuSegment2 $uuPoint]
check "intersects point mustIntersect=true" $expected $actual
check "intersects point mustIntersect=true x" [$tech dbuToUU $viewType [$point x]] [$uuPoint x]
check "intersects point mustIntersect=true y" [$tech dbuToUU $viewType [$point y]] [$uuPoint y]
set expected [$segment intersects $segment2 $point 0]
set actual [$uuSegment intersects $uuSegment2 $uuPoint 0]
check "intersects point mustIntersect=false" $expected $actual
check "intersects point mustIntersect=false x" [$tech dbuToUU $viewType [$point x]] [$uuPoint x]
check "intersects point mustIntersect=false y" [$tech dbuToUU $viewType [$point y]] [$uuPoint y]

set uuSegment [oa::oaUUSegment $tech $viewType]
check "getTech" $tech [$uuSegment getTech]
check "getViewType" $viewType [$uuSegment getViewType]

if {$numFails == 0} {
    puts "All tests passed."
} else {
    error "$numFails tests failed."
}

exec rm -rf $path

