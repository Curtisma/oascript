/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#ifdef SWIGPERL
%module "oa::tech";
#endif

#ifdef SWIGRUBY
%module "oa::tech";
#endif

#ifdef SWIGTCL
%module "tech";
#endif

#ifdef SWIGPYTHON
%module "tech";
#endif

#ifdef SWIGCSHARP
%module "oasCSharpTech";
#endif

%{

#include <oa/oaDesignDB.h>

USE_OA_NAMESPACE
USE_OA_COMMON_NAMESPACE
USE_OA_PLUGIN_NAMESPACE

#undef SWIG_SHADOW
#define SWIG_SHADOW 0

oaNameSpace* getDefaultNameSpace();

// Call oaTechInit automatically to avoid SEGV if the user forgets.
// The user can choose to NOT call this in the event that an older OA DM
// model is desired by setting $OASCRIPT_USE_INIT = 0.  Scripting languages
// execute this via SWIG_init(), C# via "%pragma(csharp) imclasscode".
static void oasTechInit()
{
	char *useinit = getenv("OASCRIPT_USE_INIT");
	if (!useinit || strcmp(useinit, "0")) {
		OpenAccess_4::oaTechInit();
	}
}
%}

// Basic agnostic setup
%include "oa.i"

// Optional target-language-specific code:
%include <target_tech_pre.i>
%include <generic/extensions/tech/oaUUPoint.i>
%include <generic/extensions/tech/oaUUVector.i>
%include <generic/extensions/tech/oaUUSegment.i>
%include <generic/extensions/tech/oaUUBox.i>

#if !defined(oaTechDB_P)
#define oaTechDB_P



// *****************************************************************************
// Public Header Includes
// *****************************************************************************
%import "dm.i"

%include "oa/oaTechDBTypes.h"

%include "oa/oaTechModTypes.h"
%include "oa/oaTechMsgs.h"
%include "oa/oaTechObject.h"
%include "oa/oaLayerArray.h"
%include "oa/oaTechCollection.h"
%include "oa/oaTechHeader.h"
%include "oa/oaTech.h"
%include "oa/oaTechException.h"
%include "oa/oaLayer.h"
%include "oa/oaDerivedLayerParamDef.h"
%include "oa/oaDerivedLayerParam.h"
%include "oa/oaDerivedLayerDef.h"
%include "oa/oaDerivedLayer.h"
%include "oa/oaSizedLayer.h"
%include "oa/oaPhysicalLayer.h"
%include "oa/oaPurpose.h"
%include "oa/oaSiteDef.h"
%include "oa/oaViaParam.h"
%include "oa/oaViaDef.h"
%include "oa/oaViaDefArray.h"
%include "oa/oaTechValue.h"
%include "oa/oaViaSpec.h"
%include "oa/oaOpPoint.h"
%include "oa/oaAnalysisLib.h"
%include "oa/oaTechTraits.h"
%include "oa/oaTechLayerHeader.h"
%include "oa/oaTechViaDefHeader.h"
%include "oa/oaTechObserver.h"
%include "oa/oaLayerConstraint.h"
%include "oa/oaLayerPairConstraint.h"
%include "oa/oaLayerArrayConstraint.h"

#endif

// Optional target-language-specific code:
%include <target_tech_post.i>

%include <generic/extensions/tech.i>

#if !defined(SWIGCSHARP)
%init %{

  // Call oaTechInit automatically to avoid SEGV if the user forgets.
  // The user can choose to NOT call this in the event that an older OA DM
  // model is desired by setting $OASCRIPT_USE_INIT = 0.  Scripting languages
  // execute this via SWIG_init(), C# via "%pragma(csharp) imclasscode".
  oasTechInit();
  
%}
#endif

