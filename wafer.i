/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#ifdef SWIGPERL
%module "oa::wafer";
#endif

#ifdef SWIGRUBY
%module "oa::wafer";
#endif

#ifdef SWIGTCL
%module "wafer";
#endif

#ifdef SWIGPYTHON
%module "wafer";
#endif

#ifdef SWIGCSHARP
%module "oasCSharpWafer";
#endif

%{

#include <oa/oaDesignDB.h>
#include <oa/oaWaferDB.h>

USE_OA_NAMESPACE
USE_OA_COMMON_NAMESPACE
USE_OA_PLUGIN_NAMESPACE

#undef SWIG_SHADOW
#define SWIG_SHADOW 0

oaNameSpace* getDefaultNameSpace();

// Call oaWaferInit automatically to avoid SEGV if the user forgets.
// The user can choose to NOT call this in the event that an older OA DM
// model is desired by setting $OASCRIPT_USE_INIT = 0.  Scripting languages
// execute this via SWIG_init(), C# via "%pragma(csharp) imclasscode".
static void oasWaferInit()
{
	char *useinit = getenv("OASCRIPT_USE_INIT");
	if (!useinit || strcmp(useinit, "0")) {
		OpenAccess_4::oaWaferInit();
	}
}
%}

// Basic agnostic setup
%include "oa.i"

// Optional target-language-specific code:
%include <target_wafer_pre.i>



#if !defined(oaWaferDB_P)
#define oaWaferDB_P



// *****************************************************************************
// Public Header Includes
// *****************************************************************************
%import "design.i"

%include "oa/oaWaferDBTypes.h"

%include "oa/oaWaferMsgs.h"
%include "oa/oaWaferModTypes.h"
%include "oa/oaWaferObject.h"
%include "oa/oaWaferException.h"
%include "oa/oaWafer.h"
%include "oa/oaWaferFeature.h"
%include "oa/oaWaferDesc.h"
%include "oa/oaFrame.h"
%include "oa/oaReticle.h"
%include "oa/oaReticleRef.h"
%include "oa/oaStepperMap.h"
%include "oa/oaImage.h"
%include "oa/oaFrameInst.h"
%include "oa/oaDesignInst.h"
%include "oa/oaWaferCollection.h"
%include "oa/oaWaferTraits.h"
%include "oa/oaWaferObserver.h"



// *****************************************************************************
// Public Inline Includes
// *****************************************************************************
%include "oa/oaWaferEnumWrapper.inl"
%include "oa/oaWaferException.inl"
%include "oa/oaImage.inl"
%include "oa/oaDesignInst.inl"
%include "oa/oaWaferCollection.inl"



#endif

// Optional target-language-specific code:
%include <target_wafer_post.i>

%include <generic/extensions/wafer.i>

#if !defined(SWIGCSHARP)
%init %{

  // Call oaWaferInit automatically to avoid SEGV if the user forgets.
  // The user can choose to NOT call this in the event that an older OA DM
  // model is desired by setting $OASCRIPT_USE_INIT = 0.  Scripting languages
  // execute this via SWIG_init(), C# via "%pragma(csharp) imclasscode".
  oasWaferInit();
  
%}
#endif

